import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { FollowingService } from "./following.service";
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: "app-following",
  templateUrl: "./following.component.html",
  styleUrls: ["./following.component.css"]
})
export class FollowingComponent implements OnInit {
  // following: any;
  following = [];
  baseimage: any;
  length: any;
  isEmpty: any;
  status: any;
  user_id: any;
  followText: any;
  previousUrl : any;
  msg: any;
  errprmsg: any;
  showLoader = false;
  constructor(private followingService: FollowingService, private toaster: ToastrService) {
    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.isEmpty = true;
    this.loadFollowing();
    this.followText = "Following";
    this.previousUrl = localStorage.getItem("previousUrl")
  }
  loadFollowing() {
    this.followingService.getFollowing().subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        this.following = mapped;
        this.showLoader = false;
      },
      error => {
        console.log("ERROR", error);
      }
    );
  }

  followUser(e) {
    this.followingService
      .followUser({ status: e.target.dataset.status, user_id: e.target.dataset.user })
      .pipe(first())
      .subscribe(
        (res: any) => {
          if(res.status){
           // this.toaster.success(res.message);
            //this.loadFollowing();
          
            if(e.target.dataset.status == 2){
              $(".user_"+e.target.dataset.user).attr('data-status', 1);
            }else{
              $(".user_"+e.target.dataset.user).attr('data-status', 2);
            }

            if( $(".user_"+e.target.dataset.user).text() == 'Following'){
              $(".user_"+e.target.dataset.user).text('Follow') 
              $(".user_"+e.target.dataset.user).removeClass('following_btn') 
             }else{
              $(".user_"+e.target.dataset.user).text('Following') 
             }
          }else{
            this.toaster.error(res.message)
          }
        },
        error => {
          console.log("ERROR", error);
        }
      );
  }
}
