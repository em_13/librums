import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class FollowingService {
  userdata: any;
  baseURL: string;
  constructor(private http: HttpClient) {
    this.baseURL = BaseUrl.frontend;
  }

  getFollowing() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + `/getFollowing`, null, { headers });
  }

  followUser(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });

    return this.http.post(this.baseURL + `/followUser`, postData, { headers });
  }
}
