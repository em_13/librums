import { Injectable } from '@angular/core';
import { BaseUrl } from "./../../../base-url";
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class ShareSingleBookService {
  userdata: any;
  baseuel: string;
  constructor(private http: HttpClient) {
    this.baseuel = BaseUrl.frontend;
  }

  getBookShelf(data) {
    return this.http.post(this.baseuel + `/getShareBookById`, data);
  }
}
