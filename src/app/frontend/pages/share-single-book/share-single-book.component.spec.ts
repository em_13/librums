import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareSingleBookComponent } from './share-single-book.component';

describe('ShareSingleBookComponent', () => {
  let component: ShareSingleBookComponent;
  let fixture: ComponentFixture<ShareSingleBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareSingleBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareSingleBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
