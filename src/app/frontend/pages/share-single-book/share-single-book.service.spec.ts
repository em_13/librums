import { TestBed } from '@angular/core/testing';

import { ShareSingleBookService } from './share-single-book.service';

describe('ShareSingleBookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShareSingleBookService = TestBed.get(ShareSingleBookService);
    expect(service).toBeTruthy();
  });
});
