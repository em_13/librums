import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseUrl } from "./../../../base-url";
import { ShareSingleBookService } from './share-single-book.service';
@Component({
  selector: 'app-share-single-book',
  templateUrl: './share-single-book.component.html',
  styleUrls: ['./share-single-book.component.css']
})
export class ShareSingleBookComponent implements OnInit {
  NewStoriesSlider: any;
  baseurl: any;
  bookId: any;
  rmbtn = false;
  addbtn = false;  
  showLoader = false;  
  SlideOptions = {
    stagePadding: 200,
    loop: false,
    margin: 10,
    nav: false,
    dots: false,
    items: 1,
    lazyLoad: false,
    responsive: {
      0: {
        items: 1,
        nav: false,
        stagePadding: 60
      },
      600: {
        items: 1,
        nav: false,
        stagePadding: 100
      },
      1000: {
        items: 1,
        nav: false,
        stagePadding: 200
      },
      1200: {
        items: 1,
        nav: false,
        stagePadding: 250
      },
      1400: {
        items: 1,
        nav: false,
        stagePadding: 300
      },
      1600: {
        items: 1,
        nav: false,
        stagePadding: 350
      },
      1800: {
        items: 2,
        nav: false,
        stagePadding: 300
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: false };
  
  constructor(  private bookshelf: ShareSingleBookService, private activatedRoute: ActivatedRoute) {
    this.baseurl = BaseUrl.image;
  }

  ngOnInit() {
    this.bookId = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadBookShelfData();
  }

  loadBookShelfData() {
    this.bookshelf.getBookShelf({'device_type':'web', book_id: this.bookId }).subscribe(
      (res: any) => {
        if(res.status){
          this.NewStoriesSlider = res.data;
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
}
