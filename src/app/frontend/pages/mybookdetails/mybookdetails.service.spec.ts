import { TestBed } from '@angular/core/testing';

import { MybookdetailsService } from './mybookdetails.service';

describe('MybookdetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MybookdetailsService = TestBed.get(MybookdetailsService);
    expect(service).toBeTruthy();
  });
});
