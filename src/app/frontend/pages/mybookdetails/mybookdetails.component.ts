import { BaseUrl } from './../../../base-url';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MybookdetailsService } from './mybookdetails.service';
import { SEOServiceService } from 'src/app/seoservice.service';
@Component({
  selector: "app-mybookdetails",
  templateUrl: "./mybookdetails.component.html",
  styleUrls: ["./mybookdetails.component.css"]
})
export class MybookdetailsComponent implements OnInit {
  bookId: any;
  bookData: any;
  previousUrl : any;
  _baseURL: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private mybookdetails: MybookdetailsService,
    private _seoService: SEOServiceService
  ) {
    this._baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.previousUrl = localStorage.getItem("previousUrl")
    this.bookId = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadAuthorData();
  }

  //load my book details
  loadAuthorData() {
    this.mybookdetails.getBookDetail({ book_id: this.bookId }).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        this.bookData = res.data;
        this._seoService.updateTitle(this.bookData.title);
        localStorage.setItem('mybooks', JSON.stringify(this.bookData));
      },
      error => {
        console.log("ERROR");
      }
    );
  }
}
