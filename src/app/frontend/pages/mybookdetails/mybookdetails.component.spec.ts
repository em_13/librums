import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MybookdetailsComponent } from './mybookdetails.component';

describe('MybookdetailsComponent', () => {
  let component: MybookdetailsComponent;
  let fixture: ComponentFixture<MybookdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MybookdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MybookdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
