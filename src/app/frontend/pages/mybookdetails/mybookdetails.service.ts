import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class MybookdetailsService {
  userdata: any;
  _baseURL: any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }
  getBookDetail(bookiid) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/getMyBookDetail`, bookiid, {
      headers: headers
    });
  }
}
