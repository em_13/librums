import { Component, OnInit } from "@angular/core";
import { SubscriptionService } from "./subscription.service";
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
declare var $: any;
@Component({
  selector: "app-subscription",
  templateUrl: "./subscription.component.html",
  styleUrls: ["./subscription.component.css"]
})
export class SubscriptionComponent implements OnInit {
  subscriptiondata: { type: string; value: unknown }[];
  plan_id:any;
  paypal_product_id: any;
  msg: any;
  is_subscribe = false;
  type_new :any;
  price_new: any;
  paypal_plan_new :any;
  paypal_product_new : any;

  constructor(private subscription: SubscriptionService,  private confirmationDialogService: ConfirmationDialogService) {}

  ngOnInit() {
    this.loadReport();
  }

  loadReport() {
    this.subscription.subscriptionList().subscribe(
      (res: any) => {
        if(res.status){
          for (let index = 0; index < res.data.length; index++) {
            if(res.data[index].is_subscribe){
              this.is_subscribe = true;
            }
          }
          this.subscriptiondata = res.data;
        }
        // console.log(res.data);
      },
      error => {
        console.log("ERROR", error);
      }
    );
  }

  onPayment(type, price, paypal_plan_id, paypal_product_id){
    if(this.is_subscribe){
      this.type_new = type;
      this.price_new = price;
      this.paypal_plan_new = paypal_plan_id;
      this.paypal_product_new = paypal_product_id;
      $("#myModal").modal('show');
    }else{
      this.startPayment(type, price, paypal_plan_id, paypal_product_id);
    }
  }

  startPayment(type, price, paypal_plan_id, paypal_product_id){
    $("#myModal").modal('hide');
    this.subscription.subscriptionRequest({plan_id: paypal_plan_id, product_id: paypal_product_id }).subscribe(
      (res: any) => {
        if (res.status) {
    
          let externalUrl = res.data.href;
          window.open(externalUrl, '_blank');
        
        } else {
        }
      },
      error => {
     
      }
    );
  }

  onCancel(type, price, paypal_plan_id, paypal_product_id){
    this.plan_id = paypal_plan_id;
    this.paypal_product_id = paypal_product_id;
    $("#save_box").modal('hide');
  }

  onCancelConfirm(){
    this.subscription.subscriptionRequestCancel({plan_id: this.plan_id}).subscribe(
      (res: any) => {
        if (res.status) {
          this.subscriptiondata = res.data;
          this.msg = res.message;
        } else {
        }
      },
      error => {
     
      }
    );
  }
}
