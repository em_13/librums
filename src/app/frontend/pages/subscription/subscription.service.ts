import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";

@Injectable({
  providedIn: "root"
})
export class SubscriptionService {
  subdata: any;
  _baseURL: string;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  subscriptionList() {
    this.subdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.subdata.data.token
    });
    return this.http.post(this._baseURL + "/subscriptionList", null, {
      headers
    });
  }

  subscriptionRequest(data) {
    this.subdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.subdata.data.token
    });
    return this.http.post(this._baseURL + "/subscribePlan", data, {
      headers
    });
  }

  subscriptionRequestCancel(data) {
    this.subdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.subdata.data.token
    });
    return this.http.post(this._baseURL + "/cancelPlan", data, {
      headers
    });
  }

}
