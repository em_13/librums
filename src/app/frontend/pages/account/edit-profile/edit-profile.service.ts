import { BaseUrl } from "./../../../../base-url";
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class EditProfileService {
  usersdata: any;
  updateuser: any;
  usersimage: any;
  baseurl: any;
  baseimage: any;
  constructor(private http: HttpClient) {
    this.baseurl = BaseUrl.frontend;
    this.baseimage = BaseUrl.imageApi;
  }

  getUserData() {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(this.baseurl + `/getUserProfile`, null, {
      headers
    });
  }

  //upload user image
  user_image(image) {
    this.usersimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": this.usersimage.data.token
    });
    return this.http.post(this.baseimage + `user_image`, image, {
      headers
    });
  }

  cover_image(image) {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(this.baseimage + `cover_image`, image, {
      headers
    });
  }

  updateUser(data) {
    this.updateuser = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.updateuser.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.baseurl + `/updateProfile`, data, {
      headers
    });
  }
}
