import { BaseUrl } from "./../../../../base-url";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { EditProfileService } from "./edit-profile.service";
import { Router } from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.component.html",
  styleUrls: ["./edit-profile.component.css"]
})
export class EditProfileComponent implements OnInit {
  updateForm: any;
  usersUpdate: any;
  submitted: any;
  loading = false;
  showMsg = false;
  errorMsg = false;
  selectedFiles: any;
  currentFileUpload: any;
  usersimage: any;
  baseimage: any;
  showLoader = false;
  myDate = new Date();
  myDates = new Date();
  modal: any;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    showTodayBtn: false,
    minYear : 1900,
    disableSince: {year: this.myDates.getFullYear(), month: this.myDates.getMonth() + 1, day: this.myDates.getDate()}
  };

  constructor(
    private formBuilder: FormBuilder,
    private editUserService: EditProfileService,
    private router: Router,
    private datePipe: DatePipe,
    private toastr: ToastrService
  ) {
    this.baseimage = BaseUrl.image;
  }
 

  ngOnInit() {
    this.getUserData();

    this.updateForm = this.formBuilder.group({
      dob: ["", Validators.required],
      username: ["", Validators.required],
      fullname: ["", Validators.required],
      email: ["", Validators.required],
      gender: [""],
      bio: [""],
      location: [""],
      user_image: [""],
      cover_image: [""]
    });

  }

  getUserData() {
    this.editUserService.getUserData().subscribe(
      (res: any) => {
        if (res.status) {
          if(res.data.dob){
            var array = res.data.dob.split('/');
            this.modal = { date: { year: array[2], month: array[1], day: array[0] } };
          }
          if(res.data.cover_image){
            res.data.coverimage = this.baseimage+''+res.data.cover_image;
          }else{
            res.data.coverimage = 'assets/img/bg_place_holder.jpg';
          }
          this.usersUpdate = res.data;
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }


  //on select image
  onSelectImage(event) {
    if (event.target.files.length > 0) {
      this.showLoader  = true;
      let formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.editUserService.user_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              $("#edit-profile-pic").attr('src', this.baseimage+'/'+res.data)
              $(".hide-o").hide();
              $(".header-user-pic").attr('src', this.baseimage+'/'+res.data)
              this.updateForm.controls["user_image"].setValue(res.data);
              this.showLoader  = false;
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  //on select cover image
  onSelectCoverImage(event) {
    if (event.target.files.length > 0) {
      this.showLoader  = true;
      let formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.editUserService.cover_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              $("#edit-cover-pic").attr('src', this.baseimage+'/'+res.data)
              this.updateForm.controls["cover_image"].setValue(res.data);
              this.showLoader  = false;
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

   // convenience getter for easy access to form fields
   // convenience getter for easy access to form fields
   get f() {
    return this.updateForm.controls;
  }

  //form submit
  onSubmit() {
    this.submitted = true;

    if (this.updateForm.invalid) {
      return;
    }
    if(this.updateForm.value.dob.date.day){
      this.updateForm.value.dob = this.updateForm.value.dob.date.day+"/"+this.updateForm.value.dob.date.month+"/"+this.updateForm.value.dob.date.year;
    }else{
      this.updateForm.value.dob = '';
    }
    if(this.updateForm.value.bio == undefined){
      this.updateForm.value.bio = '';
    }
    if(this.updateForm.value.gender == undefined){
      this.updateForm.value.gender = '';
    }
    if(this.updateForm.value.location == undefined){
      this.updateForm.value.location = '';
    }
    this.loading = true;
    this.editUserService.updateUser(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
           localStorage.setItem("currentUser", JSON.stringify(res));
          this.toastr.success('Profile updated succesfully.');
          this.router.navigate(["/myprofile"]);
          this.getUserData();
          console.log(res.data);
       
        } else {
          this.toastr.error(res.message);
         
        }
      },
      error => {
        this.loading = false;
      }
    );
  }
}
