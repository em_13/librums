import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/_services/alert.service';
import { ReportService } from '../../report/report.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  getreportissue: any;
  userdata : any;
  reportForm: FormGroup;
  loading = false;
  submitted = false;
  showMsg = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private reportService: ReportService,
    private toaster: ToastrService
  ) { }

  ngOnInit() {

    this.reportForm = this.formBuilder.group({
      type: ['', Validators.required],
      content: ['', Validators.required],
    });
 
    this.loadReport();
  }
  // convenience getter for easy access to form fields
  get f() { return this.reportForm.controls; }

 // submit issue form
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.reportForm.invalid) {
        return;
    }
    this.loading = true;
    this.reportService.feedback(this.reportForm.value)
        .pipe(first())
        .subscribe(
            data => {
                this.router.navigate(['/feedback']);
                this.toaster.success('Feedback submitted successfully.')
                setTimeout(() => {
                  this.router.navigate(["/"]);
                  })
                this.reset();
            },
            error => {
              this.toaster.error('Something went wrong.')
              this.loading = false;
            });
  }

  // reset form data
  private reset(){
    this.reportForm = this.formBuilder.group({
      type: [''],
      content: [''],
    });
  }

  //get report issue listing
  private loadReport() {
    this.userdata = JSON.parse(localStorage.getItem('currentUser'));
    this.reportService.getreportissue()
    .subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({type, value}));
        this.getreportissue = mapped;
      },
      error => {
        console.log("ERROR");
      });
  }


}

