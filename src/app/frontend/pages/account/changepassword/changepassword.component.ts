import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ChangepasswordService } from "./changepassword.service";
import { Router } from "@angular/router";
import { AlertService } from "src/app/_services/alert.service";
import { first } from "rxjs/operators";
import { MustMatch } from 'src/app/_helpers/must-match';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: "app-changepassword",
  templateUrl: "./changepassword.component.html",
  styleUrls: ["./changepassword.component.css"]
})
export class ChangepasswordComponent implements OnInit {
  changepasswrodForm: any;
  loading = false;
  submitted = false;
  showMsg = false;
  errorMsg = "";

  constructor(
    private formbuilder: FormBuilder,
    private changepassword: ChangepasswordService,
    private router: Router,
    private alertService: AlertService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.changepasswrodForm = this.formbuilder.group({
      old_password: ["", Validators.required],
      new_password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ["", Validators.required]
    },
    {
      validator: MustMatch('new_password', 'confirm_password')
  });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.changepasswrodForm.controls;
  }

  // submit issue form
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.changepasswrodForm.invalid) {
      return;
    }
    this.loading = true;

    this.changepassword.Change(this.changepasswrodForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success(res.message);
          // this.router.navigate(["/myprofile"]);
          this.reset();
          this.loading = false;
        } else {
          this.toaster.error(res.message);
          this.loading = false;
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
  // reset form data
  private reset() {
    this.changepasswrodForm = this.formbuilder.group({
      old_password: [""],
      new_password: [""],
      confirm_password: [""]
    });
  }
}
