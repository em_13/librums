import { BaseUrl } from "./../../../../base-url";
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ChangepasswordService {
  userdata: any;
  baseurl: any;
  constructor(private http: HttpClient) {
    this.baseurl = BaseUrl.frontend;
  }

  Change(data) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseurl + `/changePassword`, data, {
      headers
    });
  }
}
