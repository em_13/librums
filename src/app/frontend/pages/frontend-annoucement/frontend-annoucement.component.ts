import { Component, OnInit } from '@angular/core';
import { BaseUrl } from "./../../../base-url";
import { FrontendAnnoucmentService } from './frontend-annoucment.service';

@Component({
  selector: 'app-frontend-annoucement',
  templateUrl: './frontend-annoucement.component.html',
  styleUrls: ['./frontend-annoucement.component.css']
})
export class FrontendAnnoucementComponent implements OnInit {

  getnotification: any;
  baseimage:any;
  showLoader = false;
  constructor( private annoucement: FrontendAnnoucmentService) { 

    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.loadNotification();
  }

  loadNotification() {
    this.annoucement.getAnnoucement()
    .subscribe(
      (res: any) => {
       if(res.status){
         this.getnotification = res.data;
         this.showLoader = false;

         console.log(this.getnotification)
       }
      },
      error => {
        console.log("ERROR");
      });
  }
}
