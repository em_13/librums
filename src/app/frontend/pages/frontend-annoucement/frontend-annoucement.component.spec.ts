import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontendAnnoucementComponent } from './frontend-annoucement.component';

describe('FrontendAnnoucementComponent', () => {
  let component: FrontendAnnoucementComponent;
  let fixture: ComponentFixture<FrontendAnnoucementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontendAnnoucementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontendAnnoucementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
