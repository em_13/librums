import { TestBed } from '@angular/core/testing';

import { FrontendAnnoucmentService } from './frontend-annoucment.service';

describe('FrontendAnnoucmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FrontendAnnoucmentService = TestBed.get(FrontendAnnoucmentService);
    expect(service).toBeTruthy();
  });
});
