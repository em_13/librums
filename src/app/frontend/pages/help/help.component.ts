import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HelpService } from './help.service';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  reportForm: FormGroup;
  loading = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private help: HelpService,
    private toaster: ToastrService
  ) { }

  ngOnInit() {
    this.reportForm = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", Validators.required],
      subject: ["", Validators.required],
      message: ["", Validators.required]
    });
  }

    // convenience getter for easy access to form fields
    get f() {
      return this.reportForm.controls;
    }

      // submit issue form
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.reportForm.invalid) {
      return;
    }
    this.loading = true;
    this.help
      .report(this.reportForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.toaster.success('Your message has been successfully sent. We will contact you very soon!.', 'Thank you!')
          this.reset();
        },
        error => {
          this.toaster.error("Something went wrong.")
          this.loading = false;
        }
      );
  }

   // reset form data
   private reset() {
    this.reportForm = this.formBuilder.group({
      name: [""],
      email: [""],
      message: [""],
      subject: [""]
    });
  }

}
