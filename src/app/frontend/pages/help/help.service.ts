import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";
@Injectable({
  providedIn: 'root'
})
export class HelpService {
  _baseURL: string;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  report(data) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return this.http.post(this._baseURL + `/contactUs`, data, {
      headers: headers
    });
  }
}
