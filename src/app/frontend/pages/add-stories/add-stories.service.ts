import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AddStoriesService {
  getstory: any;
  baseurl: any;
  base: any;
  addstory: any;
  usersimage: any;
  constructor(private http: HttpClient) {
    this.baseurl = BaseUrl.frontend;
    this.base = BaseUrl.imageApi;
  }

  addStory(data) {
    this.addstory = JSON.parse(localStorage.getItem("currentUser"));
    const tags = [];
    data.tags.forEach(function(item) {
      tags.push(item.value);
    });

    data.tags = tags;
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.addstory.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.baseurl + `/addStory`, data, {
      headers
    });
  }

  getContentType() {
    this.getstory = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.getstory.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.baseurl + "/getContentType", null, {
      headers
    });
  }

  book_cover_image(image) {
    this.usersimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "multipart/form-data",
      "x-access-token": this.usersimage.data.token
    });
    return this.http.post(this.base + `book_cover_image`, image, {
      headers
    });
  }
}
