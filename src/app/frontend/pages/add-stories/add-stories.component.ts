import { Component, OnInit, AfterViewChecked } from "@angular/core";
import { AddStoriesService } from "./add-stories.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: "app-add-stories",
  templateUrl: "./add-stories.component.html",
  styleUrls: ["./add-stories.component.css"]
})
export class AddStoriesComponent implements OnInit {
  story: any;
  updateForm: any;
  loading = false;
  color = '#000000';
  submitted = false;
  showMsg = false;
  errorMsg: any;
  alertService: any;
  contanttype: any;
  selectedFiles: any;
  currentFileUpload: any;
  usersimage: any;
  imageSrc1: any;
  
  finalImage : any;
  
  constructor(
    private AddStoriesService: AddStoriesService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.loadStories();
    this.updateForm = this.formBuilder.group({
      title: ["", Validators.required],
      description: ["", Validators.required],
      content_type: ["", Validators.required],
      genre: ["", Validators.required],
      copyright: [""],
      tags: ["", Validators.required],
      not_for_below_eighteen: ["false"], 
      is_published: [2],
      cover_image: [""],
      image:[""]
    });
    $('#toggle-event').change(function() {
      console.log('Toggle: ' + $(this).prop('checked'));
      // this.updateForm.value.not_for_below_eighteen = $(this).prop('checked');
      $('#toggle-event').val($(this).prop('checked'));
    })
    $(window).on('load',function(){
      $('#toggle-event').bootstrapToggle('off');
    });
  }

  get f() {
    return this.updateForm.controls;
  }

  loadStories() {
    this.AddStoriesService.getContentType().subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        // console.log(mapped);
        this.contanttype = mapped;
       
      },
      () => {
        console.log("ERROR");
      }
    );
  }

 xyz(id){
  $( "#text_color" ).trigger( "change" );
 }
  onSelectCoverImage(event) {
    if (event.target.files.length > 0) {
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.AddStoriesService.book_cover_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              this.updateForm.controls["cover_image"].setValue(res.data);
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }
  saveCoverImage() {
    console.log(this.updateForm.value);
  var ImageURL = localStorage.getItem("image");
// Split the base64 string in data and contentType
var block = ImageURL.split(";");
// Get the content type of the image
var contentType = block[0].split(":")[1];// In this case "image/gif"
// get the real base64 content of the file
var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

var blob = this.b64toBlob(realData, contentType,'');
// Create a FormData and append the file with "image" as parameter name
var formDataToUpload = new FormData();
formDataToUpload.append("image", blob);
     
      this.AddStoriesService.book_cover_image(formDataToUpload).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              this.updateForm.controls["cover_image"].setValue(res.data);
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    
  }

   b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}
  
  onSubmit() {
   
    this.submitted = true;
    console.log( this.updateForm.value)
    this.updateForm.value.not_for_below_eighteen = ($('#toggle-event').prop('checked'))? 'true':'false';
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.loading = true;
    this.submitted = true;
    this.AddStoriesService.addStory(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
            this.toastr.success(res.message);
            localStorage.setItem('mybooks', JSON.stringify(res.data));
           
          setTimeout(() => {
             this.router.navigate(['/add-new-chapter/'+res.data._id]);
             
          }, 2000);  //5s
           } else {
          this.toastr.error(res.message);
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
