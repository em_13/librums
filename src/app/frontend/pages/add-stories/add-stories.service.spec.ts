import { TestBed } from '@angular/core/testing';

import { AddStoriesService } from './add-stories.service';

describe('AddStoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddStoriesService = TestBed.get(AddStoriesService);
    expect(service).toBeTruthy();
  });
});
