import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class PrivacyService {
  baseURL: string;

  constructor(private http: HttpClient) {
    this.baseURL = BaseUrl.frontend;
  }

  privacy() {
    return this.http.get(this.baseURL + `/privacypolicy`);
  }
}
