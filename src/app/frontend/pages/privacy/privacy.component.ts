import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { PrivacyService } from './privacy.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  privacy;
  constructor(
    private privacyService: PrivacyService
  ) { }

  ngOnInit() {
    this.loadPrivacy();
  }

  private loadPrivacy() {
    this.privacyService.privacy().pipe(first()).subscribe(privacy => {
        this.privacy = privacy;
    });
  }

}
