import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AllBooksService {

 userdata: any;
  _baseURL: any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }
 
  gethomedata(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/getBookListing`, postData, {
      headers
    });
  }
 
  loadMoreBooks(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/loadMoreBooks`, postData, {
      headers
    });
  }
}
