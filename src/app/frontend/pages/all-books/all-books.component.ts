import { BaseUrl } from "./../../../base-url";
import { Component, OnInit, AfterViewChecked } from "@angular/core";
import { AllBooksService } from "./all-books.service";
import { ActivatedRoute } from "@angular/router";
import { Route } from '@angular/compiler/src/core';
import { Router } from "@angular/router";
import { SEOServiceService } from 'src/app/seoservice.service';
declare var $: any;

@Component({
  selector: 'app-all-books',
  templateUrl: './all-books.component.html',
  styleUrls: ['./all-books.component.css']
})
export class AllBooksComponent implements OnInit {

  /**scrol */
  page_no = 0;
  array = [];
  sum = 100;
  classes = 'col-lg-6 col-xl-6';
  // throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  modalOpen = false;
  /**scrol */
  activetab: any;
  alsoLikes: any;
  gethomedata: any;
  type: any;
  baseURL: any;
  showLoader = false;
  pagenumer = 0;
  pageNumber = 10;
  textUnder: any;
  SlideOptions = {
    stagePadding: 200,
    loop: true,
    margin: 10,
    nav: false,
    dots: false,
    items: 1,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
        nav: false,
        stagePadding: 60
      },
      600: {
        items: 1,
        nav: false,
        stagePadding: 100
      },
      1000: {
        items: 1,
        nav: false,
        stagePadding: 200
      },
      1200: {
        items: 1,
        nav: false,
        stagePadding: 250
      },
      1400: {
        items: 1,
        nav: false,
        stagePadding: 300
      },
      1600: {
        items: 1,
        nav: false,
        stagePadding: 350
      },
      1800: {
        items: 1,
        nav: false,
        stagePadding: 450
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: false };
  constructor(
    private homeDetailService: AllBooksService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _seoService: SEOServiceService
  ) {
    this.baseURL = BaseUrl.image;
  }

  ngOnInit() {
    // this.type = this.activatedRoute.snapshot.paramMap.get("id");
    this.type = this.activatedRoute.snapshot.url[1].path;
    if (this.type == 'new_stories') {
      this._seoService.updateTitle('New Stories');
      this.textUnder = 'A fresh set of stories, just for you';
    } else if (this.type == 'last_updated') {
      this._seoService.updateTitle('Last Updated');
      this.textUnder = 'Check out the latest updates, just for you';
    } else if (this.type == 'most_liked') {
      this._seoService.updateTitle('Most Liked');
      this.textUnder = 'A most liked set of stories, just for you';
    } else if (this.type == 'most_read') {
      this._seoService.updateTitle('Most Read');
      this.textUnder = 'A most read set of stories, just for you';
    } else {
      this._seoService.updateTitle('New Stories');
      this.textUnder = 'A fresh set of stories, just for you';
    }
    // console.log(this.type);
    // this.activetab = this.activatedRoute.snapshot.paramMap.get("tabname");
    this.showLoader = true;
    this.loadHome();
    this.appendItems(0, this.sum);

    $('#right-button').click(function () {
      event.preventDefault();
      $('#custom_tabs_pills').animate({
        scrollLeft: "+=200px"
      }, "slow");
    });

    $('#left-button').click(function () {
      event.preventDefault();
      $('#custom_tabs_pills').animate({
        scrollLeft: "-=200px"
      }, "slow");
    });
  }

  ngAfterViewChecked() {
    $('.flipster').each(function (index, value) {
      $("#coverflow" + index).flipster({
        style: 'carousel',
        spacing: -0.5,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    });
    $("#alsolike").flipster({

      style: 'carousel',
      spacing: -0.5,
      buttons: true,
      start: 'center',
      loop: true,
      autoplay: false,
      click: true,
      scrollwheel: false,
    });
    $('#left-button').click(function () {
      event.preventDefault();
      $('.custom_tabs_pills').animate({
        scrollLeft: "-=300px"
      }, "slow");
    });

    $('#right-button').click(function () {
      event.preventDefault();
      $('.custom_tabs_pills').animate({
        scrollLeft: "+=300px"
      }, "slow");
    });

  }

  loadHome() {
    this.homeDetailService.gethomedata({ type: this.type }).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        console.log(mapped);
        this.showLoader = false;
        this.gethomedata = mapped;
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  searchTag(text) {
    localStorage.setItem("search", text);
    this.router.navigate(["/search"]);
  }

  onExploreHome(id) {
    if ($("#coverflow" + id).find('.flip-items').find('li').length == 1) {
      $(".flip-items").css({ "width": "700px", "margin": "0 auto" })
    } else {
      $(".flip-items").css({ "width": "auto", "margin": "0" })
      $("#coverflow" + id).flipster({
        style: 'carousel',
        spacing: -0.3,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    }
  }

  /**Scroll */
  addItems(startIndex, endIndex, _method) {
    for (let i = 0; i < this.sum; ++i) {
      this.array[_method]([i, ' ', this.generateWord()].join(''));
    }
  }

  appendItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'push');
  }

  prependItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'unshift');
  }

  onScrollDown() {
    this.pagenumer = this.pagenumer + 1;
    let id = $(".nav-link.active").data('sectionvalue');
    let tabnamse = $(".nav-link.active").data('tabename');
    this.homeDetailService.loadMoreBooks({ genre_name: tabnamse, genre_id: id, type: this.type, page_no: this.pagenumer }).subscribe(
      (res: any) => {
        if (res.status) {
          this.pageNumber = this.pageNumber + 10;
          for (let i = 0; i < this.gethomedata.length; i++) {
            if (this.gethomedata[i].type == 'data') {
              for (let index = 0; index < this.gethomedata[i].value.length; index++) {
                if (this.gethomedata[i].value[index].tag == tabnamse) {
                  let resultArr = [];
                  resultArr = this.gethomedata[i].value[index].data.concat(res.data.data);
                  this.gethomedata[i].value[index].data = resultArr;
                }
              }
            }
          }
          console.log(this.gethomedata)
        }
      },
      error => {
        console.log("ERROR");
      }
    );
    // add another 20 items
    const start = this.sum;
    this.sum += 20;
    this.appendItems(start, this.sum);

    this.direction = 'down'
  }

  onUp(ev) {
    console.log('scrolled up!', ev);
    const start = this.sum;
    this.sum += 20;
    this.prependItems(start, this.sum);

    this.direction = 'up';
  }
  generateWord() {
    return Math.floor(Math.random() * 6) + 1;
  }
  onGrid(){
    this.classes = 'col-lg-6 col-xl-6';
  }
  onList(){
    this.classes = 'col-lg-12 col-xl-12';
  }
  /**Scroll */

}
