import { BaseUrl } from "./../../../base-url";
import { Component, OnInit, AfterViewChecked } from "@angular/core";
import { HomeDetailService } from "./home-detail.service";
import { ActivatedRoute } from "@angular/router";
import { Route } from '@angular/compiler/src/core';
import { Router } from "@angular/router";
import { SEOServiceService } from 'src/app/seoservice.service';
declare var $: any;
@Component({
  selector: "app-home-detail",
  templateUrl: "./home-detail.component.html",
  styleUrls: ["./home-detail.component.css"]
})
export class HomeDetailComponent implements OnInit {

  /**scrol */
  page_no = 0;
  array = [];
  sum = 100;
  // throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  modalOpen = false;
  /**scrol */
  activetab: any;
  alsoLikes: any;
  gethomedata: any;
  type: any;
  baseURL: any;
  showLoader = false;
  pagenumer = 0;
  pageNumber = 10;
  textUnder: any;
  SlideOptions = {
    stagePadding: 120,
    loop: false,
    nav: false,
    dots: false,
    items: 1,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
        nav: false,
      },
      600: {
        items: 1,
        nav: false,
      },
      1000: {
        items: 1,
        nav: false,
      },
      1500: {
        stagePadding: 350,
        items: 1
      },
      1600: {
        stagePadding: 350,
        items: 1
      },
      1700: {
        stagePadding: 400,
        items: 1
      },
      1800: {
        stagePadding: 450,
        items: 1
      },
      1900: {
        stagePadding: 480,
        items: 1
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: false };
  constructor(
    private homeDetailService: HomeDetailService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _seoService: SEOServiceService
  ) {
    this.baseURL = BaseUrl.image;
  }

  ngOnInit() {
    // this.type = this.activatedRoute.snapshot.paramMap.get("id");
    this.type = this.activatedRoute.snapshot.url[0].path;
    if (this.type == 'new_stories') {
      this._seoService.updateTitle('New Stories');
      this.textUnder = 'A fresh set of stories, just for you';
    } else if (this.type == 'last_updated') {
      this._seoService.updateTitle('Last Updated');
      this.textUnder = 'Check out the latest updates, just for you';
    } else if (this.type == 'most_liked') {
      this._seoService.updateTitle('Most Liked');
      this.textUnder = 'A most liked set of stories, just for you';
    } else if (this.type == 'most_read') {
      this._seoService.updateTitle('Most Read');
      this.textUnder = 'A most read set of stories, just for you';
    } else {
      this._seoService.updateTitle('New Stories');
      this.textUnder = 'A fresh set of stories, just for you';
    }
    // console.log(this.type);
    // this.activetab = this.activatedRoute.snapshot.paramMap.get("tabname");
    this.showLoader = true;
    this.loadHome();
    this.AlsoLike();
    this.appendItems(0, this.sum);

    $('#right-button').click(function () {
      event.preventDefault();
      $('#custom_tabs_pills').animate({
        scrollLeft: "+=200px"
      }, "slow");
    });

    $('#left-button').click(function () {
      event.preventDefault();
      $('#custom_tabs_pills').animate({
        scrollLeft: "-=200px"
      }, "slow");
    });
  }

  ngAfterViewChecked() {
    $('.flipster').each(function (index, value) {
      $("#coverflow" + index).flipster({
        style: 'carousel',
        spacing: -0.3,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    });
    $("#alsolike").flipster({

      style: 'carousel',
      spacing: -0.3,
      buttons: true,
      start: 'center',
      loop: true,
      autoplay: false,
      click: true,
      scrollwheel: false,
    });
    $('#left-button').click(function () {
      event.preventDefault();
      $('.custom_tabs_pills').animate({
        scrollLeft: "-=300px"
      }, "slow");
    });

    $('#right-button').click(function () {
      event.preventDefault();
      $('.custom_tabs_pills').animate({
        scrollLeft: "+=300px"
      }, "slow");
    });

  }

  loadHome() {
    this.homeDetailService.gethomedata({ type: this.type }).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        console.log(mapped);
        this.showLoader = false;
        this.gethomedata = mapped;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  AlsoLike() {
    this.homeDetailService.getAlsoLike().subscribe(
      (res: any) => {

        this.showLoader = false;
        this.alsoLikes = res.data;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  searchTag(text) {
    localStorage.setItem("search", text);
    this.router.navigate(["/search"]);
  }

  onExploreHome(id) {
    if ($("#coverflow" + id).find('.flip-items').find('li').length == 1) {
      $(".flip-items").css({ "width": "700px", "margin": "0 auto" })
    } else {
      $(".flip-items").css({ "width": "auto", "margin": "0" })
      $("#coverflow" + id).flipster({
        style: 'carousel',
        spacing: -0.3,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    }
  }

  /**Scroll */
  addItems(startIndex, endIndex, _method) {
    for (let i = 0; i < this.sum; ++i) {
      this.array[_method]([i, ' ', this.generateWord()].join(''));
    }
  }

  appendItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'push');
  }

  prependItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'unshift');
  }

  onScrollDown() {
    this.pagenumer = this.pagenumer + 1;
    let id = $(".nav-link.active").data('sectionvalue');
    let tabnamse = $(".nav-link.active").data('tabename');
    this.homeDetailService.loadMoreBooks({ genre_name: tabnamse, genre_id: id, type: this.type, page_no: this.pagenumer }).subscribe(
      (res: any) => {
        if (res.status) {
          this.pageNumber = this.pageNumber + 10;
          for (let i = 0; i < this.gethomedata.length; i++) {
            if (this.gethomedata[i].type == 'data') {
              for (let index = 0; index < this.gethomedata[i].value.length; index++) {
                if (this.gethomedata[i].value[index].tag == tabnamse) {
                  let resultArr = [];
                  resultArr = this.gethomedata[i].value[index].data.concat(res.data.data);
                  this.gethomedata[i].value[index].data = resultArr;
                }
              }
            }
          }
          console.log(this.gethomedata)
        }
      },
      error => {
        console.log("ERROR");
      }
    );
    // add another 20 items
    const start = this.sum;
    this.sum += 20;
    this.appendItems(start, this.sum);

    this.direction = 'down'
  }

  onUp(ev) {
    console.log('scrolled up!', ev);
    const start = this.sum;
    this.sum += 20;
    this.prependItems(start, this.sum);

    this.direction = 'up';
  }
  generateWord() {
    return Math.floor(Math.random() * 6) + 1;
  }
  /**Scroll */

}
