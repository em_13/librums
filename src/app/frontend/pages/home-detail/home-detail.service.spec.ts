import { TestBed } from '@angular/core/testing';

import { HomeDetailService } from './home-detail.service';

describe('HomeDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HomeDetailService = TestBed.get(HomeDetailService);
    expect(service).toBeTruthy();
  });
});
