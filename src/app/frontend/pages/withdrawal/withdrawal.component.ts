import { Component, OnInit } from '@angular/core';
import { WithdrawalService } from './withdrawal.service';
import { BaseUrl } from "../../../base-url";
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.css']
})
export class WithdrawalComponent implements OnInit {
  paymenthistory: any;
  userdata: any;
  _baseURL: any;
  emailCheck = true;
  phoneCheck = false;
  paypalCheck = false;
  paymentForm: any;
  submitted = false;
  loading = false;
  total:any;
  checkError = false;
  checkSuccess = false;
  checkErrorInvalid = false;
  public temp: Object = false;
  constructor(private withdrawal: WithdrawalService,
    private toaster: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router,
    ) {
    this._baseURL = BaseUrl.image;
   }

  ngOnInit() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));

    this.paymentForm = this.formBuilder.group({
      amount: ["", [Validators.required, Validators.min(10),Validators.max(this.total) ]],
      type: ["", Validators.required],
      value: ["", Validators.required],
    });

    this.paymentHistory();
  }


    paymentHistory(){
      this.withdrawal.paymentHistory().subscribe(
        (res: any) => {
          if(res.status){
            this.paymenthistory = res.data.data;
            this.total = res.data.total;
            this.temp = true;
          }else{
            this.paymenthistory = '';
            this.total = 0;
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }

  onItemChange(val){
    if(val == 'PAYPAL_ID'){
      this.paypalCheck = true;
      this.phoneCheck = false;
      this.emailCheck = false;
    }else if(val == 'PHONE'){
      this.phoneCheck = true;
      this.paypalCheck = false;
      this.emailCheck = false;
    }else{
      this.emailCheck = true;
      this.phoneCheck = false;
      this.paypalCheck = false;
    }
  }
    // convenience getter for easy access to form fields
    get f() {
      return this.paymentForm.controls;
    }

    transform(rawNum) {
      rawNum = rawNum.charAt(0) != 0 ? "0" + rawNum : "" + rawNum;
  
      let newStr = "";
      let i = 0;
  
      for (; i < Math.floor(rawNum.length / 2) - 1; i++) {
        newStr = newStr + rawNum.substr(i * 2, 2) + "-";
      }
  
      return newStr + rawNum.substr(i * 2);
    }

  onSubmit(){
    this.submitted = true;
    // stop here if form is invalid
    if (this.paymentForm.invalid) {
      return;
    }
    if(this.total < this.paymentForm.value.amount){
      this.toaster.error('Withdrawal amount must be less then wallet amount.')
      return;
    }
    if(this.paymentForm.value.type == 'PHONE'){
      this.paymentForm.value.value = this.transform(this.paymentForm.value.value);
    }
  
    this.loading = true;
    this.withdrawal.withdrawalpayment(this.paymentForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success('Payment withdrawn successfully.')
          this.loading = false;
          $("#withdrawal_payment").modal('hide');
          this.paymentHistory();
        } else {
          this.loading = false;
          this.toaster.error('Receiver is invalid or does not match with type.')
        }
      },
      error => {
        this.loading = false;
      }
    );

  }

}
