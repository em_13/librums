import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BaseUrl } from "./../../../base-url";
@Injectable({
  providedIn: 'root'
})
export class WithdrawalService {
  userdata: any;
  baseuel: string;
  constructor(private http: HttpClient) { 
    this.baseuel = BaseUrl.frontend;
  }

  paymentHistory(){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/paymentHistory`, null, {
      headers: headers
    });

  }

  withdrawalpayment(data){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/withdrawl`, data, {
      headers: headers
    });
  }
}
