import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AddStoriesService } from '../add-stories/add-stories.service';
import { EditstoriesService } from './editstories.service';
import { BaseUrl } from "./../../../base-url";
import { ToastrService } from 'ngx-toastr';
import { SEOServiceService } from 'src/app/seoservice.service';
declare var $:any;
@Component({
  selector: 'app-editstories',
  templateUrl: './editstories.component.html',
  styleUrls: ['./editstories.component.css']
})
export class EditstoriesComponent implements OnInit {

  story: any;
  color = '#000000';
  updateForm: any;
  loading = false;
  submitted = false;
  showMsg = false;
  errorMsg: any;
  isBlocked : any;
  alertService: any;
  contanttypes: any;
  selectedFiles: any;
  currentFileUpload: any;
  usersimage: any;
  imageSrc1: any;
  loadEditBook: any;
  baseURL: any;
  finalImage : any;
  book_id: any;
  showLoader = false;

  constructor(
    private AddStoriesService: AddStoriesService,
    private router: Router,
    private formBuilder: FormBuilder,
    private editStoriesService: EditstoriesService,
    private activeRoute: ActivatedRoute, 
    private toaster: ToastrService,
    private _seoService: SEOServiceService
  ) {
    this.baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.baseURL = BaseUrl.image;
    this.book_id = this.activeRoute.snapshot.paramMap.get("id");

    this.loadStories();
    this.updateForm = this.formBuilder.group({
      title: ["", Validators.required],
      description: ["", Validators.required],
      content_type: ["", Validators.required],
      genre: ["", Validators.required],
      copyright: [""],
      tags: ["", Validators.required],
      not_for_below_eighteen: ["false"],
      is_published: [1],
      cover_image: [""],
      book_id:[""]
    });

    this.loadBook();
    $(window).on('load',function(){
    $("#cover_image3").cropzee({
      aspectRatio: 1.33,
    });
    $("#cover_image4").cropzee({ 
      aspectRatio: 1.33,
                  
    });
    });
    $('#toggle-event').change(function() {
      console.log('Toggle: ' + $(this).prop('checked'));
      // this.updateForm.value.not_for_below_eighteen = $(this).prop('checked');
      $('#toggle-event').val($(this).prop('checked'));
        })
  }

  get f() {
    return this.updateForm.controls;
  }

  loadStories() {
    this.AddStoriesService.getContentType().subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        // console.log(mapped);
        this.contanttypes = mapped;
       
      },
      () => {
        console.log("ERROR");
      }
    );
  }

  loadBook(){
    
    this.editStoriesService.getEditBook({'book_id': this.book_id}).subscribe(
      (res: any) => {
        if(res.status){
          const tags = [];
          res.data.tags.forEach(function(item) {
            tags.push({'display': item, 'value': item});
          });
          res.data.tags = tags;

          let str = res.data.description;
          let removestr = 'So, '+res.data.title+':';
          let desc = str.replace(removestr, '');
          res.data.description = desc;
         
          if(res.data.not_for_below_eighteen){
            $('#toggle-event').bootstrapToggle('on');
          }else{
            $('#toggle-event').bootstrapToggle('off');
          }
          this.updateForm.value.cover_image = res.data.cover_image;
          this.isBlocked = res.data.is_blocked;
          this.loadEditBook = res.data
          this._seoService.updateTitle(this.loadEditBook.title);
          this.showLoader = false;
        }
      },
      error => {}
    );


  }

  xyz(id){
    $( "#text_color" ).trigger( "change" );
   }
  onSelectCoverImage(event) {
    if (event.target.files.length > 0) {
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.AddStoriesService.book_cover_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }
  saveCoverImage() {
  var ImageURL = localStorage.getItem("image");
// Split the base64 string in data and contentType
var block = ImageURL.split(";");
// Get the content type of the image
var contentType = block[0].split(":")[1];// In this case "image/gif"
// get the real base64 content of the file
var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

var blob = this.b64toBlob(realData, contentType,'');
// Create a FormData and append the file with "image" as parameter name
var formDataToUpload = new FormData();
formDataToUpload.append("image", blob);
     
      this.AddStoriesService.book_cover_image(formDataToUpload).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              this.updateForm.controls["cover_image"].setValue(res.data);
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    
  }

   b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}
  
  onSubmit() {
    this.updateForm.value.not_for_below_eighteen = ($('#toggle-event').prop('checked'))? 'true':'false';
   
    console.log(this.updateForm.value)
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.loading = true;
    this.submitted = true;
    // this.updateForm.value.not_for_below_eighteen = (this.updateForm.value.not_for_below_eighteen)? 'true':'false';
    this.updateForm.value.book_id = this.book_id;
    this.editStoriesService.updateStory(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success(res.message);
          if(this.isBlocked){
            setTimeout(() => {
              this.router.navigate(['/contact-us']);
            }, 3000);  //5s
          }else{

            setTimeout(() => {
              this.router.navigate(['/mystories']);
            }, 3000);  //5s
          }
           } else {
            this.toaster.error(res.message);
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

}
