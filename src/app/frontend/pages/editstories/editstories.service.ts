import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BaseUrl } from "./../../../base-url";
@Injectable({
  providedIn: 'root'
})
export class EditstoriesService {
  baseURL: any;
  user: any;
  constructor( private http: HttpClient) { 
    this.baseURL = BaseUrl.frontend;
  }

  getEditBook(data){
    this.user = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.user.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.baseURL + "/getMyBookDetail", data, {
      headers
    });
  }

  updateStory(data) {
    this.user = JSON.parse(localStorage.getItem("currentUser"));
    const tags = [];
    data.tags.forEach(function(item) {
      tags.push(item.value);
    });

    data.tags = tags;
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.user.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.baseURL + `/updateStory`, data, {
      headers
    });
  }

}
