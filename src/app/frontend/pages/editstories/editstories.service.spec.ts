import { TestBed } from '@angular/core/testing';

import { EditstoriesService } from './editstories.service';

describe('EditstoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditstoriesService = TestBed.get(EditstoriesService);
    expect(service).toBeTruthy();
  });
});
