import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditstoriesComponent } from './editstories.component';

describe('EditstoriesComponent', () => {
  let component: EditstoriesComponent;
  let fixture: ComponentFixture<EditstoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditstoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditstoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
