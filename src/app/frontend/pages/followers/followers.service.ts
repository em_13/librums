import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class FollowersService {
  userdata: any;
  baseURL: any;

  constructor(private http: HttpClient) {
    this.baseURL = BaseUrl.frontend;
  }

  getFollowers() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + `/getFollower`, null, { headers });
  }

  followUser(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });

    return this.http.post(this.baseURL + `/followUser`, postData, { headers });
  }
}
