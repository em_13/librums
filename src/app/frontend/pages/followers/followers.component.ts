import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { FollowersService } from "./followers.service";
import { AlertService } from "src/app/_services/alert.service";
import { first } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: "app-followers",
  templateUrl: "./followers.component.html",
  styleUrls: ["./followers.component.css"]
})
export class FollowersComponent implements OnInit {
  followers = [];
  length: any;
  baseimage: any;
  msg: any;
  errprmsg: any;
  previousUrl : any;
  constructor(
    private followersService: FollowersService,
    private alertService: AlertService,
    private toaster: ToastrService
  ) {
    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.loadFollowers();
    this.previousUrl = localStorage.getItem("previousUrl")
  }
  loadFollowers() {
    this.followersService.getFollowers().subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        console.log(mapped);
        this.followers = mapped;
      },
      error => {
        console.log("ERROR", error);
      }
    );
  }
  followUser(e) {
    this.followersService
      .followUser({ status: e.target.dataset.status, user_id: e.target.dataset.user})
      .pipe(first())
      .subscribe(
        (res: any) => {
          if(res.status){
            // this.toaster.success(res.message);
            if(e.target.dataset.status == 2){
              $(".user_"+e.target.dataset.user).attr('data-status', 1);
            }else{
              $(".user_"+e.target.dataset.user).attr('data-status', 2);
            }

            if( $(".user_"+e.target.dataset.user).text() == 'Following'){
              $(".user_"+e.target.dataset.user).text('Follow') 
              $(".user_"+e.target.dataset.user).removeClass('following_btn') 
             }else{
              $(".user_"+e.target.dataset.user).text('Following') 
              $(".user_"+e.target.dataset.user).addClass('following_btn') 
             }
          }else{
            this.toaster.error(res.message);
          }
        },
        error => {
          console.log("ERROR", error);
        }
      );
  }
}
