import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewChapterComponent } from './preview-chapter.component';

describe('PreviewChapterComponent', () => {
  let component: PreviewChapterComponent;
  let fixture: ComponentFixture<PreviewChapterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewChapterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewChapterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
