import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-preview-chapter',
  templateUrl: './preview-chapter.component.html',
  styleUrls: ['./preview-chapter.component.css']
})
export class PreviewChapterComponent implements OnInit {
  chapterData: any;
  safeurl: any;
  is_white = "color_white";
  constructor(
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
     let pages = localStorage.getItem('previewPage');
     this.chapterData = JSON.parse(pages)
     console.log("------------",this.chapterData.video)
     if(this.chapterData.video !== 'about:blank'){
       this.safeurl = this._sanitizer.bypassSecurityTrustResourceUrl(this.chapterData.video);
     }else{
this.safeurl = '';
     }
     console.log('sdfbdsjbfjdsjk'+this.safeurl);
     console.log(this.chapterData)
     $(".medium-insert-buttons").remove();
  }

  changeLayout(item){
    $(".change_chapter_color_black").removeClass('color_black');
    $(".change_chapter_color_black").removeClass('color_wheat');
    $(".change_chapter_color_black").removeClass('color_white');
    
    $(".change_chapter_color_black").addClass(item)
  }
  onFontSize(size){
    $(".change_chapter_color_black p").css('font-size', size+'px')
  }
  onFontChange(font){
    console.log(font)
    $(".change_chapter_color_black p").css('font-family', font)
  }
}
