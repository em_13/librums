import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";

@Injectable({
  providedIn: "root"
})
export class SearchService {
  searchdata: any;
  _baseURL: string;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  searchBook(data) {
    this.searchdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.searchdata.data.token
    });
    return this.http.post(this._baseURL + "/searchBook", data, {
      headers
    });
  }
  getSearchList(data) {
    this.searchdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.searchdata.data.token
    });
    return this.http.post(this._baseURL + "/getSearchList", data, {
      headers
    });
  }

  loadMoreBooks(data){
    this.searchdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.searchdata.data.token
    });
    return this.http.post(this._baseURL + "/searchBook", data, {
      headers
    });
  }
}
