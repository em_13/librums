import { Component, OnInit } from "@angular/core";
import { SearchService } from "./search.service";
import { AlertService } from "src/app/_services/alert.service";
import { BaseUrl } from "../../../base-url";
import { Router } from '@angular/router';
import { fakeAsync } from '@angular/core/testing';
import { ExploreService } from '../explore/explore.service';
declare var $:any;
@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.css"]
})
export class SearchComponent implements OnInit {
  showMsg = false;
  baseURL: string;
  returnData : any;
  searchList : any;
  clickSearch = false;
  pagenumer = 0;
  classes = 'col-lg-6 col-xl-6';
  pageNumber = 10;
  SlideOptions = {
    stagePadding: 200,
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    items: 1,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
        nav: false,
        stagePadding: 60
      },
      600: {
        items: 1,
        nav: false,
        stagePadding: 100
      },
      1000: {
        items: 1,
        nav: false,
        stagePadding: 200
      },
      1200: {
        items: 1,
        nav: false,
        stagePadding: 250
      },
      1400: {
        items: 1,
        nav: false,
        stagePadding: 300
      },
      1600: {
        items: 1,
        nav: false,
        stagePadding: 350
      },
      1800: {
        items: 1,
        nav: false,
        stagePadding: 400
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: false };
  searchValue: { type: string; value: unknown }[];
  errorMsg: any;
  nodatafound = false;
  loading: boolean;
  constructor(
    private Search: SearchService,
    private alertService: AlertService,
    private exploreS: ExploreService,
    
    private router : Router
  ) {
    this.baseURL = BaseUrl.image;
  }

  // tslint:disable-next-line: align

  ngOnInit() {
    this.nodatafound = true;
    let search = localStorage.getItem("search");
    if(search){
      this.onSearchChange(search);
      $('#search').val(search);
      localStorage.removeItem("search");
    }else{
      this.onSearchChange('');
    }
    
    setTimeout(function(){ 
      var $item = $('li.nav-item'), //Cache your DOM selector
      visible = 2, //Set the number of items that will be visible
      index = 0, //Starting index
      endIndex = ( $item.length / visible ) - 1; //End index
      
      $('#right-button').click(function(){
      if(index < endIndex ){
        index++;
        $item.animate({'left':'-=100px'});
      }
      });
      
      $('#left-button').click(function(){
      if(index > 0){
        index--;            
        $item.animate({'left':'+=100px'});
      }
      });
      
    
     }, 3000);
  }

  ngAfterViewChecked(){
    $('.flipster').each(function(index, value) {
      $("#coverflow"+index).flipster({
        style: 'carousel',
        spacing: -0.5,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    });
    $('#left-button').click(function() {
      event.preventDefault();
      $('.custom_tabs_pills').animate({
        scrollLeft: "-=300px"
      }, "slow");
    });
    
     $('#right-button').click(function() {
      event.preventDefault();
      $('.custom_tabs_pills').animate({
        scrollLeft: "+=300px"
      }, "slow");
    });

  }
  searchTag(text) {
    // localStorage.setItem("search",text);
    this.onSearchChangeClick(text);
    this.searchList = '';
    this.clickSearch = true;
    $('#search').val(text);
  }
  searchTagList(text) {
    // localStorage.setItem("search",text);
    
    this.searchList = '';
    this.clickSearch = true;
    $('#search').val(text);
  }

  getSearchList(searchValue: any): void {
    this.Search.getSearchList({ 'device_type':'web', search: searchValue }).subscribe(
      (res: any) => {
        if (res.status) {
          console.log(res.data);
          this.searchList = res.data;
          this.nodatafound = false;
        } else {
          this.searchList = '';
          this.nodatafound = true;
          this.errorMsg = res.message;
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
      );
    

  }
  onSearchSunmit(){
  
    this.onSearchChange($('#search').val());
  }
  onSearchChange(searchValue: any): void {
    // if(this.clickSearch){
// this.clickSearch = false;
    // }else{

      // this.getSearchList(searchValue);
    // }
    if(searchValue !== ''){

      this.Search.searchBook({ 'device_type':'web', search: searchValue }).subscribe(
        (res: any) => {
          if (res.status) {
            const mapped = Object.entries(res.data).map(([type, value]) => ({
              type,
              value
            }));
            this.pagenumer = 0;
            this.returnData = mapped;
            this.showMsg = true;
            this.nodatafound = false;
          } else {
            this.returnData = '';
            this.nodatafound = true;
            this.errorMsg = res.message;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
        );
      }else{
        this.exploreS.explore({ type: 'explore' }).subscribe( 
          (res: any) => {
            if (res.status) {
              const mapped = Object.entries(res.data).map(([type, value]) => ({
                type,
                value
              }));
              this.pagenumer = 0;
              this.returnData = mapped;
              this.showMsg = true;
              this.nodatafound = false;
            } else {
              this.returnData = '';
              this.nodatafound = true;
              this.errorMsg = res.message;
            }
          },
          error => {
            console.log("ERROR");
          }
        );
      }
      }
      onSearchChangeClick(searchValue: any): void {
        // if(this.clickSearch){
    // this.clickSearch = false;
        // }else{
    
          // this.getSearchList(searchValue);
        // }
        if(searchValue !== ''){
    
          this.Search.searchBook({ 'device_type':'web', search: searchValue }).subscribe(
            (res: any) => {
              if (res.status) {
                const mapped = Object.entries(res.data).map(([type, value]) => ({
                  type,
                  value
                }));
                this.pagenumer = 0;
                this.returnData = mapped;
                this.showMsg = true;
                this.nodatafound = false;
              } else {
                this.returnData = '';
                this.nodatafound = true;
                this.errorMsg = res.message;
              }
            },
            error => {
              this.alertService.error(error);
              this.loading = false;
            }
            );
          }else{
            this.exploreS.explore({ type: 'explore' }).subscribe( 
              (res: any) => {
                if (res.status) {
                  const mapped = Object.entries(res.data).map(([type, value]) => ({
                    type,
                    value
                  }));
                  this.pagenumer = 0;
                  this.returnData = mapped;
                  this.showMsg = true;
                  this.nodatafound = false;
                } else {
                  this.returnData = '';
                  this.nodatafound = true;
                  this.errorMsg = res.message;
                }
              },
              error => {
                console.log("ERROR");
              }
            );
          }
          }

  onScrollDown () {
    this.pagenumer = this.pagenumer+1;
    let id = $(".nav-link.active").data('sectionvalue');
    let tabnamse = $(".nav-link.active").data('tabename');
    let search = $("#search").val();
    if(search !== ''){
    this.Search.loadMoreBooks({genre_name:tabnamse, genre_id: id, page_no:this.pagenumer, search:search }).subscribe( 
      (res: any) => {
        if(res.status){
          // console.log(res.data)
          this.pageNumber = this.pageNumber+10;
          for( let i=0; i<this.returnData.length; i++){
            if(this.returnData[i].type == 'data'){
              for (let index = 0; index < this.returnData[i].value.length; index++) {
                  if(this.returnData[i].value[index].tag == tabnamse){
                    // console.log(this.returnData[i].value[index].data)
                    let resultArr = [];
                    resultArr= this.returnData[i].value[index].data.concat(res.data);
                    this.returnData[i].value[index].data = resultArr;
                  } 
              }
            }
          }
          // console.log(this.returnData)
        }
      },
      error => {
        console.log("ERROR");
      }
    );
    }else{
      this.exploreS.loadMoreBooks({genre_name:tabnamse, genre_id: id, type: 'explore', page_no:this.pagenumer }).subscribe( 
        (res: any) => {
          if(res.status){
            this.pageNumber = this.pageNumber+10;
            for( let i=0; i<this.returnData.length; i++){
              if(this.returnData[i].type == 'data'){
                for (let index = 0; index < this.returnData[i].value.length; index++) {
                    if(this.returnData[i].value[index].tag == tabnamse){
                      // console.log(this.returnData[i].value[index].data)
                      let resultArr = [];
                      resultArr= this.returnData[i].value[index].data.concat(res.data.data);
                      this.returnData[i].value[index].data = resultArr;
                    } 
                }
              }
            }
            console.log(this.returnData)
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  onGrid(){
    this.classes = 'col-lg-6 col-xl-6';
  }
  onList(){
    this.classes = 'col-lg-12 col-xl-12';
  }
}
