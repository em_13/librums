import { Component, OnInit } from "@angular/core";
import { BaseUrl } from "../../../base-url";
import { HttpHeaders } from '@angular/common/http';
declare let paypal: any;

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"]
})
export class PaymentComponent implements OnInit {
  constructor() { }  
  planId: any;  
  subcripId: any;  
  basicAuth = 'Basic Af-FG-qPrC_Zau9sWip8h8OUJ68XMctAXbiljFZasxYhOhum8UIyqmcmoZIhiAvq1-pk8FlUS7Iy2bHU+ELV0P5AvVgmaPEdt6O1ZeQRywHBBFbwvOC7PBTjCKzYDQPYUCEEVCRo-AzHOd-wOZi5LqX-kA_BAI8Vq'

  ngOnInit() {     
    const self = this;  
    this.planId = 'P-20D52460DL479523BLV56M5Y';  //Default Plan Id
    paypal.Buttons({  
      createSubscription: function (data, actions) {  
        return actions.subscription.create({  
          'plan_id': 'P-20D52460DL479523BLV56M5Y',  
        });  
      },  
      onApprove: function (data, actions) {  
        console.log(data);  
        // alert('You have successfully created subscription ' + data.subscriptionID);  
        self.getSubcriptionDetails(data.subscriptionID);  
      },  
      onCancel: function (data) {  
        // Show a cancel page, or return to cart  
        console.log(data);  
      },  
      onError: function (err) {  
        // Show an error page here, when an error occurs  
        console.log(err);  
      }  
  
    }).render('#paypalbutton');  

    console.log(paypal);
  
  }

    // ============Start Get Subcription Details Method============================  
    getSubcriptionDetails(subcriptionId) {  
      const xhttp = new XMLHttpRequest();  
      xhttp.onreadystatechange = function () {  
        if (this.readyState === 4 && this.status === 200) {  
          console.log(JSON.parse(this.responseText));  
          alert(JSON.stringify(this.responseText));  
        }  
      };  
      xhttp.open('GET', 'https://api.sandbox.paypal.com/v1/billing/subscriptions/' + subcriptionId, true);  
      xhttp.setRequestHeader('Authorization', this.basicAuth);  
    
      xhttp.send();  
    }

}
