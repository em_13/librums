import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";

@Injectable({
  providedIn: "root"
})
export class PaymentService {
  payment: any;
  _baseURL: string;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  createPayment(){
    this.payment = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.payment.data.token
    });
    return this.http.post(this._baseURL + "/pyment", null, {
      headers
    });
  }

  saveTransaction() {
    this.payment = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.payment.data.token
    });
    return this.http.post(this._baseURL + "/saveTransaction", null, {
      headers
    });
  }
}
