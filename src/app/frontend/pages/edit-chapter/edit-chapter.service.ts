import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "./../../../base-url";

@Injectable({
  providedIn: 'root'
})
export class EditChapterService {
  userdata: any;
  baseURL: any;
  constructor(private http: HttpClient) { 
    this.baseURL = BaseUrl.frontend;
  }

  getChapterDetails(data){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + `/getChapterDetail`, data, { headers });
  }
}
