import { TestBed } from '@angular/core/testing';

import { EditChapterService } from './edit-chapter.service';

describe('EditChapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditChapterService = TestBed.get(EditChapterService);
    expect(service).toBeTruthy();
  });
});
