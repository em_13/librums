import { Component, OnInit, Pipe, AfterViewInit } from '@angular/core';
import { EditChapterService } from './edit-chapter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ReadChapterService } from '../read-chapter/read-chapter.service';
import { first } from "rxjs/operators";
import { BaseUrl } from "./../../../base-url";
import { ChapterAddService } from '../chapter-add/chapter-add.service';
import { DomSanitizer } from '@angular/platform-browser';
import { VideoDetails } from '../chapter-add/video-details';
import MediumEditor from 'medium-editor';
declare var $:any;
@Component({
  selector: 'app-edit-chapter',
  templateUrl: './edit-chapter.component.html',
  styleUrls: ['./edit-chapter.component.css']
})
export class EditChapterComponent implements OnInit {

  chapterData: any;
  chapter_id: any;
  commentData: any;
  book_id: any;
  submitted = false;
  baseURL: any;
  updateForm: any;

  getGenre: any;
  loading = false;
  showMsg = false;
  errorMsg: any;
  alertService: any;
  selectedFiles: any;
  currentFileUpload: any;
  chapterimage: any;

  videoId: any;

  messages:string;
  safeURL:any;
  loadings: boolean;
  message = '';
  childmessage: string ;
  showData= false;
  sourceValue : any;
  results: VideoDetails[];
  videoURL: any;
  bookData: any;
  showLoader=  false;
  RichTextImage: any;

  image: any;
  title:any;
  video:any;
  html: any;
  
  prices: any = ['0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0', '1.5', '2.0']

  constructor( private editChapter: EditChapterService, 
    private activeRoute: ActivatedRoute, 
    private formBuilder: FormBuilder,
    private chapterService: ReadChapterService,
    private addtype: ChapterAddService,
    private router: Router,
    private _sanitizer: DomSanitizer
    ) 
    {
      this.baseURL = BaseUrl.image;
     }

  ngOnInit() {
    this.showLoader = true;
    this.book_id = this.activeRoute.snapshot.paramMap.get("book_id");
    this.chapter_id = this.activeRoute.snapshot.paramMap.get("chapter_id");

    this.updateForm = this.formBuilder.group({
      book_id: this.book_id,
      chapter_id: this.chapter_id,
      title: ["", Validators.required],
      content: ["", Validators.required],
      is_paid: [""],
      amount: [""],
      image: [""],
      video: [""],
      is_published: [""],
      is_completed: ['false']
    });
    this.baseURL = BaseUrl.image;
    this.bookData = localStorage.getItem('mybooks');
    this.bookData = JSON.parse(this.bookData);

    this.videoId =  $("#videoid").val();
    
    this.baseURL = BaseUrl.image;
    if(this.videoId){
      this.videoURL="https://www.youtube.com/embed/"+this.videoId;
      this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.videoURL);
    }
    
    this.loadData();

    $( document ).on('click',".e-icon-btn", function(){
      if($(this).attr('aria-label') == 'Insert Image'){
        $(".rich-edit").val("true");
        $("#toolbar_Image").modal('show')
        $("#rich-edit").val('true');
      }
    })

    setTimeout(function(){ 
      var editor = new MediumEditor('#containernew');
      $(function () {
        $('#containernew').mediumInsert({
            editor: editor,
            addons: {
              images: {
                  fileUploadOptions: {
                    type: 'post',
                    url: 'https://librums.com:2001/api/uploadImageEditor', 
                    // url: 'http://18.209.143.118:8002/api/uploadImageEditor', 
                  },
                  uploadCompleted: function($el, data) {
                   
                  },
              }
          }
        });
      });

     }, 2000);
     $('#toggle-event').change(function() {
      $('#toggle-event').val($(this).prop('checked'));
      if($(this).prop('checked')){

        $('#amount').attr('readonly', false);
        $('#amount').val('');
      }else{
        // $('#amount-div').hide();
        $('#amount').attr('readonly', true);
        $('#amount').val('0.00');
      }
        })
  }

  ngAfterViewInit() {
   
  }

  //get chapter data
  loadData() {
   let data = {'chapter_id': this.chapter_id }
    this.editChapter.getChapterDetails(data).subscribe(
      (res: any) => {
        if (res.status) {
          this.chapterData = res.data;
          if(this.chapterData.video ){
            this.videoURL="https://www.youtube.com/embed/"+this.chapterData.video;
             this.videoURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.videoURL);
          }
          if(this.chapterData.is_paid){

            $('#toggle-event').bootstrapToggle('on');
          }else{
            $('#toggle-event').bootstrapToggle('off');
          }
          if(this.chapterData.is_paid){
            this.showData = true;
          }
          this.showLoader = false;
        } else {
          this.chapterData = "";
        }
      },
      error => {}
    );
  }


  get f() {
    return this.updateForm.controls;
  }

  onSelectCoverImage(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true;
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.addtype.chapter_image(formData).subscribe(
        (res: any) => {
          if (res) {
            if (res.status) {
              $(".hello").show();
              this.chapterimage = res.data;
              this.updateForm.controls["image"].setValue(res.data);
              this.showLoader = false;
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  onSelectCoverImagePopup(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true;
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.addtype.chapter_image(formData).subscribe(
        (res: any) => {
          if (res) {
            if (res.status) {
              this.RichTextImage = res.data;
              $(".e-content").append('<br /><img width=200 src="'+this.baseURL+this.RichTextImage+'">');
              $("#toolbar_Image").modal('hide')
              this.showLoader = false;
              $("#videoid").val('');
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  onSubmitSave(status) {
    this.submitted = true;

    // this.updateForm.value.is_paid = $( "#is_paid option:selected" ).val();
    this.updateForm.value.is_paid = ($('#toggle-event').prop('checked'))? 'true':'false';
   
    this.updateForm.value.amount =(this.updateForm.value.amount === 0)? "0.00":this.updateForm.value.amount ;
    // stop here if form is invalid
    $(".medium-insert-buttons").remove();
    this.updateForm.value.video = $("#videoid").val();
    this.updateForm.value.content = $(".medium-editor-element")[0].innerHTML;
    this.updateForm.value.is_completed =(this.updateForm.value.is_completed)?"true":"false" ;
    console.log(this.updateForm.value.is_paid)
    if(this.updateForm.value.video){
      this.updateForm.value.image = "";
    }
  
    if (this.updateForm.invalid) {
      return;
    }
 
    this.updateForm.value.is_published = status;
    this.loading = true;
    this.submitted = true;
    this.addtype.updateChapter(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.showMsg = true;
          this.router.navigate(["/mystories"]);
        } else {
          this.errorMsg = res.message;
        }
      },
      error => {
        this.loading = false;
      }
    );
  }

  public onOptionsSelected(event){
    let dd = event.target;
    this.sourceValue = dd.value;

    if(this.updateForm.value.is_paid === true)
    {
      this.showData=true;
    }
    else{
      this.updateForm.value.amount = "0.00"
      this.showData=false;
    }
    console.log(this.showData)
  }

  updateResults(results: VideoDetails[]): void {
    this.results = results;
    if (this.results.length === 0) {
      this.message = 'Not found...';
    } else {
      this.message = 'Top 10 results:';
    }
  }

  PreviewClick(){
    this.title = $("#title").val();
    this.image = $(".chapterimage").attr('src');
    this.video = $("#abc_frame").attr("src");
    this.html =  $(".medium-editor-element")[0].innerHTML;
    let Arr = {'title': this.title, 'image':this.image, 'html': this.html, 'video': this.video}
    localStorage.setItem('previewPage', JSON.stringify(Arr));
  }

}
