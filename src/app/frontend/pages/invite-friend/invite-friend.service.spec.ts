import { TestBed } from '@angular/core/testing';

import { InviteFriendService } from './invite-friend.service';

describe('InviteFriendService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InviteFriendService = TestBed.get(InviteFriendService);
    expect(service).toBeTruthy();
  });
});
