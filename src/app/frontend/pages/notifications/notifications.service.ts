import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class NotificationsService {
  userdata: any;
  _baseURL: any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  getNotification() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this._baseURL + `/getNotifications`, null, {
      headers: headers
    });
  }
}
