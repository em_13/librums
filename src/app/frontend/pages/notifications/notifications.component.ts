import { Component, OnInit } from '@angular/core';
import { NotificationsService } from './notifications.service';
import { BaseUrl } from "./../../../base-url";
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  getnotification: any;
  baseimage:any;
  showLoader = false;
  constructor( private notification: NotificationsService) { 

    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.loadNotification();
  }

  loadNotification() {
    this.notification.getNotification()
    .subscribe(
      (res: any) => {
       if(res.status){
         this.getnotification = res.data;
         this.showLoader = false;
       }
      },
      error => {
        console.log("ERROR");
      });
  }
}
