import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class HomeService {
  userdata: any;
  baseURL: string;
  base: string;

  constructor(private http: HttpClient) {
    this.baseURL = BaseUrl.frontend;
    this.base = BaseUrl.admin;
  }

  gethomedata() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + `/getHomeData`, null, { headers });
  }
  getSliders() {
    // alert('call');
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.base + `getSliders`, null, {
      headers
    });
  }
}
