import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { HomeService } from "./home.service";
import { Router } from "@angular/router";
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from 'src/app/_guard/auth.guard';
import { ToastrService } from 'ngx-toastr';

declare var $: any;

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  role: any;
  gethomedata: any;
  slider: any;
  firstTime: Boolean;
  showLoader = false;
  showWelcom = true;


  SlideOptions = {
    stagePadding: 120,
    margin: 0,
    dots: false,
    slideSpeed: 1000,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        // stagePadding: 300,
        items: 1
      },
      1200: {
        stagePadding: 300,
        items: 1
      },
      1300: {
        stagePadding: 300,
        items: 1
      },
      1400: {
        stagePadding: 300,
        items: 1
      },
      1500: {
        stagePadding: 350,
        items: 1
      },
      1600: {
        stagePadding: 380,
        items: 1
      },
      1700: {
        stagePadding: 400,
        items: 1
      },
      1900: {
        stagePadding: 420,
        items: 1
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: false, startPosition: 1 };
  baseimage: any;
  is_subscribed = false;
  constructor(private homeService: HomeService, private router: Router, config: NgbCarouselConfig, private authguard: AuthGuard) {
    this.baseimage = BaseUrl.image;
    config.interval = 2000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
    this.is_subscribed = this.authguard.is_subscribed;
  }

  ngOnInit() {
    this.showLoader = true;
    this.loadHome();
    this.loadSlider();
    const userdata = JSON.parse(localStorage.getItem("currentUser"));
    this.role = userdata.data.role;
    this.firstTime = (userdata.data.last_login_at == "") ? true : false;
    if (this.role === 1) {
      this.router.navigate(["/dashboard"]);
    } else {
      this.router.navigate(["/"]);
    }

    if (localStorage.getItem("welcome_mesage")) {
      this.showWelcom = false
    }
    if (this.showWelcom) {

      $(window).on('load', function () {

        $('#wlcm_back').modal('show');
        localStorage.setItem("welcome_mesage", "true");

      });
    }
  }

  loadHome() {
    this.homeService.gethomedata().subscribe(
      (res: any) => {
        if (res.status) {
          const mapped = Object.entries(res.data).map(([type, value]) => ({
            type,
            value
          }));
          this.showLoader = false;
          this.gethomedata = mapped;
        } else {
          //this.router.navigate(["/login"]);
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  loadSlider() {
    // alert('dssf');
    this.homeService.getSliders().subscribe(
      (res: any) => {
        // alert('if');
        this.slider = res.data;

      },
      error => {
        alert('else');
        // tslint:disable-next-line: quotemark
        console.log("ERROR");
      }
    );
  }
  authorProfile(id) {
    this.router.navigate(["/authorprofile"], { state: { id: id } });
  }
  searchTag(text) {
    localStorage.setItem("search", text);
    this.router.navigate(["/search"]);
  }
}
