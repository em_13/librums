import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { first } from "rxjs/operators";
import { AlertService } from "src/app/_services/alert.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { LoginService } from "./login.service";

import { GoogleLoginProvider, FacebookLoginProvider, AuthService, SocialUser } 
         from 'angular-6-social-login';  
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login'; 
import { SocialloginServiceService } from 'src/app/_services/sociallogin-service.service';
import { ToastrService } from 'ngx-toastr';

// This lets me use jquery
declare var $: any;

@Component({
  selector: "app-logins",
  templateUrl: "./logins.component.html",
  styleUrls: ["./logins.component.css"]
})
export class LoginsComponent implements OnInit {

  private user: SocialUser;
  private loggedIn: boolean;
  resendLink = false;
  @ViewChild('loginRef', {static: true }) loginElement: ElementRef;

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  showMsg = "";
  showMsgForget = "";
  auth2: any;
  Error= false;
  msgError: any;
  // tslint:disable-next-line: variable-name
  device_token = "web_token";
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private loginService: LoginService,
    private authenticationService: AuthenticationService,
    private authService: AuthService,
    public OAuth: AuthService,  
    private SocialloginService: SocialloginServiceService, 
    private toastr: ToastrService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(["/"]);
    }
  }

  fbLibrary() {
    (window as any).fbAsyncInit = function() {
      window['FB'].init({
        appId      : '2460638890823149',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      });
      window['FB'].AppEvents.logPageView();
    };
 
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  }



  prepareLoginButton() {
    let socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.OAuth.signIn(socialPlatformProvider).then(socialusers => {  
      this.googleLogin(socialusers);  
    }); 
  }

  ngOnInit() {
    this.fbLibrary();
    this.loginForm = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required]
    });

   
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || "/";

    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });

  }
  login() {
    window['FB'].login((response) => {
        console.log('login response',response);
        if (response.authResponse) {
         
          window['FB'].api('/me', {
            fields: 'last_name, first_name, email, birthday, gender',
          }, (userInfo) => {
            console.log(userInfo);
            this.loginService.checkUserExist({'value': userInfo.email, 'type': 1, 'social_type': 1, 'social_id': userInfo.id}).subscribe(
              (res: any) => {
                if (res.status) {
                  this.facebooklogin(userInfo);
                } else {
                  this.Error = true;
                  this.showMsg = res.message;
                  
                }
              },
              error => {
                this.loading = false;
              }
            );

          });
           
        } else {
          console.log('User login failed');
        }
    }, {scope: 'email'});
  }
  public googleLogin(userInfo){
    this.loginService.sociallogin({'type': 2, 'social_id': userInfo.id, 'device_token': 'web_toke', 'email': userInfo.email, 'fullname': userInfo.name }).subscribe(
      (res: any) => {
        if (res.status) {
          if (res.role === 1) {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "admin");
             window.location.href = "/";
          } else {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "user");
            window.location.href = "/";
          }
        } else {
          this.showMsg = res.message;
         
        }
      },
      error => {
        this.loading = false;
      }
    );
  }
  public facebooklogin(userInfo){
    this.loginService.sociallogin({'type': 1, 'social_id': userInfo.id, 'device_token': 'web_toke', 'email': userInfo.email, 'fullname': userInfo.first_name + ' ' + userInfo.last_name }).subscribe(
      (res: any) => {
        if (res.status) {
          if (res.role === 1) {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "admin");
            window.location.href = "/";
          } else {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "user");
           window.location.href = "/";
          }
        } else {
          this.showMsg = res.message;
         
        }
      },
      error => {
        this.loading = false;
      }
    );
  }


  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }


  // submit login form
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService
      .login(this.f.email.value, this.f.password.value, this.device_token)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status) {
            // this.toastr.error('Successfully login.')
            if (data.data.role === 1) {
              localStorage.setItem("role", "admin");
              window.location.href = "/";
            } else {
              localStorage.setItem("role", "user");
              window.location.href = "/";
            }
          } else {
           if(data.message == 'Please verify your email address before proceeding.'){
             this.resendLink = true;
           }
           this.msgError = data.message;  
            // this.toastr.error(data.message)
            this.loading = false;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }

  // resend activtion link
  resendLinkCall(){
    this.loginService.resend({email:this.f.email.value}).subscribe(
      (res: any) => {
        if (res.status) {
          this.resendLink = false;
          this.toastr.success(res.message);
        } else {
          this.toastr.error(res.message);
         
        }
      },
      error => {
        this.loading = false;
      }
    );

    
  }

}
