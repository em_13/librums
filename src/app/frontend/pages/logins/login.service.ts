import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class LoginService {
  baseURL: any;

  constructor(private http: HttpClient) {
    this.baseURL = BaseUrl.frontend;
  }
  forget(data) {
    return this.http.post(this.baseURL + `/forgotpassword`, data);
  }

  checkUserExist(data){
    return this.http.post(this.baseURL + `/checkEmailAndUsername`, data);
  }

  sociallogin(data){
    return this.http.post(this.baseURL + `/sociallogin`, data);
  }

  resend(data) {
    return this.http.post(this.baseURL + `/resendVerifyMail`, data);
  }

}
