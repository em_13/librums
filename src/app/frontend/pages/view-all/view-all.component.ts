import { Component, OnInit } from '@angular/core';
import { ViewAllService } from './view-all.service';
import { ActivatedRoute } from "@angular/router";
import { Route } from '@angular/compiler/src/core';
import { Router } from "@angular/router";
import { SEOServiceService } from 'src/app/seoservice.service';
import { ToastrService } from 'ngx-toastr';
import { BaseUrl } from "./../../../base-url";
declare var $: any;
@Component({
  selector: 'app-view-all',
  templateUrl: './view-all.component.html',
  styleUrls: ['./view-all.component.css']
})
export class ViewAllComponent implements OnInit {
/**scrol */
page_no = 0;
array = [];
sum = 100;
classes = 'col-lg-6 col-xl-6';
// throttle = 300;
scrollDistance = 1;
scrollUpDistance = 2;
direction = '';
modalOpen = false;
/**scrol */
activetab: any;
alsoLikes: any;
gethomedata: any;
type: any;
baseURL: any;
showLoader = false;
pagenumer = 0;
pageNumber = 10;
textUnder: any;
constructor(
  private homeDetailService: ViewAllService,
  private activatedRoute: ActivatedRoute,
  private router: Router,
  private _seoService: SEOServiceService, private toaster: ToastrService
) {
  this.baseURL = BaseUrl.image;
}

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.paramMap.get("type");
    this.showLoader = true;
    this.loadHome();
    this.appendItems(0, this.sum);
  }
  loadHome() {
    this.homeDetailService.gethomedata({ type: (this.type === 'published')?1:2 }).subscribe(
      (res: any) => {
        if(res.status){
          this.gethomedata = res.data;
        }
        this.showLoader = false;
      
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  searchTag(text) {
    localStorage.setItem("search", text);
    this.router.navigate(["/search"]);
  }

 

  /**Scroll */
  addItems(startIndex, endIndex, _method) {
    for (let i = 0; i < this.sum; ++i) {
      this.array[_method]([i, ' ', this.generateWord()].join(''));
    }
  }
  deleteBook(id){
    this.homeDetailService.deleteBook({'book_id': id}).subscribe(
      (res: any) => {
        if(res.status){
          this.toaster.success(res.message)
          this.showLoader = true;
          this.loadHome();
        }else{
          this.toaster.error(res.message)
        }
      },
      error => {}
    );
  }
  appendItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'push');
  }

  prependItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'unshift');
  }

  onScrollDown() {
    this.pagenumer = this.pagenumer + 1;
    let id = $(".nav-link.active").data('sectionvalue');
    let tabnamse = $(".nav-link.active").data('tabename');
    this.homeDetailService.loadMoreBooks({ type: (this.type === 'published')?1:2, page_no: this.pagenumer }).subscribe(
      (res: any) => {
        if(res.status){
          this.pageNumber = this.pageNumber+10;
          let resultArr = [];
          resultArr=  this.gethomedata.concat(res.data);
           this.gethomedata = resultArr;
          console.log( this.gethomedata)
        }
        this.showLoader = false;
      },
      error => {
        console.log("ERROR");
      }
    );
    // add another 20 items
    const start = this.sum;
    this.sum += 20;
    this.appendItems(start, this.sum);

    this.direction = 'down'
  }

  onUp(ev) {
    console.log('scrolled up!', ev);
    const start = this.sum;
    this.sum += 20;
    this.prependItems(start, this.sum);

    this.direction = 'up';
  }
  generateWord() {
    return Math.floor(Math.random() * 6) + 1;
  }
  onGrid(){
    this.classes = 'col-lg-6 col-xl-6';
  }
  onList(){
    this.classes = 'col-lg-12 col-xl-12';
  }
  /**Scroll */
}
