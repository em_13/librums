import { TestBed } from '@angular/core/testing';

import { MystoriesService } from './mystories.service';

describe('MystoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MystoriesService = TestBed.get(MystoriesService);
    expect(service).toBeTruthy();
  });
});
