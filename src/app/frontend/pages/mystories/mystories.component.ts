import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { MystoriesService } from "./mystories.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: "app-mystories",
  templateUrl: "./mystories.component.html",
  styleUrls: ["./mystories.component.css"]
})
export class MystoriesComponent implements OnInit {
  getstoriesdata: any;
  publishedStories: any;
  publishCount: any;
  draftCount: any;
  draftStories: any;
  _baseURL: any;
  errorMsg = '';
  errorMsgs = false;
  sMsgs = false;
  successMsg = '';
  showLoader = false;
  pagenumber : any;
  activepage: number;

  constructor(private mystroies: MystoriesService, private router : Router, private toaster: ToastrService) {
    this._baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.loadMyStories();
    this.getStoriesList();
  }
  ngAfterViewChecked() {
   
    $("#alsolike").flipster({

      style: 'carousel',
      spacing: -0.5,
      buttons: true,
      start: 'center',
      loop: true,
      autoplay: false,
      click: true,
      scrollwheel: false,
    });
    $("#alsolike1").flipster({

      style: 'carousel',
      spacing: -0.3,
      buttons: true,
      start: 'center',
      loop: true,
      autoplay: false,
      click: true,
      scrollwheel: false,
    });
    // if ($("#alsolike1").find('.flip-items').find('li').length == 1) {
    //   $(".flip-items").css({ "width": "700px", "margin": "0 auto" })
    // } else {
    //   $(".flip-items").css({ "width": "auto", "margin": "0" })
    // }

  }
  //load my stories
  loadMyStories() {
    this.mystroies.gethomedata({page_no:0}).subscribe(
      (res: any) => {
        if (res.status) {
          this.showLoader = false;
          if(res.total <= 10){
            
          }else{
             this.pagenumber = res.total/10;
            this.pagenumber = Math.ceil(this.pagenumber);
            this.counter(this.pagenumber)
          }
          this.getstoriesdata = res.data;
          this.publishCount = res.publishCount;
          this.draftCount = res.draftCount;
          this.activepage = 0;
        
        } else {
          this.showLoader = false;
          this.getstoriesdata = "";
        }
      },
      error => {}
    );
  }
  getStoriesList() {
    this.mystroies.getStoriesList().subscribe(
      (res: any) => {
        if (res.status) {
          this.showLoader = false;
         
        this.publishedStories = res.data.published;
        this.draftStories = res.data.drafted;
        this.publishCount = res.publishCount;
          this.draftCount = res.draftCount;
         
        
        } else {
          this.showLoader = false;
          this.getstoriesdata = "";
        }
      },
      error => {}
    );
  }

  counter(i: number) {
    return new Array(i);
  }

  pageChange(number){
    this.mystroies.gethomedata({page_no:number}).subscribe(
      (res: any) => {
        if (res.status) {
          if(res.total == 10){
           
          }else{
             this.pagenumber = res.total/10;
            this.pagenumber = Math.ceil(this.pagenumber);
            this.counter(this.pagenumber)
          }
          this.getstoriesdata = res.data;
          this.activepage = number;
          this.showLoader = false;
        } else {
          this.getstoriesdata = "";
        }
      },
      error => {}
    );
  }

  //delete book
  deleteBook(id){
    this.mystroies.deleteBook({'book_id': id}).subscribe(
      (res: any) => {
        if(res.status){
          this.toaster.success(res.message)
          this.showLoader = true;
          this.loadMyStories();
        }else{
          this.toaster.error(res.message)
        }
      },
      error => {}
    );
  }
  searchTag(text) {
    localStorage.setItem("search",text);
    this.router.navigate(["/search"]);
  }
}
