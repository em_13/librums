import { Component, OnInit } from '@angular/core';
import { BaseUrl } from './../../../base-url';
import { MyWallService } from './my-wall.service';
import { first } from 'rxjs/operators';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { PostMessageService } from '../post-message/post-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
declare var $:any;
@Component({
  selector: 'app-my-wall',
  templateUrl: './my-wall.component.html',
  styleUrls: ['./my-wall.component.css']
})
export class MyWallComponent implements OnInit {
  getGenre: any;
  updateForm: any;
  loading = false;
  submitted = false;
  showMsg  = '';
  errorMsg: any;
  alertService: any;
  selectedFiles: FileList;
  currentFileUpload: File;
  postimage: any;
  user_id: any;
  _baseURL: any;
  showLoader = false;
  wallData: any;
  userdata: any;
  userImage: any;
  imgSrc : any;
  currentDate = new Date();
  postid:any;
  timeZoneOffset : any;
  constructor(private mywalldata: MyWallService,
     private confirmationDialogService: ConfirmationDialogService,
      private postmessage: PostMessageService,
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private formBuilder: FormBuilder,
      private toaster: ToastrService
      ) {
    this._baseURL = BaseUrl.image;
   }

  ngOnInit() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    console.log(this.userdata)
    this.userImage = this.userdata.data.image;
    this.timeZoneOffset = this.timeConvert(this.currentDate.getTimezoneOffset() * 2);
    
    this.wallDataLoad();

    this.updateForm = this.formBuilder.group({
      user_id: this.user_id,
      content: ["", Validators.required],
      post_image: [""]
    });
  }
   timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    var total = -(rhours + rminutes)
    if(n < 0){

      return  "+"+total;
    }else{
      return  "-"+total;

    }
    // return num + " minutes = " + rhours + " hour(s) and " + rminutes + " minute(s).";
    }
  wallDataLoad(){
    this.mywalldata.getMyPost({ page_no: 0 }).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        if(res.status){
          this.wallData = res.data;
        }else{

        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  deletWallPost(id){
    this.postid = id;
  }
  viewImage(src){
    this.imgSrc = src;
    $('#view-image').modal('show');
  }

  deleteConfirm(){
    this.mywalldata
    .removeWallPost({ post_id: this.postid })
    .pipe(first())
    .subscribe(
      (res: any) => {
        if(res.status){
          this.toaster.success(res.message)
          this.wallDataLoad();
        }else{
          this.toaster.error(res.message)
        }
        
      },
      error => {
        console.log("ERROR", error);
      }
    )
  }

  onSelectFile(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true
      let formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.postmessage.post_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.postimage = res;
            if (res.status) {
              $(".post-image").hide();
              $(".details_listing_txt_img1").show();
              $("#post-image").show();
              $("#post-image").attr('src', this._baseURL+'/'+res.data);
              this.updateForm.controls["post_image"].setValue(res.data);
              this.showLoader = false
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  get f() {
    return this.updateForm.controls;
  }
  onSubmit() {
    this.loading = true;
    this.submitted = true;
    this.user_id = $("#user_id").val();
    this.updateForm.value.user_id = $("#user_id").val();
    // stop here if form is invalid
  
    if (this.updateForm.invalid) {
      this.loading = false;
      return;
    }
 
    this.postmessage.savePost(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          $("#myModal").modal('hide')
          this.wallDataLoad();
          // this.router.navigate(['authorprofile/'+this.user_id]);
         this.toaster.success('Post submitted successfully.')
          this.loading = false;
          $(".details_listing_txt_img1").hide();
          $("#post-image").hide();
          this.updateForm.reset();
        } else {
          this.toaster.error(res.message);
         
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

}
