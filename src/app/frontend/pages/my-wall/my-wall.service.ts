import { Injectable } from '@angular/core';
import { BaseUrl } from './../../../base-url';
import { HttpHeaders, HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MyWallService {

  userdata: any;
  _baseURL: any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }
  getMyPost(pageNo) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/getMyPost`, pageNo, {
      headers: headers
    });
  }
  removeWallPost(data){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });

    return this.http.post(this._baseURL + `/deletePost`, data, { headers });
  }
}
