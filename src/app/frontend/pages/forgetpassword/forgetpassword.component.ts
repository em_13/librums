import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  ForgetForm: FormGroup;
  submitted = false;
  loading = false;
  showMsg: any;
  showMsgForget: any;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private toaster: ToastrService
  ) { }

  ngOnInit() {
    this.ForgetForm = this.formBuilder.group({
      email: ["", Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.ForgetForm.controls;
  }

    // submit forget password
    onSubmitForget() {
      console.log(this.ForgetForm.value.email)
      this.submitted = true;
  
      // stop here if form is invalid
      // if (this.ForgetForm.invalid) {
      //   return;
      // }
      this.loading = true;
      this.authenticationService
        .forget(this.ForgetForm.value.email)
        .pipe(first())
        .subscribe(
          data => {
            if (data.status) {
              this.reset();
              this.loading = false;
              this.toaster.success(data.message);
              this.showMsgForget ='';
              $("#forgot").modal("hide");
              this.router.navigate([this.returnUrl]);
            } else {
              this.toaster.error(data.message);
              this.loading = false;
            }
          },
          error => {
         
          }
        );
    }

      // reset form data
  private reset() {
    this.ForgetForm = this.formBuilder.group({
      // tslint:disable-next-line: quotemark
      email: [""]
    });
  }

}
