import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { ReportService } from "./report.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AlertService } from "src/app/_services/alert.service";
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css"]
})
export class ReportComponent implements OnInit {
  getreportissue: any;
  userdata: any;
  reportForm: FormGroup;
  loading = false;
  submitted = false;
  showMsg = false;
  _baseURL: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private reportService: ReportService,
    private toaster: ToastrService
  ) {
    this._baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.reportForm = this.formBuilder.group({
      type: ["", Validators.required],
      content: ["", Validators.required],
      book_id: [(localStorage.getItem("book_id"))?localStorage.getItem("book_id"):""]
    });

    this.loadReport();
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.reportForm.controls;
  }

  // submit issue form
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.reportForm.invalid) {
      return;
    }
    this.loading = true;
    this.reportService
      .report(this.reportForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.toaster.success("Issue reported successfully.")
          // this.router.navigate(["/report"]);
          this.showMsg = true;
          localStorage.removeItem("book_id")
          this.reset();
          setTimeout(() => {
          this.router.navigate(["/"]);
          })
        },
        error => {
          this.toaster.error("Something went wrong.")
          this.loading = false;
        }
      );
  }

  // reset form data
  private reset() {
    this.reportForm = this.formBuilder.group({
      type: [""],
      content: [""]
    });
  }

  //get report issue listing
  private loadReport() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    this.reportService.getreportissue().subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        this.getreportissue = mapped;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
}
