import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";
@Injectable({
  providedIn: "root"
})
export class ReportService {
  userdata: any;
  _baseURL: string;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  getreportissue() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/getIssueType`, null, {
      headers: headers
    });
  }

  report(data) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/reportIssue`, data, {
      headers: headers
    });
  }

  feedback(data) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/giveFeedback`, data, {
      headers: headers
    });
  }
}
