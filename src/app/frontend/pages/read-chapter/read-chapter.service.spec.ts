import { TestBed } from '@angular/core/testing';

import { ReadChapterService } from './read-chapter.service';

describe('ReadChapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReadChapterService = TestBed.get(ReadChapterService);
    expect(service).toBeTruthy();
  });
});
