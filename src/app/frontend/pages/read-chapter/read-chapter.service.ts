import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";
@Injectable({
  providedIn: "root"
})
export class ReadChapterService {
  _baseURL: string;
  userdata: any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }
  getChapter(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/getChapter`, postData, {
      headers
    });
  }
  getComments(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/getComment`, postData, {
      headers
    });
  }
  likeBook(data) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/likeBook`, data, {
      headers: headers
    });
  }
  addComment(comment: string, book_id: any, chapter_id:any) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(
      this._baseURL + `/addComment`,
      { comment, book_id, chapter_id },
      { headers: headers }
    );
  }

  savePayment(data){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/saveTransaction`, data, {
      headers
    });
  }

  shareBook(data){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/shareBook`, data, {
      headers: headers
    });
  }
}
