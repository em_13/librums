import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class AboutService {
  baseurl: any;
  constructor(private http: HttpClient) {
    this.baseurl = BaseUrl.frontend;
  }

  about() {
    return this.http.get(this.baseurl + `/aboutus`);
  }
}
