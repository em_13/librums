import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { AboutService } from './about.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  about;
  constructor(
    private aboutService: AboutService
  ) { }

  ngOnInit() {
    this.loadAbout();
  }

  private loadAbout() {
    this.aboutService.about().pipe(first()).subscribe(about => {
        this.about = about;
    });
  }

}
