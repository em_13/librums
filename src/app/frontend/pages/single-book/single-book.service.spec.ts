import { TestBed } from '@angular/core/testing';

import { SingleBookService } from './single-book.service';

describe('SingleBookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SingleBookService = TestBed.get(SingleBookService);
    expect(service).toBeTruthy();
  });
});
