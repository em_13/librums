import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class SingleBookService {
  userdata: any;
  baseuel: string;
  constructor(private http: HttpClient) {
    this.baseuel = BaseUrl.frontend;
  }

  getBookShelf(data) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/getBookById`, data, {
      headers: headers
    });
  }
  removeFromBookshelf(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });

    return this.http.post(this.baseuel + `/removeFromBookshelf`, postData, { headers });
  }
}
