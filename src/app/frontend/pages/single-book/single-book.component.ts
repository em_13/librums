import { Component, OnInit, AfterViewChecked, Pipe } from "@angular/core";
import { BaseUrl } from "./../../../base-url";
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/_services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SingleBookService } from './single-book.service';
import { BookDetailService } from '../book-detail/book-detail.service';
import { ToastrService } from 'ngx-toastr';
import { SEOServiceService } from 'src/app/seoservice.service';

declare var $: any;
@Component({
  selector: 'app-single-book',
  templateUrl: './single-book.component.html',
  styleUrls: ['./single-book.component.css']
})
@Pipe({ name: 'safeHtml' })
export class SingleBookComponent implements OnInit {

  NewStoriesSlider: any;
  baseurl: any;
  bookId: any;
  rmbtn = false;
  addbtn = false;  
  previousUrl :any;
  showLoader = false;
  bookshelfActive = false;
  SlideOptions = {
    stagePadding: 200,
    loop: false,
    margin: 10,
    nav: false,
    dots: false,
    items: 1,
    lazyLoad: false,
    responsive: {
      0: {
        items: 1,
        nav: false,
        stagePadding: 60
      },
      600: {
        items: 1,
        nav: false,
        stagePadding: 100
      },
      1000: {
        items: 1,
        nav: false,
        stagePadding: 200
      },
      1200: {
        items: 1,
        nav: false,
        stagePadding: 250
      },
      1400: {
        items: 1,
        nav: false,
        stagePadding: 300
      },
      1600: {
        items: 1,
        nav: false,
        stagePadding: 350
      },
      1800: {
        items: 2,
        nav: false,
        stagePadding: 300
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: false };
  
  constructor( private bookDetailService: BookDetailService,
     private bookshelf: SingleBookService,
      private router: Router,private alertService: 
      AlertService, private activatedRoute: 
      ActivatedRoute, private toaster: ToastrService,
      private _seoService: SEOServiceService
      ) {
    this.baseurl = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.bookId = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadBookShelfData();
    this.previousUrl = localStorage.getItem("previousUrl")
  }
  ngAfterViewChecked(){
      $("#alsolike").flipster({
        style: 'carousel',
        spacing: -0.3,
        buttons: true,
        start: 'center',
        loop: true,
        
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
      // this.loadBookShelfData();
      // this.bookId = this.activatedRoute.snapshot.paramMap.get("id");
      // this.loadBookShelfData();
  }
  loadBookShelfData() {
    this.bookshelf.getBookShelf({'device_type':'web', book_id: this.bookId }).subscribe(
      (res: any) => {
        if(res.status){
          this.NewStoriesSlider = res.data;
          if(this.NewStoriesSlider.is_in_bookshelf){
            this.bookshelfActive = true;
          }
          localStorage.setItem('mybooks', JSON.stringify(res.data));
          this.showLoader = false;
          this._seoService.updateTitle(this.NewStoriesSlider.title)
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  addBookShelf(event) {
    this.bookDetailService.addToBookShelf(event).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success(res.message);
          this.bookshelfActive = true;
          this.addbtn = false;
          this.rmbtn = true;
          // this.loadBookShelfData();
        } else {
          this.toaster.error(res.message);
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  removeBookShelf(event) {
    this.bookDetailService.removeToBookShelf(event).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success(res.message);
          this.bookshelfActive = false;
          this.addbtn = true;
          this.rmbtn = false;
          // this.loadBookShelfData();
        } else {
          this.toaster.error(res.message);
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  reportBook(id) {
    localStorage.setItem("book_id",id);
    this.router.navigate(["/report"]);
  }
  searchTag(text) {
    localStorage.setItem("search",text);
    this.router.navigate(["/search"]);
  }

  // reload(id){
  //   this.bookId = id;
  //   this.loadBookShelfData();
  //   // this.router.navigate(['/singlebook/'+id]);
  // }

  singleBook(id){
    // this.loadBookShelfData();
    // this.router.navigate(['/singlebook/'+id]);
  }

}
