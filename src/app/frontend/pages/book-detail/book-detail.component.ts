import { BaseUrl } from "./../../../base-url";
import { Component, OnInit, AfterViewChecked } from "@angular/core";
import { Router, RouterStateSnapshot, ActivatedRoute } from "@angular/router";
import { BookDetailService } from "./book-detail.service";
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: "app-book-detail",
  templateUrl: "./book-detail.component.html",
  styleUrls: ["./book-detail.component.css"]
})
export class BookDetailComponent implements OnInit {
  bookId: any;
  genreId: any;
  bookData: any;
  alsoLikes: any;
  previousUrl : any;
  showLoader = false;
  addbtn = false;
  rmbtn = false;
  SlideOptions = {
    stagePadding: 200,
    loop: false,
    startPosition: 1,
    margin: 0,
    nav: false,
    dots: false,
    items: 1,
    lazyLoad: false,
    responsive: {
      0: {
        items: 1,
        nav: false,
        stagePadding: 60
      },
      600: {
        items: 1,
        nav: false,
        stagePadding: 100
      },
      1000: {
        items: 1,
        nav: false,
        stagePadding: 200
      },
      1200: {
        items: 1,
        nav: false,
        stagePadding: 250
      },
      1400: {
        items: 1,
        nav: false,
        stagePadding: 300
      },
      1600: {
        items: 1,
        nav: false,
        stagePadding: 350
      },
      1800: {
        items: 1,
        nav: false,
        stagePadding: 400
      }
    }
  };
  CarouselOptions = { items: 2, dots: false, nav: false };
  type: string;
  baseurl: any;
  constructor(
    private router: Router,
    private bookDetailService: BookDetailService,
    private toaster: ToastrService,
    private activatedRoute: ActivatedRoute
  ) {
    this.baseurl = BaseUrl.image;
  }

  ngOnInit() {
    this.bookId = this.activatedRoute.snapshot.paramMap.get("id");
    this.type = this.activatedRoute.snapshot.paramMap.get("type");
    this.genreId = this.activatedRoute.snapshot.paramMap.get("genre_id");
    this.loadAuthorData();
    this.AlsoLike();
    this.previousUrl = localStorage.getItem("previousUrl")
  }

  ngAfterViewChecked() {
    $('.also_like_slider').each(function (index, value) {
      $("#alsolike" + index).flipster({
        style: 'carousel',
        spacing: -0.3,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    });
    $("#alsolike").flipster({

      style: 'carousel',
      spacing: -0.3,
      buttons: true,
      start: 'center',
      loop: true,
      autoplay: false,
      click: true,
      scrollwheel: false,
    });
    // this.showLoader = false;
  }

  loadAuthorData() {
    this.showLoader = true;
    this.bookDetailService
      .getBookDetail({ 'device_type': 'web', type: this.type, book_id: this.bookId, genre_id: (this.type == 'explore')?this.genreId:'' })
      .subscribe(
        (res: any) => {
          const mapped = Object.entries(res.data).map(([type, value]) => ({
            type,
            value
          }));
          if (res.data) {
            for (var i = 0; i < res.data.length; i++) {
              if (res.data[i].active) {
                this.SlideOptions.startPosition = i;
                this.bookData = res.data;
              }
            }
            this.showLoader = false;
          }

        },
        error => {
          console.log("ERROR");
        }
      );
  }
  AlsoLike() {
    this.bookDetailService.getAlsoLike().subscribe(
      (res: any) => {

        this.showLoader = false;
        this.alsoLikes = res.data;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  addBookShelf(event) {
    this.bookDetailService.addToBookShelf(event).subscribe(
      (res: any) => {
        if (res.status) {
          // alert(res.message);
          this.toaster.success(res.message);
          this.rmbtn = true;
          this.addbtn = false;
         
          // this.loadAuthorData()
        } else {
          // alert(res.message);
          this.toaster.success(res.message);
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  removeBookShelf(event) {
    this.bookDetailService.removeToBookShelf(event).subscribe(
      (res: any) => {
        if (res.status) {
          // alert(res.message);
          this.toaster.success(res.message);
          this.addbtn = true;
          this.rmbtn = false;
        } else {
          this.toaster.success(res.message);
          // alert(res.message);
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  searchTag(text) {
    localStorage.setItem("search", text);
    this.router.navigate(["/search"]);
  }
}
