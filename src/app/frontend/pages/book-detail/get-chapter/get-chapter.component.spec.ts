import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetChapterComponent } from './get-chapter.component';

describe('GetChapterComponent', () => {
  let component: GetChapterComponent;
  let fixture: ComponentFixture<GetChapterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetChapterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetChapterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
