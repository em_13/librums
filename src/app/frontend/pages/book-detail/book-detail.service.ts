import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class BookDetailService {
  userdata: any;
  baseURL: string;
  constructor(private http: HttpClient) {
    this.baseURL = BaseUrl.frontend;
  }

  getBookDetail(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + `/getBookDetail`, postData, {
      headers: headers
    });
  }
  getAlsoLike() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + `/getAlsoLikesSec`, {}, {
      headers
    });
  }
  addToBookShelf(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + "/addToBookshelf", postData, {
      headers: headers
    });
  }


  removeToBookShelf(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseURL + "/removeFromBookshelf", postData, {
      headers: headers
    });
  }
}
