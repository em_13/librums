import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";
@Injectable({
  providedIn: "root"
})
export class ProfileService {
  _baseURL: string;
  userdata: any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }
  getProfile() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this._baseURL + `/getUserProfile`, null, { headers });
  }
}
