import { Component, OnInit } from "@angular/core";
import { ProfileService } from "./profile.service";
import { BaseUrl } from "../../../base-url";
import { PostMessageService } from '../post-message/post-message.service';
import { MyWallService } from '../my-wall/my-wall.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';
import { SEOServiceService } from 'src/app/seoservice.service';
declare var $:any;
@Component({ 
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"]
})
export class FrontProfileComponent implements OnInit {

  getGenre: any;
  updateForm: any;
  loading = false;
  submitted = false;
  showMsg  = '';
  errorMsg: any;
  alertService: any;
  selectedFiles: FileList;
  currentFileUpload: File;
  postimage: any;
  user_id: any;
  _baseURL: any;
  showLoader = false;
  wallData: any;
  userdata: any;
  userImage: any;
  imgSrc : any;
  currentDate = new Date();
  postid:any;
  timeZoneOffset : any;
  profileData: any;
  showError = false;
  
  constructor(  
    private toaster: ToastrService,
    private mywalldata: MyWallService, 
    private formBuilder: FormBuilder,
    private _seoService: SEOServiceService,
    private postmessage: PostMessageService,
    private profileService: ProfileService
    ) {
    this._baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.getProfile();
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    this.userImage = this.userdata.data.image;
     
    this.wallDataLoad();

    this.updateForm = this.formBuilder.group({
      user_id: this.user_id,
      content: ["", Validators.required],
      post_image: [""]
    });
  }
  getProfile() {
    this.profileService.getProfile().subscribe(
      (res: any) => {
        if(res.data.cover_image){
          res.data.cover_image = this._baseURL+''+res.data.cover_image;
        }else{
          res.data.cover_image = 'assets/img/bg_place_holder.jpg';
        }
        this.profileData = res.data;
        this._seoService.updateTitle(this.profileData.fullname);
        this.showLoader = false;
      },
      error => {
        console.log("ERROR", error);
      }
    );
  }



  wallDataLoad(){
    this.mywalldata.getMyPost({ page_no: 0 }).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        if(res.status){
          this.wallData = res.data;
        }else{

        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  deletWallPost(id){
    this.postid = id;
  }
  viewImage(src){
    this.imgSrc = src;
    $('#view-image').modal('show');
  }

  deleteConfirm(){
    this.mywalldata
    .removeWallPost({ post_id: this.postid })
    .pipe(first())
    .subscribe(
      (res: any) => {
        if(res.status){
          this.showError = false;
          this.toaster.success(res.message)
          this.wallDataLoad();
        }else{
          this.toaster.error(res.message)
        }
        
      },
      error => {
        console.log("ERROR", error);
      }
    )
  }

  onSelectFile(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true
      let formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.postmessage.post_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.postimage = res;
            if (res.status) {
              $(".post-image").hide();
              $(".details_listing_txt_img1").show();
              $("#post-image").show();
              $("#post-image").attr('src', this._baseURL+'/'+res.data);
              this.updateForm.controls["post_image"].setValue(res.data);
              this.showLoader = false
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  // get f() {
  //   return this.updateForm.controls;
  // }
  onSubmit() {
    this.loading = true;
    this.submitted = true;
    this.user_id = $("#user_id").val();
    this.updateForm.value.user_id = $("#user_id").val();
    // stop here if form is invalid
    if(this.updateForm.controls.content.value == ''){
      this.showError = true;
      this.loading = false;
      return; 
    }
    // if (this.updateForm.invalid) {
    //   this.loading = false;
    //   return;
    // }
 
    this.postmessage.savePost(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.wallDataLoad();
          // this.router.navigate(['authorprofile/'+this.user_id]);
         this.toaster.success('Post submitted successfully.')
          this.loading = false;
          $(".details_listing_txt_img1").hide();
          $("#post-image").hide();
          this.showError = false;
          this.emptyForm();
        } else {
          this.toaster.error(res.message);
         
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
  emptyForm(){
    this.updateForm.controls["post_image"].setValue('');
    this.updateForm.controls["content"].setValue('');
  }
  CrossImage(){
    this.updateForm.controls["post_image"].setValue('');
    $("#post-image").attr('src','');
    $(".details_listing_txt_img1").hide();
    $("#post-image").hide();
  }

}
