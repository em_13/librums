import { Component, OnInit, Pipe } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthorProfileService } from "./author-profile.service";
import { first } from "rxjs/operators";
import { AlertService } from "src/app/_services/alert.service";
import { MyWallService } from '../my-wall/my-wall.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';
import { PostMessageService } from '../post-message/post-message.service';
import { BaseUrl } from './../../../base-url';
import { SEOServiceService } from 'src/app/seoservice.service';
declare var $: any;
@Component({
  selector: "app-author-profile",
  templateUrl: "./author-profile.component.html",
  styleUrls: ["./author-profile.component.css"]
})
@Pipe({ name: 'safeHtml' })
export class AuthorProfileComponent implements OnInit {
  userid: any;
  authordata: any;
  userdata: any;
  loggedIn: any;
  showLoader = false;
  selectedFiles: FileList;
  currentFileUpload: File;
  postimage: any;
  _baseURL: any;
  imgSrc: any;
  SlideOptions = {
    stagePadding: 120,
    margin: 0,
    dots: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        // stagePadding: 300,
        items: 1
      },
      1200: {
        stagePadding: 300,
        items: 1
      },
      1300: {
        stagePadding: 300,
        items: 1
      },
      1400: {
        stagePadding: 300,
        items: 1
      },
      1500: {
        stagePadding: 350,
        items: 1
      },
      1600: {
        stagePadding: 380,
        items: 1
      },
      1700: {
        stagePadding: 400,
        items: 1
      },
      1900: {
        stagePadding: 420,
        items: 1
      }
    }
  };
  CarouselOptions = { items: 2, dots: false, nav: false };
  baseimage: any;
  showMsg = '';
  postid: any;
  storyCount: any;
  likeCount: any;
  currentDate = new Date();
  updateForm: any;
  loading = false;
  submitted = false;
  showError = false;
  timeZoneOffset: any;
  user_id: any;
  constructor(
    private router: Router,
    private alertService: AlertService,
    private authorService: AuthorProfileService,
    private activatedRoute: ActivatedRoute,
    private mywalldata: MyWallService,
    private toaster: ToastrService,
    private postmessage: PostMessageService,
    private formBuilder: FormBuilder,
    private _seoService: SEOServiceService
  ) {
    this.baseimage = BaseUrl.image;
    this._baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.userid = this.activatedRoute.snapshot.paramMap.get("id");
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    this.loggedIn = this.userdata.data._id;

    this.timeZoneOffset = this.timeConvert(this.currentDate.getTimezoneOffset() * 2);

    this.updateForm = this.formBuilder.group({
      user_id: this.userid,
      content: ["", Validators.required],
      post_image: [""]
    });
    this.loadAuthorData();
    this.loadAuthorDataCount();
  }
  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    var total = -(rhours + rminutes)
    if (n < 0) {

      return "+" + total;
    } else {
      return "-" + total;

    }
    // return num + " minutes = " + rhours + " hour(s) and " + rminutes + " minute(s).";
  }
  //load the auther data
  loadAuthorData() {
    this.authorService.getAuthor(this.userid).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));

        if (res.data.cover_image) {
          res.data.coverimage = this.baseimage + '' + res.data.cover_image;
        } else {
          res.data.coverimage = 'assets/img/bg_place_holder.jpg';
        }
        this.authordata = res.data;
        this._seoService.updateTitle(this.authordata.fullname);
        this.showLoader = false;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  loadAuthorDataCount() {
    this.authorService.getAuthorCount(this.userid).subscribe(
      (res: any) => {
        this.storyCount = res.data.books;
        this.likeCount = res.data.likes;

      },
      error => {
        console.log("ERROR");
      }
    );
  }

  // follow the user pass auther_id and user_id
  follow(e) {
    let total = $(".total").text();
    this.authorService.followAuther({ status: e.target.dataset.status, user_id: e.target.dataset.user }).subscribe(
      (res: any) => {
        if (res.status) {
          // this.toaster.success(res.message);
          // this.loadAuthorData();
          if (e.target.dataset.status == 2) {
            $(".user_" + e.target.dataset.user).attr('data-status', 1);
          } else {
            $(".user_" + e.target.dataset.user).attr('data-status', 2);
          }

          if ($(".user_" + e.target.dataset.user).text() == 'Following') {
            if (total != 0) {
              $(".total").text(parseInt(total) - 1);
            }
            $(".user_" + e.target.dataset.user).removeClass('follow');
            $(".user_" + e.target.dataset.user).text('Follow')
          } else {
            $(".total").text(parseInt(total) + 1);
            $(".user_" + e.target.dataset.user).addClass('follow');
            $(".user_" + e.target.dataset.user).text('Following')
          }
        } else {
        }
      },
      error => { }
    );
  }

  deletWallPost(id) {
    this.postid = id;
  }
  deleteConfirm() {
    this.mywalldata
      .removeWallPost({ post_id: this.postid })
      .pipe(first())
      .subscribe(
        (res: any) => {
          if (res.status) {
            this.showError = false;
            this.toaster.success(res.message);
            this.loadAuthorData();
          } else {
            this.toaster.error(res.message);
          }

        },
        error => {
          console.log("ERROR", error);
        }
      );
  }

  searchTag(text) {
    localStorage.setItem("search", text);
    this.router.navigate(["/search"]);
  }

  // get f() {
  //   return this.updateForm.controls;
  // }


  onSelectFile(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true
      let formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.postmessage.post_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.postimage = res;
            if (res.status) {
              $(".post-image").hide();
              $(".details_listing_txt_img1").show();
              $("#post-image").show();
              $("#post-image").attr('src', this._baseURL + '/' + res.data);
              this.updateForm.controls["post_image"].setValue(res.data);
              this.showLoader = false
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }
  viewImage(src) {
    this.imgSrc = src;
    $('#view-image').modal('show');
  }
  onSubmit() {
    this.loading = true;
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.controls.content.value == '') {
      this.showError = true;
      this.loading = false;
      return;
    }
    // if (this.updateForm.invalid) {
    //   this.loading = false;
    //   return;
    // }
    this.updateForm.controls.user_id.value = this.userid
    this.postmessage.savePost(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          // $("#myModal").modal('hide')
          $(".details_listing_txt_img1").hide();
          $("#post-image").hide();
          this.toaster.success('Post updated successfully.')
          this.loading = false;
          this.loadAuthorData();
          this.emptyForm();
          this.showError = false;
        } else {
          this.toaster.error(res.message);

        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

  emptyForm() {
    this.updateForm.controls["post_image"].setValue('');
    this.updateForm.controls["content"].setValue('');
  }
  CrossImage() {
    this.updateForm.controls["post_image"].setValue('');
    $("#post-image").attr('src', '');
    $(".details_listing_txt_img1").hide();
    $("#post-image").hide();
  }

}
