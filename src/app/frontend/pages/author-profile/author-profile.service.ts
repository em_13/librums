import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AuthorProfileService {
  userdata: any;
  baseurl: any;
  constructor(private http: HttpClient) {
    this.baseurl = BaseUrl.frontend;
  }

  getAuthor(author_id: string) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(
      this.baseurl + `/getAuthorProfile`,
      { author_id },
      { headers: headers }
    );
  }

  getAuthorCount(author_id: string) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(
      this.baseurl + `/getAuthorStoriesCount`,
      { author_id },
      { headers: headers }
    );
  }

  followAuther(data) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseurl + `/followUser`, data, {
      headers: headers
    });
  }
}
