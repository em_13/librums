import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ExploreService {
  userdata: any;
  baseuel: string;
  constructor(private http: HttpClient) {
    this.baseuel = BaseUrl.frontend;
  }

  explore(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/getBookListing`, postData, {
      headers
    });
  }
  loadMoreBooks(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/loadMoreBooks`, postData, {
      headers
    });
  }
}
