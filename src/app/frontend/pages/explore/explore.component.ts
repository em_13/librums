import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { ExploreService } from './explore.service';

declare var $: any;

@Component({
  selector: "app-explore",
  templateUrl: "./explore.component.html",
  styleUrls: ["./explore.component.css"]
})
export class ExploreComponent implements OnInit {
  /**scrol */
  page_no = 0;
  array = [];
  sum = 100;
  // throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  textUnder: any;
  modalOpen = false;
  /**scrol */
  activetab: 0;
  gethomedata: any;
  type: any;
  baseURL: any;
  showLoader = false;
  pagenumer = 0;
  pageNumber = 10;
  SlideOptions = {
    stagePadding: 350,
    loop: false,
    nav: false,
    dots: false,
    items: 1,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
        nav: false,
      },
      600: {
        items: 1,
        nav: false,
      },
      1000: {
        items: 1,
        nav: false,
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: false };
  constructor(
    private exploreS: ExploreService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.type = 'explore';
    // console.log(this.type);
    // this.activetab = this.activatedRoute.snapshot.paramMap.get("tabname");
    this.showLoader = true;
    this.loadHome();
    this.appendItems(0, this.sum);
  }

  ngAfterViewChecked() {
    $('.flipster').each(function (index, value) {
      $("#coverflow" + index).flipster({
        style: 'carousel',
        spacing: -0.3,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    });


  }

  loadHome() {
    this.exploreS.explore({ type: this.type }).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        this.showLoader = false;
        this.gethomedata = mapped;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  searchTag(text) {
    localStorage.setItem("search", text);
    this.router.navigate(["/search"]);
  }

  onExploreHome(id) {
    if ($("#coverflow" + id).find('.flip-items').find('li').length == 1) {
      $(".flip-items").css({ "width": "700px", "margin": "0 auto" })
    } else {
      $(".flip-items").css({ "width": "auto", "margin": "0" })
      $("#coverflow" + id).flipster({
        style: 'carousel',
        spacing: -0.3,
        buttons: true,
        start: 'center',
        loop: true,
        autoplay: false,
        click: true,
        scrollwheel: false,
      });
    }
  }

  /**Scroll */
  addItems(startIndex, endIndex, _method) {
    for (let i = 0; i < this.sum; ++i) {
      this.array[_method]([i, ' ', this.generateWord()].join(''));
    }
  }

  appendItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'push');
  }

  prependItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, 'unshift');
  }
  onScrollDown() {
    this.pagenumer = this.pagenumer + 1;
    let id = $(".nav-link.active").data('sectionvalue');
    let tabnamse = $(".nav-link.active").data('tabename');
    this.exploreS.loadMoreBooks({ genre_name: tabnamse, genre_id: id, type: this.type, page_no: this.pagenumer }).subscribe(
      (res: any) => {
        if (res.status) {
          this.pageNumber = this.pageNumber + 10;
          for (let i = 0; i < this.gethomedata.length; i++) {
            if (this.gethomedata[i].type == 'data') {
              for (let index = 0; index < this.gethomedata[i].value.length; index++) {
                if (this.gethomedata[i].value[index].tag == tabnamse) {
                  let resultArr = [];
                  resultArr = this.gethomedata[i].value[index].data.concat(res.data.data);
                  this.gethomedata[i].value[index].data = resultArr;
                }
              }
            }
          }
          console.log(this.gethomedata)
        }
      },
      error => {
        console.log("ERROR");
      }
    );
    // add another 20 items
    const start = this.sum;
    this.sum += 20;
    this.appendItems(start, this.sum);

    this.direction = 'down'
  }

  generateWord() {
    return Math.floor(Math.random() * 6) + 1;
  }
  /**Scroll */
}
