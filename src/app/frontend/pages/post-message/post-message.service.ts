import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class PostMessageService {
  postdata: any;
  postimage: any;
  _baseURL: any;
  _baseimage: any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
    this._baseimage = BaseUrl.imageApi;
  }

  savePost(data) {
    console.log(data);
    this.postdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.postdata.data.token
    });
    return this.http.post(this._baseURL + `/savePost`, data, {
      headers
    });
  }

  post_image(image) {
    this.postimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": this.postimage.data.token
    });
    return this.http.post(this._baseimage + `post_image`, image, {
      headers
    });
  }
}
