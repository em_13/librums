import { Component, OnInit } from "@angular/core";
import { PostMessageService } from "./post-message.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { BaseUrl } from "./../../../base-url";
import { pasteCleanupGroupingTags } from "@syncfusion/ej2-richtexteditor";
import { ToastrService } from "ngx-toastr";
declare var $: any;
@Component({
  selector: "app-post-message",
  templateUrl: "./post-message.component.html",
  styleUrls: ["./post-message.component.css"]
})
export class PostMessageComponent implements OnInit {
  getGenre: any;
  updateForm: any;
  loading = false;
  submitted = false;
  showMsg = "";
  errorMsg: any;
  alertService: any;
  selectedFiles: FileList;
  currentFileUpload: File;
  postimage: any;
  user_id: any;
  _baseURL: any;
  showLoader = false;
  constructor(
    private postmessage: PostMessageService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private toaster: ToastrService
  ) {
    this._baseURL = BaseUrl.image;
  }

  ngOnInit() {
    this.updateForm = this.formBuilder.group({
      user_id: this.user_id,
      content: ["", Validators.required],
      post_image: [""]
    });
  }

  onSelectFile(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true;
      let formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.postmessage.post_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.postimage = res;
            if (res.status) {
              $("#post-image").attr("src", this._baseURL + "/" + res.data);
              this.updateForm.controls["post_image"].setValue(res.data);
              this.showLoader = false;
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  get f() {
    return this.updateForm.controls;
  }
  onSubmit() {
    this.loading = true;
    this.submitted = true;
    this.user_id = $("#user_id").val();
    this.updateForm.value.user_id = $("#user_id").val();
    // stop here if form is invalid

    if (this.updateForm.invalid) {
      this.loading = false;
      return;
    }

    this.postmessage.savePost(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          $("#myModal").modal('hide')
          // this.router.navigate(['authorprofile/'+this.user_id]);
          this.toaster.success('Post submitted successfully.')
          this.loading = false;
        } else {
          this.toaster.error(res.message);
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
