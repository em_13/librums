import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ChapterAddService {
  addtype: any;
  base: string;
  chapterimage: any;
  baseimage: string;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.frontend;
    this.baseimage = BaseUrl.imageApi;
  }


  addChapter(data) {
    this.addtype = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.addtype.data.token
    });
    return this.http.post(this.base + `/addChapter`, data, {
      headers
    });
  }

  updateChapter(data) {
    this.addtype = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.addtype.data.token
    });
    return this.http.post(this.base + `/updateChapter`, data, {
      headers
    });
  }

  chapter_image(image) {
    this.chapterimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": this.chapterimage.data.token
    });
    return this.http.post(this.baseimage + `chapter_image`, image, {
      headers
    });
  }
}
