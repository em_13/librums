import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ChapterAddService } from "./chapter-add.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { BaseUrl } from "./../../../base-url";
import { VideoDetails } from './video-details';
import { DomSanitizer } from '@angular/platform-browser';
import { ToolbarService, LinkService, ImageService, HtmlEditorService } from '@syncfusion/ej2-angular-richtexteditor';
declare var $:any;
import MediumEditor from 'medium-editor';
@Component({
  selector: "app-chapter-add",
  templateUrl: "./chapter-add.component.html",
  styleUrls: ["./chapter-add.component.css"]
})
export class ChapterAddComponent implements OnInit {
 
  getGenre: any;
  updateForm: any;
  baseURL: any;
  loading = false;
  submitted = false;
  showMsg = false;
  errorMsg: any;
  alertService: any;
  book_id: string;
  selectedFiles: any;
  currentFileUpload: any;
  chapterimage: any;

  videoId: any;

  RichTextImage: any;

  messages:string;
  safeURL:any;
  results: VideoDetails[];
  loadings: boolean;
  message = '';
  childmessage: string ;
  videoURL="https://www.youtube.com/embed/1KT2asqA1J8";

  sourceValue : any;
  showData = false;
  is_white = "color_white";
  activeClass: any;
  
  bookData : any;
  showLoader = false;
  baseimage: any;


  image: any;
  title:any;
  video:any;
  html: any;
  prices: any = ['0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0', '1.5', '2.0']
  constructor(
    private addtype: ChapterAddService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private _sanitizer: DomSanitizer
  ) {
    this.baseURL = BaseUrl.image;
    this.videoId =  $("#videoid").val();
    this.baseURL = BaseUrl.image;
    this.baseimage = BaseUrl.imageApi;
    if(this.videoId){
      this.videoURL="https://www.youtube.com/embed/"+this.videoId;
      this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.videoURL);
    }
  }

  ngAfterViewInit() {

  }
  ngAfterViewChecked(){
    //check if reading setting is set
    let color = localStorage.getItem('color');
    let font = localStorage.getItem('font');
    let size = localStorage.getItem('size');
    if(color){
      this.is_white = color;
      this.activeClass = color;
      // $('body').on('load', '#cc').find('span').css('background', color);
    }
    if(size){
      // $('body').on('load', '#cc').find('p').css('font-size', size+'px');
      // $('body').on('load', '#cc').find('span').css('font-size', size+'px');
    }
    if(font){
    //  $('body').on('load', '#cc').find('p').css('font-family', font);
    //  $('body').on('load', '#cc').find('span').css('font-family', font);
   }
}

  ngOnInit() {
    this.book_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.updateForm = this.formBuilder.group({
      book_id: this.book_id,
      title: ["", Validators.required],
      content: [""],
      is_paid: [""],
      amount: [""],
      image: [""],
      video: [""],
      is_published: [""],
      is_completed: ['false']
      
    });

   this.bookData = localStorage.getItem('mybooks');
   this.bookData = JSON.parse(this.bookData);
   console.log(this.bookData);

    $( document ).on('click',".e-icon-btn", function(){
      if($(this).attr('aria-label') == 'Insert Image'){
        $("#toolbar_Image").modal('show')
        $("#rich-edit").val('true');
      }
    })
    // $(window).bind('beforeunload', function(){
    //   return "Do you want to exit this page?";
    // });

    var editor = new MediumEditor('#container');
    $(function () {
      $('#container').mediumInsert({
          editor: editor,
          
            toolbar: {
                /* These are the default options for the toolbar,
                   if nothing is passed this is what is used */
                //allowMultiParagraphSelection: true,
                buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote','redo','undo'],
               // diffLeft: 0,
               // diffTop: -10,
               // firstButtonClass: 'medium-editor-button-first',
               // lastButtonClass: 'medium-editor-button-last',
                //relativeContainer: null,
               // standardizeSelectionStart: false,
               // static: false,
                /* options which only apply when static is true */
                //align: 'center',
                //sticky: false,
                //updateOnEmptySelection: false
            },
          addons: {
            images: {
                fileUploadOptions: {
                  type: 'post',
                  url: 'https://librums.com:2001/api/uploadImageEditor', 
                  // url: 'http://18.209.143.118:8002/api/uploadImageEditor', 
                },
                uploadCompleted: function($el, data) {
                },
            }
        }
      });
    });
    $('#toggle-event').change(function() {
      $('#toggle-event').val($(this).prop('checked'));
      if($(this).prop('checked')){

        $('#amount').show();
        // $('#amount').attr('readonly', false);
        $('#amount').val('');
      }else{
        $('#amount').hide();
        // $('#amount').attr('readonly', true);
        $('#amount').val('0.00');
      }
        })
        $('#toggle-event').bootstrapToggle()
  }

  get f() {
    return this.updateForm.controls;
  }

  onSelectCoverImage(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true;
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.addtype.chapter_image(formData).subscribe(
        (res: any) => {
          if (res) {
            if (res.status) {
              $(".hello").show();
              $("#abc_frame").hide();
              this.chapterimage = res.data;
              this.updateForm.controls["image"].setValue(res.data);
              this.showLoader = false;
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  onSelectCoverImagePopup(event) {
    if (event.target.files.length > 0) {
      this.showLoader = true;
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.addtype.chapter_image(formData).subscribe(
        (res: any) => {
          if (res) {
            if (res.status) {
              this.RichTextImage = res.data;
              $(".e-content").append('<br /><img width=200 src="'+this.baseURL+this.RichTextImage+'">');
              $("#toolbar_Image").modal('hide')
              this.showLoader = false;
              $("#videoid").val('');
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  onSubmitSave(status) {
    // alert("Fffff");
    console.log(this.updateForm.value)
    this.submitted = true;
    // stop here if form is invalid
 
    this.updateForm.value.video = $("#videoid").val();
    $(".medium-insert-buttons").remove();
    this.updateForm.value.content = $(".medium-editor-element")[0].innerHTML;
    // this.updateForm.value.is_paid = (this.updateForm.value.is_paid)? "true":"false";
    this.updateForm.value.is_paid = ($('#toggle-event').prop('checked'))? 'true':'false';
   
    this.updateForm.value.amount = (!$('#toggle-event').prop('checked'))? "0.00":this.updateForm.value.amount;
   
    this.updateForm.value.is_completed =(this.updateForm.value.is_completed)?"true":"false" ;
    // console.log(this.updateForm.value.content)
    if(this.updateForm.invalid) {
      console.log('dfsff')
      return;
    }
    this.updateForm.value.is_published = status;
    this.loading = true;
    this.submitted = true;
    console.log(this.updateForm.value)
    this.addtype.addChapter(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.showMsg = true;
          this.router.navigate(["/mystories"]);
        } else {
          this.errorMsg = res.message;
        }
      },
      error => {
        this.loading = false;
      }
    );
  }

  updateResults(results: VideoDetails[]): void {
    this.results = results;
    if (this.results.length === 0) {
      this.message = 'Not found...';
    } else {
      this.message = 'Top 10 results:';
    }
  }

  public onOptionsSelected(event){
    console.log(this.updateForm.value.is_paid);
    // let dd = event.target;
    // this.sourceValue = dd.value;
 
    if(this.updateForm.value.is_paid === true)
    // if(this.sourceValue === 'true')
    {
      this.showData=true;
    }
    else{
      this.updateForm.value.amount = "0.00"
      this.showData=false;
    }
    console.log(this.showData)
  }
  
  public insertImageSettings = {
    saveUrl : 'https://aspnetmvc.syncfusion.com/services/api/uploadbox/Save'
  }


  PreviewClick(){
    this.title = $("#title").val();
    this.image = $(".chapterimage").attr('src');
    this.video = $("#abc_frame").attr("src");
    this.html =  $(".medium-editor-element")[0].innerHTML;
    let Arr = {'title': this.title, 'image':this.image, 'html': this.html, 'video': this.video}
    localStorage.setItem('previewPage', JSON.stringify(Arr));
  }
   
}
