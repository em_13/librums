import { TestBed } from '@angular/core/testing';

import { YouttubeSearchService } from './youttube-search.service';

describe('YouttubeSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: YouttubeSearchService = TestBed.get(YouttubeSearchService);
    expect(service).toBeTruthy();
  });
});
