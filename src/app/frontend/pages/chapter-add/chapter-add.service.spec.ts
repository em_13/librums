import { TestBed } from '@angular/core/testing';

import { ChapterAddService } from './chapter-add.service';

describe('ChapterAddService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChapterAddService = TestBed.get(ChapterAddService);
    expect(service).toBeTruthy();
  });
});
