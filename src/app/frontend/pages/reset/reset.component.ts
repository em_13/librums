import { Component, OnInit } from "@angular/core";
import { ResetService } from "./reset.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { Router, RouterStateSnapshot, ActivatedRoute } from "@angular/router";
import { AlertService } from "src/app/_services/alert.service";
import { first } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
import { MustMatch } from 'src/app/_helpers/must-match';
@Component({
  selector: "app-reset",
  templateUrl: "./reset.component.html",
  styleUrls: ["./reset.component.css"]
})
export class ResetComponent implements OnInit {
  resetForm: any;
  loading = false;
  submitted = false;
  showMsg = false;
  errorMsg = "";
  token: any;

  constructor(
    private resetService: ResetService,
    private formbuilder: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    if(localStorage.getItem("currentUser")){
      localStorage.removeItem("role");
      localStorage.removeItem("currentUser");
      localStorage.removeItem("welcome_mesage");
      location.reload(true);
    }
    
    this.token = this.activatedRoute.snapshot.paramMap.get("id");

    this.resetForm = this.formbuilder.group({
      token: this.token,
      new_password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ['', Validators.required]
    }, {
      validator: MustMatch('new_password', 'confirm_password')
   });

  }

  get f() {
    return this.resetForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.resetForm)
    if (this.resetForm.invalid) {
      return;
    }
    this.loading = true;
    this.resetService.resetPassword(this.resetForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.router.navigate(["/login"]);
          this.toaster.success('Password updated successfully.')
          this.reset();
        } else {
          this.toaster.error(res.message)
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

  private reset() {
    this.resetForm = this.formbuilder.group({
      new_password: [""],
      confirm_password: [""]
    });
  }

}
