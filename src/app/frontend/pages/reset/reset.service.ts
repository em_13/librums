import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ResetService {
  baseURL: string;
  constructor(private http: HttpClient) {
    this.baseURL = BaseUrl.frontend;
  }

  resetPassword(new_password) {
    // tslint:disable-next-line: align
    return this.http.post(this.baseURL + `/resetPassword`, new_password);
  }
}
