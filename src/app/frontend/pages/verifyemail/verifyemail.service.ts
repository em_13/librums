import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";
@Injectable({
  providedIn: 'root'
})
export class VerifyemailService {
  _baseURL : any;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  verifyEmail(token : any) {
   return this.http.get(this._baseURL + `/verifyEmail/`+token);
  }
}
