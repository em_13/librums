import { TestBed } from '@angular/core/testing';

import { VerifyemailService } from './verifyemail.service';

describe('VerifyemailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VerifyemailService = TestBed.get(VerifyemailService);
    expect(service).toBeTruthy();
  });
});
