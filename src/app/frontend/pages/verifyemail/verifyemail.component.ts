import { Component, OnInit } from '@angular/core';
import { VerifyemailService } from './verifyemail.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-verifyemail',
  templateUrl: './verifyemail.component.html',
  styleUrls: ['./verifyemail.component.css']
})
export class VerifyemailComponent implements OnInit {
token : any;
  constructor( private activatedRoute: ActivatedRoute,private verifyemail : VerifyemailService) { }

  ngOnInit() {
    this.token = this.activatedRoute.snapshot.paramMap.get("id");
   
    this.verifyEmail();
  }
  verifyEmail() {
    this.verifyemail.verifyEmail(this.token).subscribe(
      (res: any) => {
        if(res.status){
         
        }
        // console.log(res.data);
      },
      error => {
        console.log("ERROR", error);
      }
    );
  }
}
