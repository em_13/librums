import { Component, OnInit } from '@angular/core';
import { TermsService } from './terms.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {
  terms ;
  constructor(
    private termService: TermsService
  ) { }

  ngOnInit() {
    this.loadTerm();
  }
  private loadTerm(){
    this.termService.terms().pipe(first()).subscribe(terms => {
      this.terms = terms;
    });
  }

}
