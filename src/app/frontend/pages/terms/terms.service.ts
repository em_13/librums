import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";
@Injectable({
  providedIn: "root"
})
export class TermsService {
  _baseURL: string;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  terms() {
    return this.http.get(this._baseURL + `/termandcondition`);
  }
}
