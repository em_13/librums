import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class BookshelfService {
  userdata: any;
  baseuel: string;
  constructor(private http: HttpClient) {
    this.baseuel = BaseUrl.frontend;
  }

  getBookShelf() {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/getMyBookshelf`, null, {
      headers: headers
    });
  }
  removeFromBookshelf(postData) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });

    return this.http.post(this.baseuel + `/removeFromBookshelf`, postData, { headers });
  }

  loadMoreBooks(data){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/getMyBookshelf`, data, {
      headers: headers
    });
  }

  shareBook(data){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.baseuel + `/shareBook`, data, {
      headers: headers
    });
  }


}
