import { Component, OnInit } from "@angular/core";
import { BookshelfService } from "./bookshelf.service";
import { BaseUrl } from "./../../../base-url";
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/_services/alert.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: "app-bookshelf",
  templateUrl: "./bookshelf.component.html",
  styleUrls: ["./bookshelf.component.css"]
})
export class BookshelfComponent implements OnInit {
  bookshelfdata: any;
  baseurl: any;
  pagenumer = 0;
  pageNumber = 10;
  SlideOptions = {
    stagePadding: 200,
    loop: false,
    margin: 10,   
    nav: true,
    dots: false,
    items: 1,
    lazyLoad: false,
    responsive: {
      0: {
        items: 1,
        nav: true,
        stagePadding: 60
      },
      600: {
        items: 1,
        nav: true,
        stagePadding: 100
      },
      1000: {
        items: 1,
        nav: true,
        stagePadding: 200
      },
      1200: {
        items: 1,
        nav: true,
        stagePadding: 250
      },
      1400: {
        items: 1,
        nav: true,
        stagePadding: 300
      },
      1600: {
        items: 1,
        nav: true,
        stagePadding: 350
      },
      1800: {
        items: 2,
        nav: true,
        stagePadding: 300
      }
    }
  };
  CarouselOptions = { items: 1, dots: false, nav: true };
  
  constructor(private toaster: ToastrService,private bookshelf: BookshelfService, private router: Router,private alertService: AlertService) {
    this.baseurl = BaseUrl.image;
  }

  ngOnInit() {
    this.loadBookShelfData();
  }

  loadBookShelfData() {
    this.bookshelf.getBookShelf().subscribe(
      (res: any) => {
        if(res.status){
          this.bookshelfdata = res.data;
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  removeFromBookshelf( book_id) {
    this.bookshelf
      .removeFromBookshelf({ book_id: book_id })
      .pipe(first())
      .subscribe(
        (res: any) => {
          // this.alertService.success("User follow successfully", true);
          this.toaster.success(res.message);
          this.loadBookShelfData();
        },
        error => {
          console.log("ERROR", error);
        }
      );
  }
  reportBook(id) {
    localStorage.setItem("book_id",id);
    this.router.navigate(["/report-an-issue"]);
  }
  searchTag(text) {
    localStorage.setItem("search",text);
    this.router.navigate(["/search"]);
  }



  onScrollDown () {
    this.pagenumer = this.pagenumer+1;
    let id = $(".nav-link.active").data('sectionvalue');
    let tabnamse = $(".nav-link.active").data('tabename');
    this.bookshelf.loadMoreBooks({page_no:this.pagenumer }).subscribe( 
      (res: any) => {
        if(res.status){
          this.pageNumber = this.pageNumber+10;
          let resultArr = [];
          resultArr=  this.bookshelfdata.concat(res.data);
           this.bookshelfdata = resultArr;
          console.log( this.bookshelfdata)
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  sharePop(book_id){
    $(".share-social").each(function(key, index){
      $(index).find('i').attr('data-id', book_id)
      // $(index).data('id', book_id)
      if($(index).data('type') == 'facebook'){
        $(index).attr('data-id', book_id)
        $(index).attr('href', "https://www.facebook.com/sharer/sharer.php?u=https://librums.com/shareBook.html?book_id="+book_id+"&quote=If you have a family of avid readers, good news: librums makes it pretty easy to share books with every member of your family, So here it is please check my latest book.")
      }
      if($(index).data('type') == 'twitter'){
        $(index).data('id', book_id)
        $(index).attr('href', "https://twitter.com/share?url=https://librums.com/shareBook.html?book_id="+book_id+"&quote=If you have a family of avid readers, good news: librums makes it pretty easy to share books with every member of your family, So here it is please check my latest book.")
      }
      if($(index).data('type') == 'linkedin'){
        $(index).data('id', book_id)
        $(index).attr('href', "https://www.linkedin.com/shareArticle?mini=true&url=https://librums.com/shareBook.html?book_id="+book_id+"&quote=If you have a family of avid readers, good news: librums makes it pretty easy to share books with every member of your family, So here it is please check my latest book.")
      }
      if($(index).data('type') == 'pinterest'){
        $(index).data('id', book_id)
        $(index).attr('href', "http://pinterest.com/pin/create/button/?url=https://librums.com/shareBook.html?book_id="+book_id+"&description=If you have a family of avid readers, good news: librums makes it pretty easy to share books with every member of your family, So here it is please check my latest book.")
      }
      if($(index).data('type') == 'envelope'){
        $(index).data('id', book_id)
        $(index).attr('href', "mailto:support@librums.com?Subject=Share Book With your Family&Body=If you have a family of avid readers, good news: librums makes it pretty easy to share books with every member of your family, So here it is please check my latest book. https://librums.com/shareBook.html?book_id="+book_id)
      }
      if($(index).data('type') == 'whatsapp'){
        $(index).data('id', book_id)
        $(index).attr('href', "https://web.whatsapp.com/send?text=If you have a family of avid readers, good news: librums makes it pretty easy to share books with every member of your family, So here it is please check my latest book. https://librums.com/shareBook.html?book_id="+book_id)
      }
    })
  }

  shareBook(e){
    this.bookshelf
    .shareBook({ book_id: e.target.dataset.id })
    .pipe(first())
    .subscribe(
      (res: any) => {
        $("#forgot").modal('hide')
        this.loadBookShelfData();
      },
      error => {
        console.log("ERROR", error);
      }
    );
  }
}
