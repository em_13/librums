import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharebookComponent } from './sharebook.component';

describe('SharebookComponent', () => {
  let component: SharebookComponent;
  let fixture: ComponentFixture<SharebookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharebookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharebookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
