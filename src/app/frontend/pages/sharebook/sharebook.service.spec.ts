import { TestBed } from '@angular/core/testing';

import { SharebookService } from './sharebook.service';

describe('SharebookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharebookService = TestBed.get(SharebookService);
    expect(service).toBeTruthy();
  });
});
