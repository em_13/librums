import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BaseUrl } from "./../../../base-url";
@Injectable({
  providedIn: 'root'
})
export class SharebookService {
  userdata: any;
  baseURL: string;
  constructor(
    private http: HttpClient
  ) { 
    this.baseURL = BaseUrl.frontend;
  }

  shareBook(data){
    return this.http.post(this.baseURL + `/getShareBookById`, data);
  }
}
