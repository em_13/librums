import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharebookService } from './sharebook.service';
import { first } from 'rxjs/operators';
import { BaseUrl } from "./../../../base-url";
@Component({
  selector: 'app-sharebook',
  templateUrl: './sharebook.component.html',
  styleUrls: ['./sharebook.component.css']
})
export class SharebookComponent implements OnInit {
  token:any;
  bookdata:any;
  baseurl: any;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sharebokservice: SharebookService
  ) { 
    this.baseurl = BaseUrl.image;
  }

  ngOnInit() {
    this.token = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadDate();
  }

  loadDate(){
    this.sharebokservice.shareBook({book_id: this.token}).subscribe(
      (res: any) => {
        if(res.status){
          this.bookdata = res.data;
          console.log(this.bookdata)
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  searchTag(text) {
    localStorage.setItem("search",text);
    this.router.navigate(["/search"]);
  }

}
