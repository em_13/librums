import { Component, OnInit } from "@angular/core";
import { BaseUrl } from "./../../../base-url";
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/_services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuhorStoriesService } from './auhor-stories.service';
import { SEOServiceService } from 'src/app/seoservice.service';

@Component({
  selector: 'app-author-stories',
  templateUrl: './author-stories.component.html',
  styleUrls: ['./author-stories.component.css']
})
export class AuthorStoriesComponent implements OnInit {

  pagenumber : any;
  activepage: number;
  bookshelfdata: any;
  baseurl: any;
  showLoader = false;
  pagenumer = 0;
  pageNumber = 10;
  userName : any;
  autorid: any;
  type: any;
  
  constructor(private bookshelf: AuhorStoriesService, private router: Router, private activatedRoute: ActivatedRoute, private _seoService: SEOServiceService) {
    this.baseurl = BaseUrl.image;
  }

  ngOnInit() {
    this.autorid = this.activatedRoute.snapshot.paramMap.get("id");
    this.type = this.activatedRoute.snapshot.paramMap.get("type");
    this.loadBookShelfData();
  }

  loadBookShelfData() {
    this.bookshelf.gethomedata({type:this.type, author_id:this.autorid, page_no:0}).subscribe(
      (res: any) => {
        if(res.status){
          if(res.total <= 10){
          }else{
             this.pagenumber = res.total/10;
            this.pagenumber = Math.ceil(this.pagenumber);
            this.counter(this.pagenumber)
          }
          this.bookshelfdata = res.data;
          this.userName = res.username;
          this.activepage = 0;
          this.showLoader = false;
          this._seoService.updateTitle('Author | Stories By '+this.userName);  
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  counter(i: number) {
    return new Array(i);
  }

  pageChange(number){
    this.bookshelf.gethomedata({ type:this.type, author_id:this.autorid,page_no:number}).subscribe(
      (res: any) => {
        if(res.status){

          if(res.total == 10){
            
          }else{
            this.pagenumber = res.total/10;
            this.pagenumber = Math.ceil(this.pagenumber);
            this.counter(this.pagenumber)
          }
          this.bookshelfdata = res.data;
          this.activepage = number;
          this.showLoader = false;

        
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  searchTag(text) {
    localStorage.setItem("search",text);
    this.router.navigate(["/search"]);
  }

}
