import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorStoriesComponent } from './author-stories.component';

describe('AuthorStoriesComponent', () => {
  let component: AuthorStoriesComponent;
  let fixture: ComponentFixture<AuthorStoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorStoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorStoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
