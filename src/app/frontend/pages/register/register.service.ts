import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseUrl } from "../../../base-url";
@Injectable({
  providedIn: "root"
})
export class RegisterService {
  _baseURL: string;
  constructor(private http: HttpClient) {
    this._baseURL = BaseUrl.frontend;
  }

  register(data) {
    return this.http.post(this._baseURL + `/register`, data);
  }
}
