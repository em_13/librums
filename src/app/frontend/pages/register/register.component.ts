import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { RegisterService } from './register.service';
import { MustMatch } from 'src/app/_helpers/must-match';
import { AlertService } from 'src/app/_services/alert.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import {IMyDpOptions} from 'mydatepicker';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService, SocialUser } from 'angular-6-social-login';  
import { SocialloginServiceService } from 'src/app/_services/sociallogin-service.service';
import { LoginService } from '../logins/login.service';
import { ToastrService } from 'ngx-toastr';

declare var $:any;


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  ForgetForm: FormGroup;
  loading = false;
  submitted = false;
  myDate = new Date();
  msgSuccess: any;
  msgError: any;
public model:any;
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    showTodayBtn: false,
    minYear : 1900,
   
    disableSince: {year: this.myDate.getFullYear(), month: this.myDate.getMonth() + 1, day: this.myDate.getDate()}
  };



  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registerService: RegisterService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private authService: AuthService,
    public OAuth: AuthService,  
    private SocialloginService: SocialloginServiceService,
    private loginService: LoginService,
    private toastr: ToastrService
  ) {
    console.log({year: this.myDate.getFullYear(), month: this.myDate.getMonth() + 1, day: this.myDate.getDate()});
 
       // redirect to home if already logged in
      if (this.authenticationService.currentUserValue) {
          this.router.navigate(['/']);
      }
   }

   // Initialized to fbLibrary login.
   fbLibrary() {
    (window as any).fbAsyncInit = function() {
      window['FB'].init({
        appId      : '2460638890823149',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      });
      window['FB'].AppEvents.logPageView();
    };
 
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  }

    // Initialized to google login.
   prepareLoginButton() {
    let socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.OAuth.signIn(socialPlatformProvider).then(socialusers => {  
      this.googleLogin(socialusers);  
    }); 
  }

     // Initialized to specific date (09.10.2018).
  

  ngOnInit() {
    // this.toastr.success("Your account was successfully created. We have sent an e-mail to confirm your account.")

    this.registerForm = this.formBuilder.group({
      fullname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      dob: ['', Validators.required],
      username: [null, [
        Validators.required,Validators.pattern(/^(?=[A-Za-z0-9 ])(?=.{4,})/)]
      ],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    },
    {
        validator: MustMatch('password', 'confirmPassword')
    });
    var specialKeys = new Array();
    specialKeys.push(8);  //Backspace
    specialKeys.push(9);  //Tab
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right
    $(document).on('keyup','#username', function (e) {
      var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
      var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
      return ret;
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }


  onSubmit() {
    this.submitted = true;
    if(this.registerForm.value.dob == undefined){
      this.registerForm.value.dob = '';
    }
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.registerForm.value.username = this.registerForm.value.username.toLowerCase();
    this.registerForm.value.email = this.registerForm.value.email.toLowerCase();

    this.registerForm.value.dob = this.registerForm.value.dob.date.day+"/"+this.registerForm.value.dob.date.month+"/"+this.registerForm.value.dob.date.year;
    this.loading = true;
    this.registerService.register(this.registerForm.value)
        .pipe(first())
        .subscribe(
            (res: any) => {
              if(res.status){
                this.loading = false;
                this.msgError = '';
                this.router.navigate(['/login']);
                this.toastr.success("Your account was created successfully. We have sent an e-mail to confirm your account.")
              }else{
                this.toastr.error(res.message)
                this.loading = false;
              }
            },
            error => {
              this.loading = false;
            });
  }


  public googleLogin(userInfo){
    this.loginService.sociallogin({'type': 2, 'social_id': userInfo.id, 'device_token': 'web_toke', 'email': userInfo.email, 'fullname': userInfo.name }).subscribe(
      (res: any) => {
        if (res.status) {
          if (res.role === 1) {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "admin");
             window.location.href = "/";
          } else {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "user");
            window.location.href = "/";
          }
        } else {
          this.msgError = res.message;
          this.loading = false;
        }
      },
      error => {
        this.loading = false;
      }
    );
  }

  login() {
    window['FB'].login((response) => {
        console.log('login response',response);
        if (response.authResponse) {
         
          window['FB'].api('/me', {
            fields: 'last_name, first_name, email, birthday, gender',
          }, (userInfo) => {
            this.loginService.checkUserExist({'value': userInfo.email, 'type': 1, 'social_type': 1, 'social_id': userInfo.id}).subscribe(
              (res: any) => {
                if (res.status) {
                  this.facebooklogin(userInfo);
                } else {
                  this.msgError = res.message;
                  
                }
              },
              error => {
                this.loading = false;
              }
            );

          });
           
        } else {
          console.log('User login failed');
        }
    }, {scope: 'email'});
  }

  public facebooklogin(userInfo){
    this.loginService.sociallogin({'type': 1, 'social_id': userInfo.id, 'device_token': 'web_toke', 'email': userInfo.email, 'fullname': userInfo.first_name + ' ' + userInfo.last_name }).subscribe(
      (res: any) => {
        if (res.status) {
          if (res.role === 1) {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "admin");
           window.location.href = "/";
          } else {
            localStorage.setItem("currentUser", JSON.stringify(res));
            localStorage.setItem("role", "user");
           window.location.href = "/";
          }
        } else {
          this.msgError = res.message;
        }
      },
      error => {
        this.loading = false;
      }
    );
  }

  omit_special_char(event)
  {   
    var k;  
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    if (k == 32) return false;
    return((k > 64 && k < 91) || (k > 94 && k < 123) || k == 8 || k == 32 || (k >= 46 && k <= 57)); 
  }

}
