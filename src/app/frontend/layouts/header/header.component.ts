import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { BaseUrl } from 'src/app/base-url';
import { Observable } from 'rxjs';
import { UrlsService } from 'src/app/urls.service';
// import { BaseUrl } ;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  headerShow = false;
  sidebar = false;
  usersUpdate : any;
  baseimage: any;
  userType : any;
  previousUrl: string = '';
  constructor( private authenticationService: AuthenticationService, private urlsService: UrlsService) {
    // redirect to home if already logged in
      if (this.authenticationService.currentUserValue) {
        if (localStorage.getItem('role') === 'admin') {
            this.sidebar = true;
            this.headerShow = false;
        } else {
            this.headerShow = true;
            this.sidebar = false;
        }
      } else {
        this.headerShow = false;
      }

      this.baseimage = BaseUrl.image;
   }

  ngOnInit() {
    this.usersUpdate = JSON.parse(localStorage.getItem("currentUser"));
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      if (localStorage.getItem('role') === 'admin') {
          this.sidebar = true;
          this.headerShow = false;
      } else {
          this.headerShow = true;
          this.sidebar = false;
      }
      console.log( this.usersUpdate)
    } else {
      this.headerShow = false;
    }
    this.urlsService.previousUrl$
        .subscribe((previousUrl: string) => {
            this.previousUrl = previousUrl
        });
        console.log("previousUrl"+this.previousUrl);
  }
}
