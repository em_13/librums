import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule, PreloadingStrategy } from "@angular/router";
import { RegisterComponent } from "./frontend/pages/register/register.component";
import { LoginsComponent } from "./frontend/pages/logins/logins.component";
import { HomeComponent } from "./frontend/pages/home/home.component";
import { AuthGuard } from "./_guard/auth.guard";
import { AboutComponent } from "./frontend/pages/about/about.component";
import { TermsComponent } from "./frontend/pages/terms/terms.component";
import { PrivacyComponent } from "./frontend/pages/privacy/privacy.component";
import { ReportComponent } from "./frontend/pages/report/report.component";
import { ExploreComponent } from "./frontend/pages/explore/explore.component";
import { BookshelfComponent } from "./frontend/pages/bookshelf/bookshelf.component";
import { NotificationsComponent } from "./frontend/pages/notifications/notifications.component";
import { LogoutComponent } from "./frontend/pages/logout/logout.component";
import { FeedbackComponent } from "./frontend/pages/account/feedback/feedback.component";
import { HelpComponent } from "./frontend/pages/help/help.component";
import { DashboardComponent } from "./backend/pages/dashboard/dashboard.component";
import { UsersComponent } from "./backend/pages/users/users.component";
import { BackendFeedbackComponent } from "./backend/pages/backend-feedback/backend-feedback.component";
import { AdvertisementsComponent } from "./backend/pages/advertisements/advertisements.component";
import { BackendNotificationsComponent } from "./backend/pages/backend-notifications/backend-notifications.component";
import { ManageContentComponent } from "./backend/pages/manage-content/manage-content.component";
import { MessageBoardComponent } from "./backend/pages/message-board/message-board.component";
import { PaymentsDashboardComponent } from "./backend/pages/payments-dashboard/payments-dashboard.component";
import { EditUsersComponent } from "./backend/pages/edit-users/edit-users.component";
import { ViewUsersComponent } from "./backend/pages/view-users/view-users.component";
import { ProfileComponent } from "./backend/pages/profile/profile.component";
import { FrontProfileComponent } from "./frontend/pages/profile/profile.component";
import { ChangePasswordComponent } from "./backend/pages/change-password/change-password.component";
import { EditManageContentComponent } from "./backend/pages/edit-manage-content/edit-manage-content.component";
import { ViewManageContentComponent } from "./backend/pages/view-manage-content/view-manage-content.component";
import { ViewChapterContentComponent } from "./backend/pages/view-chapter-content/view-chapter-content.component";
import { EditChapterContentComponent } from "./backend/pages/edit-chapter-content/edit-chapter-content.component";
import { FollowersComponent } from "./frontend/pages/followers/followers.component";
import { FollowingComponent } from "./frontend/pages/following/following.component";
import { AuthorProfileComponent } from "./frontend/pages/author-profile/author-profile.component";
import { ChangepasswordComponent } from "./frontend/pages/account/changepassword/changepassword.component";
import { BookDetailComponent } from "./frontend/pages/book-detail/book-detail.component";
import { HomeDetailComponent } from "./frontend/pages/home-detail/home-detail.component";
import { ViewUserDetaliComponent } from "./backend/pages/view-user-detali/view-user-detali.component";
import { ManagePagesComponent } from "./backend/pages/manage-pages/manage-pages.component";
import { ViewManagePageComponent } from "./backend/pages/view-manage-page/view-manage-page.component";
import { EditManagePageComponent } from "./backend/pages/edit-manage-page/edit-manage-page.component";
import { SubscriptionComponent } from "./frontend/pages/subscription/subscription.component";
import { MystoriesComponent } from "./frontend/pages/mystories/mystories.component";
import { MybookdetailsComponent } from "./frontend/pages/mybookdetails/mybookdetails.component";
import { EditProfileComponent } from "./frontend/pages/account/edit-profile/edit-profile.component";
import { AddStoriesComponent } from "./frontend/pages/add-stories/add-stories.component";
import { ManageGenreComponent } from "./backend/pages/manage-genre/manage-genre.component";
import { ManageContentTypeComponent } from "./backend/pages/manage-content-type/manage-content-type.component";
import { ManageCopyrightComponent } from "./backend/pages/manage-copyright/manage-copyright.component";
import { EditManageGenreComponent } from "./backend/pages/edit-manage-genre/edit-manage-genre.component";
import { EditManageContentTypeComponent } from "./backend/pages/edit-manage-content-type/edit-manage-content-type.component";
import { EditManageCopyrightComponent } from "./backend/pages/edit-manage-copyright/edit-manage-copyright.component";
import { AddManageGenreComponent } from "./backend/pages/add-manage-genre/add-manage-genre.component";
import { AddManageContentTypeComponent } from "./backend/pages/add-manage-content-type/add-manage-content-type.component";
import { AddManageCopyrightComponent } from "./backend/pages/add-manage-copyright/add-manage-copyright.component";
import { ReadChapterComponent } from "./frontend/pages/read-chapter/read-chapter.component";
import { User } from "./_models/user";
import { PostMessageComponent } from "./frontend/pages/post-message/post-message.component";
import { SearchComponent } from "./frontend/pages/search/search.component";
import { BookReportComponent } from "./backend/pages/book-report/book-report.component";
import { ChapterAddComponent } from "./frontend/pages/chapter-add/chapter-add.component";
import { ResetComponent } from "./frontend/pages/reset/reset.component";
import { SharebookComponent } from "./frontend/pages/sharebook/sharebook.component";
import { ErrorPageComponent } from "./frontend/pages/error-page/error-page.component";
import { AdminSubscriptionComponent } from "./backend/pages/admin-subscription/admin-subscription.component";
import { AdminAddSubscriptionComponent } from "./backend/pages/admin-add-subscription/admin-add-subscription.component";
import { AdminViewSubscriptionComponent } from "./backend/pages/admin-view-subscription/admin-view-subscription.component";
import { AdminEditSubscriptionComponent } from "./backend/pages/admin-edit-subscription/admin-edit-subscription.component";
import { ViewMessageBoardComponent } from "./backend/pages/view-message-board/view-message-board.component";
import { ViewFeedbackComponent } from "./backend/pages/view-feedback/view-feedback.component";
import { ViewBookReportComponent } from "./backend/pages/view-book-report/view-book-report.component";
import { InviteFriendComponent } from "./frontend/pages/invite-friend/invite-friend.component";
import { PaymentComponent } from "./frontend/pages/payment/payment.component";
import { EditChapterComponent } from './frontend/pages/edit-chapter/edit-chapter.component';
import { ReadingSettingComponent } from './frontend/pages/reading-setting/reading-setting.component';
import { MyWallComponent } from './frontend/pages/my-wall/my-wall.component';
import { EditstoriesComponent } from './frontend/pages/editstories/editstories.component';
import { SingleBookComponent } from './frontend/pages/single-book/single-book.component';
import { ShareSingleBookComponent } from './frontend/pages/share-single-book/share-single-book.component';
import { WithdrawalComponent } from './frontend/pages/withdrawal/withdrawal.component';
import { ChapterPaymentsComponent } from './backend/pages/chapter-payments/chapter-payments.component';
import { PreviewChapterComponent } from './frontend/pages/preview-chapter/preview-chapter.component';
import { AnnoucementComponent } from './backend/pages/annoucement/annoucement.component';
import { FrontendAnnoucementComponent } from './frontend/pages/frontend-annoucement/frontend-annoucement.component';
import { AuthorStoriesComponent } from './frontend/pages/author-stories/author-stories.component';
import { SinglebookComponent } from './frontend/pages/singlebook/singlebook.component';
import { ViewchapterComponent } from './backend/pages/viewchapter/viewchapter.component';
import { ManageNotificationComponent } from './backend/pages/manage-notification/manage-notification.component';
import { VerifyemailComponent } from './frontend/pages/verifyemail/verifyemail.component';
import { PublishedBooksComponent } from './backend/pages/published-books/published-books.component';
import { PublishedBookViewComponent } from './backend/pages/published-books/published-book-view/published-book-view.component';
import { PublishedBookContentComponent } from './backend/pages/published-books/published-book-content/published-book-content.component';
import { AuthorListComponent } from './backend/pages/author-list/author-list.component';
import { EditAuthorComponent } from './backend/pages/author-list/edit-author/edit-author.component';
import { ContactUsComponent } from './backend/pages/contact-us/contact-us.component';
import { AllBooksComponent } from './frontend/pages/all-books/all-books.component';
import { ManageSliderComponent } from './backend/pages/manage-slider/manage-slider.component';
import { AddSliderComponent } from './backend/pages/add-slider/add-slider.component';
import { ViewAllComponent } from './frontend/pages/view-all/view-all.component';
const routes: Routes = [
  { path: "sharebook/:id", component: ShareSingleBookComponent , data: {title: 'ShareBook'}},
  { path: "register", component: RegisterComponent, data: {title: 'Register'}  },
  { path: "login", component: LoginsComponent, data: {title: 'Login'} },
  { path: "verify-email/:id", component: VerifyemailComponent, data: {title: 'Verify Email'} },
  { path: "about-us", component: AboutComponent, data: {title: 'About Us'} },
  { path: "terms-conditions", component: TermsComponent, data: {title: 'Terms And Conditions'} },
  { path: "privacy-policy", component: PrivacyComponent, data: {title: 'Privacy Policy'} },
  { path: "", component: HomeComponent, canActivate: [AuthGuard] , data: {title: 'Home'}},
  { path: "home", component: HomeComponent, canActivate: [AuthGuard], data: {title: 'Home'} },
  { path: "report-an-issue", component: ReportComponent, canActivate: [AuthGuard], data: {title: 'Report an Issue'} },
  { path: "feedback", component: FeedbackComponent, canActivate: [AuthGuard], data: {title: 'Feedback'} },
  { path: "contact-us", component: HelpComponent, data: {title: 'Contact Us'} },
  { path: "explore", component: ExploreComponent, canActivate: [AuthGuard], data: {title: 'Explore'} },
  { path: "withdrawal", component: WithdrawalComponent, canActivate: [AuthGuard] , data: {title: 'Withdrawal'}},
  {
    path: "subscription",
    component: SubscriptionComponent,
    canActivate: [AuthGuard],
    data: {title: 'Subscription'}
  },
  {
    path: "mywall",
    component: MyWallComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Wall'}
  },
  {
    path: "user-annoucement",
    component: FrontendAnnoucementComponent,
    canActivate: [AuthGuard],
    data: {title: 'Announcements'}
  },
  {
    path: "mystories",
    component: MystoriesComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Stories'}
  },
  {
    path: "mybookdetails/:id",
    component: MybookdetailsComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Book Details'}
  },
  {
    path: "author/stories/:type/:id",
    component: AuthorStoriesComponent,
    canActivate: [AuthGuard],
    data: {title: 'Author | Stories'}
  },
  {
    path: "view-all/mystories/:type",
    component: ViewAllComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Stories'}
  },
  {
    path: "readchapter/:book_id/:chapter_id",
    component: ReadChapterComponent,
    canActivate: [AuthGuard],
    data: {title: 'Read Chapter'}
  },
  {
    path: "editchapter/:book_id/:chapter_id",
    component: EditChapterComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Chapter'}
  },
  {
    path: "addstories",
    component: AddStoriesComponent,
    canActivate: [AuthGuard],
    data: {title: 'Add Stories'}
  },
  {
    path: "notification",
    component: NotificationsComponent,
    canActivate: [AuthGuard],
    data: {title: 'Notification'}
  },
  {
    path: "bookshelf",
    component: BookshelfComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Bookshelf'}
  },
  { path: "logout", component: LogoutComponent, canActivate: [AuthGuard] },
  {
    path: "followers",
    component: FollowersComponent,
    canActivate: [AuthGuard],
    data: {title: 'Followers'}
  },
  {
    path: "following",
    component: FollowingComponent,
    canActivate: [AuthGuard],
    data: {title: 'Following'}
  },
  {
    path: "myprofile",
    component: FrontProfileComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Profile'}
  },
  {
    path: "most_read",
    component: HomeDetailComponent,
    canActivate: [AuthGuard],
    data: {title: 'Home Details'}
  }, 
  {
    path: "last_updated",
    component: HomeDetailComponent,
    canActivate: [AuthGuard],
    data: {title: 'Home Details'}
  }, 
  {
    path: "most_liked",
    component: HomeDetailComponent,
    canActivate: [AuthGuard],
    data: {title: 'Home Details'}
  }, 
  {
    path: "new_stories",
    component: HomeDetailComponent,
    canActivate: [AuthGuard],
    data: {title: 'Home Details'}
  },
  {
    path: "view-all/:id",
    component: AllBooksComponent,
    canActivate: [AuthGuard],
    data: {title: 'All Books'}
  },
  {
    path: "authorprofile/:id",
    component: AuthorProfileComponent,
    canActivate: [AuthGuard],
    data: {title: 'Author Profile'}
  },
  {
    path: "bookdetail/:type/:id/:genre_id",
    component: BookDetailComponent,
    canActivate: [AuthGuard],
    data: {title: 'Book Details'}
  },
  {
    path: "singlebook/:id",
    component: SingleBookComponent,
    canActivate: [AuthGuard],
    data: {title: 'Single Book'}
  },
  {
    path: "single-book/:id",
    component: SinglebookComponent,
    canActivate: [AuthGuard],
    data: {title: 'Single Book'}
  },

  {
    path: "myprofile/changepassword",
    component: ChangepasswordComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Profile | Change Password'}
  },
  {
    path: "myprofile/editprofile",
    component: EditProfileComponent,
    canActivate: [AuthGuard],
    data: {title: 'My Profile | Edit Profile'}
  },
  {
    path: "post-message/:id",
    component: PostMessageComponent,
    canActivate: [AuthGuard],
    data: {title: 'Post Message'}
  },
  {
    path: "search",
    component: SearchComponent,
    canActivate: [AuthGuard],
    data: {title: 'Search'}
  },

  // admin section route
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: {title: 'Dashboard'}
  },
  {
    path: "manage-contact-us",
    component: ContactUsComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Contact Us'}
  },
  {
    path: "manage-slider",
    component: ManageSliderComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Sliders'}
  },
  {
    path: "add-slider",
    component: AddSliderComponent,
    canActivate: [AuthGuard],
    data: {title: 'Add Slider'}
  },
  { path: "users", component: UsersComponent, canActivate: [AuthGuard], data: {title: 'Users'} },
  { path: "authors", component: AuthorListComponent, canActivate: [AuthGuard], data: {title: 'Authors'} },
  {
    path: "feedbacks-and-suggestions",
    component: BackendFeedbackComponent,
    canActivate: [AuthGuard],
    data: {title: 'Feedback and Suggestions'}
  },
  {
    path: "view-feedback/:id",
    component: ViewFeedbackComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Feedback'}
  },
  {
    path: "advertisements",
    component: AdvertisementsComponent,
    canActivate: [AuthGuard],
    data: {title: 'Advertisments'}
  },
  {
    path: "backend-notifications",
    component: BackendNotificationsComponent,
    canActivate: [AuthGuard],
    data: {title: 'Notifications'}
  },
  {
    path: "manage-notifications",
    component: ManageNotificationComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Notifications'}
  },
  {
    path: "announcement",
    component: AnnoucementComponent,
    canActivate: [AuthGuard],
    data: {title: 'Announcements'}
  },
  {
    path: "manage-books",
    component: ManageContentComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Books'}
  },
  {
    path: "published-books",
    component: PublishedBooksComponent,
    canActivate: [AuthGuard],
    data: {title: 'Published Books'}
  },
  {
    path: "view-published-book/:id",
    component: PublishedBookViewComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Published Book'}
  },
  {
    path: "published-book-content/:id",
    component: PublishedBookContentComponent,
    canActivate: [AuthGuard],
    data: {title: 'Published Book Content'}
  },
  {
    path: "reported-issues",
    component: MessageBoardComponent,
    canActivate: [AuthGuard],
    data: {title: 'Reported Issues'}
  },
  {
    path: "view-message-board/:id",
    component: ViewMessageBoardComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Reported Issue'}
  },
  {
    path: "chapter-preview",
    component: PreviewChapterComponent,
    canActivate: [AuthGuard],
    data: {title: 'Chapter Preview'}
  },
  {
    path: "chapter-payments",
    component: ChapterPaymentsComponent,
    canActivate: [AuthGuard],
    data: {title: 'Chapter Payments'}
  },
  {
    path: "subscription-payments",
    component: PaymentsDashboardComponent,
    canActivate: [AuthGuard],
    data: {title: 'Subscription Payment'}
  },
  {
    path: "edit-users/:id",
    component: EditUsersComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit User'}
  },
  {
    path: "edit-author/:id",
    component: EditAuthorComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Author'}
  },
  {
    path: "view-user-content/:id",
    component: ViewUsersComponent,
    canActivate: [AuthGuard],
    data: {title: 'View User'}
  },
  {
    path: "profile/:id",
    component: ProfileComponent,
    canActivate: [AuthGuard],
    data: {title: 'Profile'}
  },
  {
    path: "change-password",
    component: ChangePasswordComponent,
    canActivate: [AuthGuard],
    data: {title: 'Change Password'}
  },
  {
    path: "edit-manage-content/:id",
    component: EditManageContentComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit content'}
  },
  {
    path: "view-manage-content/:id",
    component: ViewManageContentComponent,
    canActivate: [AuthGuard],
    data: {title: 'List of Chapters '}
  },
  {
    path: "view-chapter-content/:id",
    component: ViewChapterContentComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Chapter'}
  },
  {
    path: "viewchapter/:id",
    component: ViewchapterComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Chapter'}
  },
  {
    path: "edit-chapter-content/:id",
    component: EditChapterContentComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Chapter'}
  },
  {
    path: "view-user-detail/:id",
    component: ViewUserDetaliComponent,
    canActivate: [AuthGuard],
    data: {title: 'View User Details'}
  },
  {
    path: "manage-pages",
    component: ManagePagesComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Pages'}
  },
  {
    path: "edit-manage-page/:id",
    component: EditManagePageComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Page'}
  },
  {
    path: "view-manage-page/:id",
    component: ViewManagePageComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Page'}
  },
  {
    path: "manage-genre",
    component: ManageGenreComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Genres'}
  },
  {
    path: "manage-content-type",
    component: ManageContentTypeComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Content Types'}
  },
  {
    path: "manage-copyright",
    component: ManageCopyrightComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Copyrights'}
  },
  {
    path: "editstories/:id",
    component: EditstoriesComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Stories'}
  },
  {
    path: "edit-manage-genre/:id",
    component: EditManageGenreComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Manage Genre'}
  },
  {
    path: "edit-manage-content-type/:id",
    component: EditManageContentTypeComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Content Type'}
  },
  {
    path: "edit-manage-copyright/:id",
    component: EditManageCopyrightComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Copyright'}
  },
  {
    path: "add-manage-genre",
    component: AddManageGenreComponent,
    canActivate: [AuthGuard],
    data: {title: 'Add Gerne'}
  },
  {
    path: "add-manage-content-type",
    component: AddManageContentTypeComponent,
    canActivate: [AuthGuard],
    data: {title: 'Add Content Type'}
  },
  {
    path: "add-manage-copyright",
    component: AddManageCopyrightComponent,
    canActivate: [AuthGuard],
    data: {title: 'Add Copyright'}
  },
  {
    path: "book-report",
    component: BookReportComponent,
    canActivate: [AuthGuard],
    data: {title: 'Book Report'}
  },
  {
    path: "reading-setting",
    component: ReadingSettingComponent,
    canActivate: [AuthGuard],
    data: {title: 'Reading Settings'}
  },
  {
    path: "add-new-chapter/:id",
    component: ChapterAddComponent,
    canActivate: [AuthGuard],
    data: {title: 'Add New Chapter'}
  },
  {
    path: "reset-password/:id",
    component: ResetComponent,
    data: {title: 'Rest Password'}
  },
  {
    path: "share-book/:id",
    component: SharebookComponent,
    data: {title: 'Share Book'}
  },
  {
    path: "error-page",
    component: ErrorPageComponent,
    data: {title: 'Error Page'}
  },
  {
    path: "manage-subscription",
    component: AdminSubscriptionComponent,
    canActivate: [AuthGuard],
    data: {title: 'Manage Subscription'}
  },
  {
    path: "add-subscription",
    component: AdminAddSubscriptionComponent,
    canActivate: [AuthGuard],
    data: {title: 'Add Subscription'}
  },
  {
    path: "edit-subscription/:id",
    component: AdminEditSubscriptionComponent,
    canActivate: [AuthGuard],
    data: {title: 'Edit Subscription'}
  },
  {
    path: "view-subscription/:id",
    component: AdminViewSubscriptionComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Subcsription'}
  },
  {
    path: "view-book-report/:id",
    component: ViewBookReportComponent,
    canActivate: [AuthGuard],
    data: {title: 'View Book Report'}
  },
  {
    path: "invite-friends",
    component: InviteFriendComponent,
    // canActivate: [AuthGuard],
    data: {title: 'Invite Friend'}
  },
  {
    path: "payment/:plan_id",
    component: PaymentComponent,
    canActivate: [AuthGuard],
    data: {title: 'Payment'}
  },
  { path: "**", redirectTo: "/" },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'enabled',
  })
],
  exports: [RouterModule]
})
export class AppRoutingModule {}
