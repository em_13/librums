import { Component } from "@angular/core";
import { AuthenticationService } from "./_services/authentication.service";
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "librums";
  header = true;
  backendHeader = false;
  backendFooter = false;
  sidebar = false;
  homeFooter = true;
  webFooter = false;
  showLoader = false;
  previousUrl: string = null;
    currentUrl: string = null;


  constructor(private authenticationService: AuthenticationService, private titleService: Title, private router: Router,  private activatedRoute: ActivatedRoute, ) {
    // redirect to home if already logged in
    // console.log(localStorage.getItem("role"));
    if (this.authenticationService.currentUserValue) {
      if (localStorage.getItem("role") === "admin") {
        this.sidebar = true;
        this.header = false;
        this.backendHeader = true;
        this.backendFooter = true;
        this.homeFooter = false;
      } else {
        this.header = false;
        this.backendHeader = false;
        this.backendFooter = false;
        this.sidebar = false;
        this.homeFooter = false;
        this.webFooter = true;
      }
    }
  }

  setDocTitle(title: string) {
    console.log('current title:::::' + this.titleService.getTitle());
    this.titleService.setTitle(title);
 }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.showLoader = true;
    document.body.scrollTop = 0;
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      if (localStorage.getItem("role") === "admin") {
        this.sidebar = true;
      } else {
        this.header = true;
        this.sidebar = false;
      }
    }

    setTimeout(()=>{ 
      this.showLoader = false;
    }, 3000);

    const appTitle = this.titleService.getTitle();
    this.router
      .events.pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          const child = this.activatedRoute.firstChild;
          if (child.snapshot.data['title']) {
            return child.snapshot.data['title'];
          }
          return appTitle;
        })
      ).subscribe((ttl: string) => {
        this.titleService.setTitle(ttl);
      });
    

/**Url */
this.router.events.pipe(
  filter((event) => event instanceof NavigationEnd)
).subscribe((event: NavigationEnd) => {
  console.log(this.currentUrl);
  console.log(event.url);
  localStorage.setItem("previousUrl", localStorage.getItem("currentUrl"));
  localStorage.setItem("currentUrl", event.url);
            
 this.previousUrl = this.currentUrl;
 this.currentUrl = event.url;
});
/**Url */

  }

  
}
