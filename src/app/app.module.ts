import { BaseUrl } from "./base-url";
import { BrowserModule, Title } from "@angular/platform-browser";
import { OwlModule } from "ngx-owl-carousel";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { ScrollToModule } from "@nicky-lenaers/ngx-scroll-to";
import { RichTextEditorAllModule } from "@syncfusion/ej2-angular-richtexteditor";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./frontend/layouts/header/header.component";
import { FooterComponent } from "./frontend/layouts/footer/footer.component";
import { RegisterComponent } from "./frontend/pages/register/register.component";
import { LoginsComponent } from "./frontend/pages/logins/logins.component";
import { HomeComponent } from "./frontend/pages/home/home.component";
import { AboutComponent } from "./frontend/pages/about/about.component";
import { TermsComponent } from "./frontend/pages/terms/terms.component";
import { PrivacyComponent } from "./frontend/pages/privacy/privacy.component";
import { HomeFooterComponent } from "./frontend/layouts/home-footer/home-footer.component";
import { HomeHeaderComponent } from "./frontend/layouts/home-header/home-header.component";
import { ExploreComponent } from "./frontend/pages/explore/explore.component";
import { ReportComponent } from "./frontend/pages/report/report.component";
import { BookshelfComponent } from "./frontend/pages/bookshelf/bookshelf.component";
import { NotificationsComponent } from "./frontend/pages/notifications/notifications.component";
import { LogoutComponent } from "./frontend/pages/logout/logout.component";
import { FeedbackComponent } from "./frontend/pages/account/feedback/feedback.component";
import { HelpComponent } from "./frontend/pages/help/help.component";
import { FollowersComponent } from "./frontend/pages/followers/followers.component";
import { FollowingComponent } from "./frontend/pages/following/following.component";
import { AuthorProfileComponent } from "./frontend/pages/author-profile/author-profile.component";
import { ChangepasswordComponent } from "./frontend/pages/account/changepassword/changepassword.component";
import { DashboardComponent } from "./backend/pages/dashboard/dashboard.component";
import { SidebarComponent } from "./backend/layouts/sidebar/sidebar.component";
import { BackendHeaderComponent } from "./backend/layouts/backend-header/backend-header.component";
import { BackendFooterComponent } from "./backend/layouts/backend-footer/backend-footer.component";
import { ManageContentComponent } from "./backend/pages/manage-content/manage-content.component";
import { PaymentsDashboardComponent } from "./backend/pages/payments-dashboard/payments-dashboard.component";
import { MessageBoardComponent } from "./backend/pages/message-board/message-board.component";
import { AdvertisementsComponent } from "./backend/pages/advertisements/advertisements.component";
import { BackendNotificationsComponent } from "./backend/pages/backend-notifications/backend-notifications.component";
import { BackendFeedbackComponent } from "./backend/pages/backend-feedback/backend-feedback.component";
import { UsersComponent } from "./backend/pages/users/users.component";
import { EditUsersComponent } from "./backend/pages/edit-users/edit-users.component";
import { ViewUsersComponent } from "./backend/pages/view-users/view-users.component";
import { DataTablesModule } from "angular-datatables";
import { CommonModule, DatePipe } from "@angular/common";
import { ProfileComponent } from "./backend/pages/profile/profile.component";
import { FrontProfileComponent } from "./frontend/pages/profile/profile.component";
import { ChangePasswordComponent } from "./backend/pages/change-password/change-password.component";
import { EditManageContentComponent } from "./backend/pages/edit-manage-content/edit-manage-content.component";
import { ViewManageContentComponent } from "./backend/pages/view-manage-content/view-manage-content.component";
import { BookDetailComponent } from "./frontend/pages/book-detail/book-detail.component";
import { HomeDetailComponent } from "./frontend/pages/home-detail/home-detail.component";
import { ViewChapterContentComponent } from "./backend/pages/view-chapter-content/view-chapter-content.component";
import { EditChapterContentComponent } from "./backend/pages/edit-chapter-content/edit-chapter-content.component";
import { ViewUserDetaliComponent } from "./backend/pages/view-user-detali/view-user-detali.component";
import { SubscriptionComponent } from "./frontend/pages/subscription/subscription.component";
import { ManagePagesComponent } from "./backend/pages/manage-pages/manage-pages.component";
import { EditManagePageComponent } from "./backend/pages/edit-manage-page/edit-manage-page.component";
import { ViewManagePageComponent } from "./backend/pages/view-manage-page/view-manage-page.component";
import { MystoriesComponent } from "./frontend/pages/mystories/mystories.component";
import { MybookdetailsComponent } from "./frontend/pages/mybookdetails/mybookdetails.component";
import { EditProfileComponent } from "./frontend/pages/account/edit-profile/edit-profile.component";
import { AddStoriesComponent } from "./frontend/pages/add-stories/add-stories.component";
import { GetChapterComponent } from "./frontend/pages/book-detail/get-chapter/get-chapter.component";

import { ManageGenreComponent } from "./backend/pages/manage-genre/manage-genre.component";
import { ManageContentTypeComponent } from "./backend/pages/manage-content-type/manage-content-type.component";
import { ManageCopyrightComponent } from "./backend/pages/manage-copyright/manage-copyright.component";
import { EditManageContentTypeComponent } from "./backend/pages/edit-manage-content-type/edit-manage-content-type.component";
import { EditManageCopyrightComponent } from "./backend/pages/edit-manage-copyright/edit-manage-copyright.component";
import { EditManageGenreComponent } from "./backend/pages/edit-manage-genre/edit-manage-genre.component";
import { AddManageContentTypeComponent } from "./backend/pages/add-manage-content-type/add-manage-content-type.component";
import { AddManageCopyrightComponent } from "./backend/pages/add-manage-copyright/add-manage-copyright.component";
import { AddManageGenreComponent } from "./backend/pages/add-manage-genre/add-manage-genre.component";
import { ReadChapterComponent } from "./frontend/pages/read-chapter/read-chapter.component";
import { PostMessageComponent } from "./frontend/pages/post-message/post-message.component";
import { SearchComponent } from "./frontend/pages/search/search.component";
import { BookReportComponent } from "./backend/pages/book-report/book-report.component";
import { SafeHtmlPipe } from "./safe-html.pipe";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { ChapterAddComponent } from "./frontend/pages/chapter-add/chapter-add.component";
import { TagInputModule } from "ngx-chips";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ResetComponent } from "./frontend/pages/reset/reset.component";
import { SharebookComponent } from "./frontend/pages/sharebook/sharebook.component";
import { ViewMessageBoardComponent } from "./backend/pages/view-message-board/view-message-board.component";
import { ViewFeedbackComponent } from "./backend/pages/view-feedback/view-feedback.component";
import { ErrorPageComponent } from "./frontend/pages/error-page/error-page.component";
import { AdminSubscriptionComponent } from "./backend/pages/admin-subscription/admin-subscription.component";
import { AdminAddSubscriptionComponent } from "./backend/pages/admin-add-subscription/admin-add-subscription.component";
import { AdminEditSubscriptionComponent } from "./backend/pages/admin-edit-subscription/admin-edit-subscription.component";
import { AdminViewSubscriptionComponent } from "./backend/pages/admin-view-subscription/admin-view-subscription.component";
import { ViewBookReportComponent } from "./backend/pages/view-book-report/view-book-report.component";
import { InviteFriendComponent } from "./frontend/pages/invite-friend/invite-friend.component";
import { PaymentComponent } from "./frontend/pages/payment/payment.component";
import { NgxPayPalModule } from "ngx-paypal";
import { NgxSpinnersModule } from "ngx-spinners";
import { SearchBoxComponent } from "./frontend/pages/chapter-add/search-box/search-box.component";
import { SearchResultComponent } from "./frontend/pages/chapter-add/search-result/search-result.component";
import { EditChapterComponent } from './frontend/pages/edit-chapter/edit-chapter.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login'; 
import { ReadingSettingComponent } from './frontend/pages/reading-setting/reading-setting.component';
import { MyDatePickerModule } from 'mydatepicker';
import { MyWallComponent } from './frontend/pages/my-wall/my-wall.component';
import { ConfirmationDialogComponent } from './frontend/pages/confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './frontend/pages/confirmation-dialog/confirmation-dialog.service';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
import { EditstoriesComponent } from './frontend/pages/editstories/editstories.component';
import { ForgetpasswordComponent } from './frontend/pages/forgetpassword/forgetpassword.component';
import { SingleBookComponent } from './frontend/pages/single-book/single-book.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ShareSingleBookComponent } from './frontend/pages/share-single-book/share-single-book.component';
import { MediumEditorModule } from 'angular2-medium-editor';
import { WithdrawalComponent } from './frontend/pages/withdrawal/withdrawal.component'
import { AdsenseModule } from 'ng2-adsense';
import { ChapterPaymentsComponent } from './backend/pages/chapter-payments/chapter-payments.component';
import { PreviewChapterComponent } from './frontend/pages/preview-chapter/preview-chapter.component';
import { AnnoucementComponent } from './backend/pages/annoucement/annoucement.component';
import { ToastrModule } from 'ngx-toastr';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';
import { FrontendAnnoucementComponent } from './frontend/pages/frontend-annoucement/frontend-annoucement.component';
import { AuthorStoriesComponent } from './frontend/pages/author-stories/author-stories.component';
import { SinglebookComponent } from './frontend/pages/singlebook/singlebook.component';
import { ViewchapterComponent } from './backend/pages/viewchapter/viewchapter.component';
import { ManageNotificationComponent } from './backend/pages/manage-notification/manage-notification.component';
import { VerifyemailComponent } from './frontend/pages/verifyemail/verifyemail.component';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { PublishedBooksComponent } from './backend/pages/published-books/published-books.component';
import { PublishedBookViewComponent } from './backend/pages/published-books/published-book-view/published-book-view.component';
import { PublishedBookContentComponent } from './backend/pages/published-books/published-book-content/published-book-content.component';
import { AuthorListComponent } from './backend/pages/author-list/author-list.component';
import { EditAuthorComponent } from './backend/pages/author-list/edit-author/edit-author.component';
import { ContactUsComponent } from './backend/pages/contact-us/contact-us.component';
import { AllBooksComponent } from './frontend/pages/all-books/all-books.component';
import { ManageSliderComponent } from './backend/pages/manage-slider/manage-slider.component';
import { AddSliderComponent } from './backend/pages/add-slider/add-slider.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { ColorSketchModule } from 'ngx-color/sketch';
import { ViewAllComponent } from './frontend/pages/view-all/view-all.component';
export function socialConfigs() {  
  const config = new AuthServiceConfig(  
    [  
      {  
        id: FacebookLoginProvider.PROVIDER_ID,  
        provider: new FacebookLoginProvider("2460638890823149") 
      },  
      {  
        id: GoogleLoginProvider.PROVIDER_ID,  
        provider: new GoogleLoginProvider("1092865189897-vtteetbesgihr8cb1l3fcmu6vopjhkps.apps.googleusercontent.com")
      }  
    ]  
  );  
  return config;  
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegisterComponent,
    LoginsComponent,
    HomeComponent,
    AboutComponent,
    TermsComponent,
    PrivacyComponent,
    HomeFooterComponent,
    HomeDetailComponent,
    HomeHeaderComponent,
    ExploreComponent,
    ReportComponent,
    BookshelfComponent,
    NotificationsComponent,
    ViewAllComponent,
    LogoutComponent,
    FeedbackComponent,
    HelpComponent,
    DashboardComponent,
    SidebarComponent,
    ViewchapterComponent,
    BackendHeaderComponent,
    BackendFooterComponent,
    ManageContentComponent,
    FrontProfileComponent,
    PaymentsDashboardComponent,
    MessageBoardComponent,
    AdvertisementsComponent,
    BackendNotificationsComponent,
    BackendFeedbackComponent,
    UsersComponent,
    EditUsersComponent,
    ViewUsersComponent,
    ChangePasswordComponent,
    EditManageContentComponent,
    FollowersComponent,
    FollowingComponent,
    AuthorProfileComponent,
    ProfileComponent,
    BookDetailComponent,
    ChangepasswordComponent,
    HomeDetailComponent,
    ContactUsComponent,
    ViewManageContentComponent,
    ViewChapterContentComponent,
    EditChapterContentComponent,
    ViewUserDetaliComponent,
    SubscriptionComponent,
    ManagePagesComponent,
    EditManagePageComponent,
    ViewManagePageComponent,
    MystoriesComponent,
    MybookdetailsComponent,
    EditProfileComponent,
    AddStoriesComponent,
    GetChapterComponent,
    ManageGenreComponent,
    ManageContentTypeComponent,
    ManageCopyrightComponent,
    EditManageContentTypeComponent,
    EditManageCopyrightComponent,
    EditManageGenreComponent,
    AddManageContentTypeComponent,
    AddManageCopyrightComponent,
    AddManageGenreComponent,
    ReadChapterComponent,
    PostMessageComponent,
    SearchComponent,
    BookReportComponent,
    AllBooksComponent,
    ManageSliderComponent,
    AddSliderComponent,
    SafeHtmlPipe,
    ChapterAddComponent,
    ResetComponent,
    SharebookComponent,
    ErrorPageComponent,
    ViewMessageBoardComponent,
    ViewFeedbackComponent,
    AdminSubscriptionComponent,
    AdminAddSubscriptionComponent,
    AdminEditSubscriptionComponent,
    AdminViewSubscriptionComponent,
    ViewBookReportComponent,
    InviteFriendComponent,
    PaymentComponent,
    SearchBoxComponent,
    SearchResultComponent,
    EditChapterComponent,
    ReadingSettingComponent,
    MyWallComponent,
    ConfirmationDialogComponent,
    ManageNotificationComponent,
    EditstoriesComponent,
    ForgetpasswordComponent,
    SingleBookComponent,
    ShareSingleBookComponent,
    WithdrawalComponent,
    ChapterPaymentsComponent,
    PreviewChapterComponent,
    AnnoucementComponent,
    ToggleButtonComponent,
    FrontendAnnoucementComponent,
    AuthorStoriesComponent,
    SinglebookComponent,
    ViewchapterComponent,
    ManageNotificationComponent,
    VerifyemailComponent,
    PublishedBooksComponent,
    PublishedBookViewComponent,
    PublishedBookContentComponent,
    AuthorListComponent,
    EditAuthorComponent,
    ContactUsComponent,
    AllBooksComponent,
    ManageSliderComponent,
    AddSliderComponent,
    ViewAllComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxTrimDirectiveModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    ColorSketchModule,
    HttpClientModule,
    DataTablesModule,
    CommonModule,
    OwlModule,
    ColorPickerModule,
    RichTextEditorAllModule,
    ScrollToModule.forRoot(),
    RichTextEditorAllModule,
    AngularFontAwesomeModule,
    TagInputModule,
    BrowserAnimationsModule,
    FormsModule,
    NgbModule,
    NgxPayPalModule,
    NgxSpinnersModule,
    SocialLoginModule,
    MyDatePickerModule,
    AdsenseModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right'
    })
  ],
  providers: [
    AuthService,  
    {  
      provide: AuthServiceConfig,  
      useFactory: socialConfigs  
    },
    ConfirmationDialogService,
    DatePipe,
    Title
  ],
  bootstrap: [AppComponent],
  entryComponents: [ ConfirmationDialogComponent ],
})
export class AppModule {}
