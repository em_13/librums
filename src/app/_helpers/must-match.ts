// tslint:disable-next-line: quotemark
import { FormGroup } from "@angular/forms";

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export function WhiteSpace(controlName: string) {
  console.log('asdasda');
  return (formGroup: FormGroup) => {
    console.log('000');
  const control = formGroup.controls[controlName];
  if (control.errors && !control.errors.whiteSpace) {
    // return if another validator has already found an error on the matchingControl
    return;
  }
  if (control.value.startsWith(' ')) {
    // return {
    //   'trimError': { value: 'control has leading whitespace' }
    // };
    console.log('111');
    control.setErrors({ whiteSpace: true });
    return;
  }
  console.log('2222');
  if (control.value.endsWith(' ')) {
    console.log('333');
    control.setErrors({ whiteSpace: true });
    return;
  }
  console.log('44444');
  control.setErrors(null);
  return;
};
}
