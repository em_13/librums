import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticationService } from "../_services/authentication.service";
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  userdata: any;
  is_subscribed = false;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private toaster: ToastrService
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    if(this.userdata){
      this.authenticationService.checkUserStatus().subscribe(
        (res: any) => {
          if(res.status){
            if(res.is_subscribed){
              this.is_subscribed = res.is_subscribed;
            }
            if(!this.userdata.data.is_completed){
              if(res.data.role != 1){
                this.router.navigate(["/myprofile/editprofile"]);
                return;
              }
            }
          }else{
            localStorage.removeItem("currentUser");
            // not logged in so redirect to login page with the return url
            this.router.navigate(["/login"], { queryParams: { returnUrl: state.url } });
            return false;
          }
        },
        error => {
          if(error.status == 405){
          $(".loader").hide()
          // $("#accountblock").modal('show');
          this.toaster.error('Your account has been temporarily suspended. Kindlly contact the support team.');
          setTimeout(function(){ 
           localStorage.removeItem("currentUser");
            // not logged in so redirect to login page with the return url
            window.location.href = "/";
            return false;
           }, 4000);
          }
        }
      );
    }

    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser) {
      if (
        route.data.roles &&
        route.data.roles.indexOf(currentUser) === "admin"
      ) {
        // role not authorised so redirect to home page
        this.router.navigate(["/dashboard"]);
        return false;
      }
      // authorised so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(["/login"], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
