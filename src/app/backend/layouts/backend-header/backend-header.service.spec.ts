import { TestBed } from '@angular/core/testing';

import { BackendHeaderService } from './backend-header.service';

describe('BackendHeaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendHeaderService = TestBed.get(BackendHeaderService);
    expect(service).toBeTruthy();
  });
});
