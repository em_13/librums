import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-backend-header",
  templateUrl: "./backend-header.component.html",
  styleUrls: ["./backend-header.component.css"]
})
export class BackendHeaderComponent implements OnInit {
  fullname: any;
  admin_id: any;
  showComponent: boolean;
  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    const userdata = JSON.parse(localStorage.getItem("currentUser"));
    this.fullname = userdata.data.fullname;
    this.admin_id = userdata.data._id;
  }
  // public refresh() {
  //   this.showComponent = false;
  //   setTimeout(x => (this.showComponent = true));
  // }
}
