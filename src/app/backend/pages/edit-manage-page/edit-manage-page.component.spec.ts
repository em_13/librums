import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManagePageComponent } from './edit-manage-page.component';

describe('EditManagePageComponent', () => {
  let component: EditManagePageComponent;
  let fixture: ComponentFixture<EditManagePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditManagePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManagePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
