import { TestBed } from '@angular/core/testing';

import { EditManagePageService } from './edit-manage-page.service';

describe('EditManagePageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditManagePageService = TestBed.get(EditManagePageService);
    expect(service).toBeTruthy();
  });
});
