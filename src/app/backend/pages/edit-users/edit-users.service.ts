import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class EditUsersService {
  usersdata: any;
  usersimage: any;
  updateuser: any;
  baseimage: any;
  base: any;
  constructor(private http: HttpClient) {
    this.baseimage = BaseUrl.imageApi;
    this.base = BaseUrl.admin;
  }

  getUserData(id) {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(
      this.base + `getUserData`,
      { user_id: id },
      {
        headers
      }
    );
  }

  user_image(image) {
    this.usersimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": this.usersimage.data.token
    });
    return this.http.post(this.baseimage + `user_image`, image, {
      headers
    });
  }

  cover_image(image) {
    this.usersimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": this.usersimage.data.token
    });
    return this.http.post(this.baseimage + `cover_image`, image, {
      headers
    });
  }

  updateUser(data) {
    this.updateuser = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.updateuser.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `updateUser`, data, {
      headers
    });
  }
}
