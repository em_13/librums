import { TestBed } from '@angular/core/testing';

import { ManageGenreService } from './manage-genre.service';

describe('ManageGenreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageGenreService = TestBed.get(ManageGenreService);
    expect(service).toBeTruthy();
  });
});
