import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ManageGenreService {
  genredata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getGenres() {
    this.genredata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.genredata.data.token
    });
    return this.http.post(this.base + `getGenres`, null, {
      headers
    });
  }
  updateStatus(data) {
    this.genredata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.genredata.data.token
    });
    return this.http.post(this.base + `updateGenreStatus`, data, {
      headers
    });
  }
}
