import { TestBed } from '@angular/core/testing';

import { EditChapterContentService } from './edit-chapter-content.service';

describe('EditChapterContentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditChapterContentService = TestBed.get(EditChapterContentService);
    expect(service).toBeTruthy();
  });
});
