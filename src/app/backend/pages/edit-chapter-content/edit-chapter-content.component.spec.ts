import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChapterContentComponent } from './edit-chapter-content.component';

describe('EditChapterContentComponent', () => {
  let component: EditChapterContentComponent;
  let fixture: ComponentFixture<EditChapterContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChapterContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChapterContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
