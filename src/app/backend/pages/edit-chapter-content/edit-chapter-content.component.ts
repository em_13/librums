import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { ViewChapterContentService } from "../view-chapter-content/view-chapter-content.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
@Component({
  selector: "app-edit-chapter-content",
  templateUrl: "./edit-chapter-content.component.html",
  styleUrls: ["./edit-chapter-content.component.css"]
})
export class EditChapterContentComponent implements OnInit {
  // tslint:disable-next-line: ban-types
  getChapter: any;
  data: any;
  // tslint:disable-next-line: ban-types
  temp: Object = false;
  loading = false;
  submitted = false;
  showMsg = false;
  showImg = true;
  updateForm: any;
  imgURL: any;
  public message: string;
  // tslint:disable-next-line: variable-name
  chapter_id: string;
  reset: any;
  errorMsg: any;
  alertService: any;
  imagePath: any;
  usersimage: any;
  selectedFiles: any;
  currentFileUpload: any;
  baseimage: any;
  constructor(
    private chapterservice: ViewChapterContentService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.chapter_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadBook();
    this.updateForm = this.formBuilder.group({
      chapter_id: [""],
      book_id: [""],
      is_published: [""],
      chapter_name: ["", Validators.required],
      content: ["", Validators.required],
      is_paid: ["", Validators.required],
      amount: ["", Validators.required],
      image: [""],
      video: [""]
    });
  }
  get f() {
    return this.updateForm.controls;
  }
  loadBook() {
    // this.manageservice.getBookDetail("5def94f7ed54f659fae740fe").subscribe(
    this.chapterservice.getChapterDetail(this.chapter_id).subscribe(
      (res: any) => {
        // console.log(res.data);
        this.getChapter = res.data;
        this.temp = true;
      },
      () => {
        console.log("ERROR");
      }
    );
  }

  onSelectCoverImage(event) {
    if (event.target.files.length > 0) {
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.chapterservice.chapter_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              this.updateForm.controls["image"].setValue(res.data);
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }
  onSubmit() {
    console.log(this.updateForm.value);
    this.submitted = true;
    if (this.updateForm.invalid) {
      console.log("here");
      return;
    }
    this.loading = true;
    this.submitted = true;
    this.chapterservice.updateChapter(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          // alert("Edit Chapter Successfully");
          this.router.navigate(["/manage-books"]);
          this.showMsg = res.message;
        } else {
          this.errorMsg = res.message;
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
