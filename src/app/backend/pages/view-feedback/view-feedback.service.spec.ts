import { TestBed } from '@angular/core/testing';

import { ViewFeedbackService } from './view-feedback.service';

describe('ViewFeedbackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewFeedbackService = TestBed.get(ViewFeedbackService);
    expect(service).toBeTruthy();
  });
});
