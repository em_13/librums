import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "./../../../base-url";
@Injectable({
  providedIn: "root"
})
export class ViewFeedbackService {
  viewfeedback: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getFeedbackDetail(id) {
    this.viewfeedback = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.viewfeedback.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getFeedbackDetail`,
      { feedback_id: id },
      {
        headers
      }
    );
  }
}
