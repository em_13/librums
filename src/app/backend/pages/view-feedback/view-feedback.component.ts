import { Component, OnInit } from "@angular/core";
import { ViewFeedbackService } from "./view-feedback.service";
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-view-feedback",
  templateUrl: "./view-feedback.component.html",
  styleUrls: ["./view-feedback.component.css"]
})
export class ViewFeedbackComponent implements OnInit {
  managePages: any;
  temp: boolean;
  getPages: any;
  // tslint:disable-next-line: variable-name
  feedback_id: string;
  showLoader = false
  constructor(
    private manage: ViewFeedbackService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.showLoader = true;
    this.feedback_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadPages();
  }
  loadPages() {
    this.manage.getFeedbackDetail(this.feedback_id).subscribe(
      (res: any) => {
        this.getPages = res.data;
        this.showLoader = false;
      },
      () => {
        console.log("ERROR");
      }
    );
  }
}
