import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class ViewchapterService {

  base: any;
  baseapi: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
    this.baseapi = BaseUrl.imageApi;
  }
  contentdata: any;
  usersimage: any;


  getChapterDetail(id) { 
    this.contentdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.contentdata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getChapterDetail`,
      { chapter_id: id },
      {
        headers
      }
    );
  }
}
