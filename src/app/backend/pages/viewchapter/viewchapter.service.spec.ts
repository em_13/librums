import { TestBed } from '@angular/core/testing';

import { ViewchapterService } from './viewchapter.service';

describe('ViewchapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewchapterService = TestBed.get(ViewchapterService);
    expect(service).toBeTruthy();
  });
});
