import { Component, OnInit } from "@angular/core";
import { EditManageGenreService } from "./edit-manage-genre.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { WhiteSpace } from 'src/app/_helpers/must-match'
@Component({
  selector: "app-edit-manage-genre",
  templateUrl: "./edit-manage-genre.component.html",
  styleUrls: ["./edit-manage-genre.component.css"]
})
export class EditManageGenreComponent implements OnInit {
  getGenre: any;
  temp: boolean;
  updateForm: any;
  genreid: any;
  loading = false;
  submitted = false;
  showMsg = false;
  showImg = true;
  errorMsg: any;
  alertService: any;
  constructor(
    private editgenre: EditManageGenreService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.genreid = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadGenre();
    this.updateForm = this.formBuilder.group({
      genre_id: [""],
      name: ["", Validators.required]
    });
  }

  loadGenre() {
    // this.manageservice.getBookDetail("5def94f7ed54f659fae740fe").subscribe(
    this.editgenre.getGenreDetail(this.genreid).subscribe(
      (res: any) => {
        // console.log(res.data);
        this.getGenre = res.data;
        this.temp = true;
      },
      () => {
        console.log("ERROR");
      }
    );
  }

  get f() {
    return this.updateForm.controls;
  }
  public cleanForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((key) => formGroup.get(key).setValue(formGroup.get(key).value.trim()));
  }
  onSubmit() {
    // console.log(this.updateForm.value);
    this.cleanForm(this.updateForm);
    // console.log(this.updateForm.value);
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.loading = true;
    this.submitted = true;
    this.editgenre.updateGenre(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success('Genre updated successfully.')
          this.router.navigate(["/manage-genre"]);
        } else {
          this.toaster.error(res.message)
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
