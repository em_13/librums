import { TestBed } from '@angular/core/testing';

import { EditManageGenreService } from './edit-manage-genre.service';

describe('EditManageGenreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditManageGenreService = TestBed.get(EditManageGenreService);
    expect(service).toBeTruthy();
  });
});
