import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManageGenreComponent } from './edit-manage-genre.component';

describe('EditManageGenreComponent', () => {
  let component: EditManageGenreComponent;
  let fixture: ComponentFixture<EditManageGenreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditManageGenreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManageGenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
