import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class EditManageGenreService {
  editgenre: any;
  updategenre: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getGenreDetail(id) {
    this.editgenre = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.editgenre.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getGenreDetail`,
      { genre_id: id },
      {
        headers
      }
    );
  }

  updateGenre(data) {
    this.updategenre = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.updategenre.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `updateGenre`, data, {
      headers
    });
  }
}
