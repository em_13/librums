import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { ViewChapterContentService } from "./view-chapter-content.service";
import { Router, RouterStateSnapshot, ActivatedRoute } from "@angular/router";
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: "app-view-chapter-content",
  templateUrl: "./view-chapter-content.component.html",
  styleUrls: ["./view-chapter-content.component.css"]
})
export class ViewChapterContentComponent implements OnInit {
  getChapter: any;
  data: any;
  chapter_id: string;
  baseimage: any;
  safeUrl: any;
  showLoader = false;

  constructor(
    private chapterservice: ViewChapterContentService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer  
  ) {
    this.baseimage = BaseUrl.image;
    this.sanitizer = sanitizer;
  }

  ngOnInit() {
    this.showLoader = true;
    this.chapter_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadChapter();
  }

  getTrustedUrl(url:any){ 
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    // console.log(this.safeUrl);
   }

  loadChapter() {
    this.chapterservice.getChapterDetail(this.chapter_id).subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        this.getChapter = res.data;
        this.getTrustedUrl('https://www.youtube.com/embed/'+this.getChapter.video);
        console.log(res.data);
        this.showLoader = false;
      },
      () => {
        console.log("ERROR");
      }
    );
  }
}
