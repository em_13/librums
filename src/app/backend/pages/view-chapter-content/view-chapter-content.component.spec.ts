import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChapterContentComponent } from './view-chapter-content.component';

describe('ViewChapterContentComponent', () => {
  let component: ViewChapterContentComponent;
  let fixture: ComponentFixture<ViewChapterContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChapterContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChapterContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
