import { TestBed } from '@angular/core/testing';

import { ViewChapterContentService } from './view-chapter-content.service';

describe('ViewChapterContentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewChapterContentService = TestBed.get(ViewChapterContentService);
    expect(service).toBeTruthy();
  });
});
