import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewSubscriptionComponent } from './admin-view-subscription.component';

describe('AdminViewSubscriptionComponent', () => {
  let component: AdminViewSubscriptionComponent;
  let fixture: ComponentFixture<AdminViewSubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewSubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
