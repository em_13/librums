import { TestBed } from '@angular/core/testing';

import { AdminViewSubscriptionService } from './admin-view-subscription.service';

describe('AdminViewSubscriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminViewSubscriptionService = TestBed.get(AdminViewSubscriptionService);
    expect(service).toBeTruthy();
  });
});
