import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ViewManagePageService {
  viewpages: any;
  datapage: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getPageDetail(id) {
    this.viewpages = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.viewpages.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getPageDetail`,
      { page_id: id },
      {
        headers
      }
    );
  }

  updatePageDetail(data) {
    this.datapage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.datapage.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `updatePageDetail`, data, {
      headers
    });
  }
}
