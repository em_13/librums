import { TestBed } from '@angular/core/testing';

import { ViewManagePageService } from './view-manage-page.service';

describe('ViewManagePageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewManagePageService = TestBed.get(ViewManagePageService);
    expect(service).toBeTruthy();
  });
});
