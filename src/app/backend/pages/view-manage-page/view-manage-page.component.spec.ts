import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewManagePageComponent } from './view-manage-page.component';

describe('ViewManagePageComponent', () => {
  let component: ViewManagePageComponent;
  let fixture: ComponentFixture<ViewManagePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewManagePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewManagePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
