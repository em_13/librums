import { Injectable } from "@angular/core";
import { BaseUrl } from "./../../../base-url";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class AdminAddSubscriptionService {
  addsub: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  addSubscription(data) {
    this.addsub = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.addsub.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `addSubscription`, data, {
      headers
    });
  }
}
