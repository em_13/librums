import { TestBed } from '@angular/core/testing';

import { AdminAddSubscriptionService } from './admin-add-subscription.service';

describe('AdminAddSubscriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminAddSubscriptionService = TestBed.get(AdminAddSubscriptionService);
    expect(service).toBeTruthy();
  });
});
