import { TestBed } from '@angular/core/testing';

import { BackendNotificationsService } from './backend-notifications.service';

describe('BackendNotificationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendNotificationsService = TestBed.get(BackendNotificationsService);
    expect(service).toBeTruthy();
  });
});
