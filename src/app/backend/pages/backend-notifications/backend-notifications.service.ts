import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class BackendNotificationsService {

  userdata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.frontend;
  }

  adminNotification(data) {
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.userdata.data.token
    });
    return this.http.post(this.base + `/adminNotification`, data, {
      headers
    });
  }
}
