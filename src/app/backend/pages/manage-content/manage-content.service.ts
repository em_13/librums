import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ManageContentService {
  managedata: any;
  base: any;
  bookdata: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  manageContent() {
    this.managedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.managedata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `manageContent`, null, {
      headers
    });
  }

  updateBookStatus(data) {
    this.bookdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.bookdata.data.token
    });
    return this.http.post(this.base + `updateBookStatus`, data, {
      headers
    });
  }
}
