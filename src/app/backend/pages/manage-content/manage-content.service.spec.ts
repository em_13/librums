import { TestBed } from '@angular/core/testing';

import { ManageContentService } from './manage-content.service';

describe('ManageContentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageContentService = TestBed.get(ManageContentService);
    expect(service).toBeTruthy();
  });
});
