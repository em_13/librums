import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ProfileService {
  admindata: any;
  base: any;
  usersdata: any;
  admin_id: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getUserData(id) {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(
      this.base + `getUserData`,
      { user_id: id },
      {
        headers
      }
    );
  }
  // userdata.data._id
  updateAdmin(data) {
    this.admindata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.admindata.data.token
    });
    return this.http.post(this.base + `updateAdmin`, data, {
      headers
    });
  }
}
