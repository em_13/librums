import { Component, OnInit } from "@angular/core";
import { ProfileService } from "./profile.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { AlertService } from "src/app/_services/alert.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"]
})
export class ProfileComponent implements OnInit {
  fullname: any;
  email: any;
  adminForm: any;
  loading = false;
  submitted = false;
  showMsg = false;
  errorMsg = "";
  admin_id: any;
  usersUpdate: any;
  showLoader = false
  constructor(
    private changeadmin: ProfileService,
    private formbuilder: FormBuilder,
    private alertService: AlertService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.showLoader = true;
    this.admin_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.getUserData();
    // const userdata = JSON.parse(localStorage.getItem("currentUser"));
    // this.fullname = userdata.data.fullname;
    // this.email = userdata.data.email;
    this.adminForm = this.formbuilder.group({
      user_id: this.admin_id,
      fullname: ["", Validators.required],
      email: [""]
    });
  }

  getUserData() {
    this.changeadmin.getUserData(this.admin_id).subscribe(
      (res: any) => {
        if (res.status) {
          this.usersUpdate = res.data;
          this.showLoader = false;
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }

  get f() {
    return this.adminForm.controls;
  }
  public cleanForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((key) => formGroup.get(key).setValue(formGroup.get(key).value.trim()));
  }
  onSubmit() {
    // console.log(this.updateForm.value);
    this.cleanForm(this.adminForm);
    this.submitted = true;
    // stop here if form is invalid
    if (this.adminForm.invalid) {
      return;
    }
    this.loading = true;
    this.changeadmin.updateAdmin(this.adminForm.value).subscribe(
      (res: any) => {
        if (res) {
          console.log(res);
          // this.router.navigate(["/dashboard"]);
          this.fullname = this.adminForm.value.fullname;
          // this.showMsg = res.message;
          this.toaster.success(res.message);
          this.getUserData();
          const userdata = JSON.parse(localStorage.getItem("currentUser"));
          userdata.data.fullname = this.fullname;
          localStorage.setItem("currentUser", JSON.stringify(userdata));
          window.location.reload();
        } else {
          this.toaster.error(res.message);
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
