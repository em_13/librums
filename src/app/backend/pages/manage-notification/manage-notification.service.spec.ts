import { TestBed } from '@angular/core/testing';

import { ManageNotificationService } from './manage-notification.service';

describe('ManageNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageNotificationService = TestBed.get(ManageNotificationService);
    expect(service).toBeTruthy();
  });
});
