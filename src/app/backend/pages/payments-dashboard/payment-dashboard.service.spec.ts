import { TestBed } from '@angular/core/testing';

import { PaymentDashboardService } from './payment-dashboard.service';

describe('PaymentDashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentDashboardService = TestBed.get(PaymentDashboardService);
    expect(service).toBeTruthy();
  });
});
