import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseUrl } from "./../../../base-url";
@Injectable({
  providedIn: 'root'
})
export class PaymentDashboardService {
  messagedata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }
 

  getPayments() {
    this.messagedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.messagedata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `getSubscriptionPayments`, null, {
      headers
    });
  }

  getChapterPayments() {
    this.messagedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.messagedata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `getChapterPayments`, null, {
      headers
    });
  }
}
