import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { ViewManageContentService } from "../view-manage-content/view-manage-content.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: "app-edit-manage-content",
  templateUrl: "./edit-manage-content.component.html",
  styleUrls: ["./edit-manage-content.component.css"]
})
export class EditManageContentComponent implements OnInit {
  getBook: any;
  data: any;
  temp: Object = false;
  bookid: any;
  usersimage: any;
  loading = false;
  submitted = false;
  showMsg = false;
  showImg = true;
  updateForm: any;
  url: any;
  contanttype: any;
  editFile: boolean;
  removeUpload: boolean;
  imageUrl: string | ArrayBuffer;
  public imagePath;
  imgURL: any;
  public message: string;
  errorMsg: any;
  alertService: any;
  maxLength: number;
  selectedFiles: any;
  currentFileUpload: any;
  baseimage: any;
  tags: any;

  constructor(
    private manageservice: ViewManageContentService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private toaster: ToastrService
  ) {
    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.bookid = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadBook();
    this.loadManage();
    this.updateForm = this.formBuilder.group({
      book_id: [""],
      tags: [""],
      is_published: [""],
      not_for_below_eighteen: ["", Validators.required],
      title: ["", Validators.required],
      description: ["", Validators.required],
      content_type: ["", Validators.required],
      genre: ["", Validators.required],
      copyright: ["", Validators.required],
      cover_image: [""]
    });
  }

  loadBook() {
    // this.manageservice.getBookDetail("5def94f7ed54f659fae740fe").subscribe(
    this.manageservice.getBookDetail(this.bookid).subscribe(
      (res: any) => {
        console.log(res.data);
        this.getBook = res.data;
        this.temp = true;
      },
      () => {
        console.log("ERROR");
      }
    );
  }

  onSelectCoverImage(event) {
    if (event.target.files.length > 0) {
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);
      formData.append("image", this.currentFileUpload);
      this.manageservice.book_cover_image(formData).subscribe(
        (res: any) => {
          if (res) {
            this.usersimage = res;
            if (res.status) {
              this.updateForm.controls["cover_image"].setValue(res.data);
            }
          }
        },
        error => {
          console.log("ERROR");
        }
      );
    }
  }

  loadManage() {
    this.manageservice.getContentType().subscribe(
      (res: any) => {
        const mapped = Object.entries(res.data).map(([type, value]) => ({
          type,
          value
        }));
        //console.log(mapped);
        this.contanttype = mapped;
        this.temp = true;
      },
      () => {
        console.log("ERROR");
      }
    );
  }

  get f() {
    return this.updateForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.updateForm.invalid) {
      return;
    }

    this.loading = true;
    this.submitted = true;
    this.manageservice.updateBook(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success("manage content updated successfully.")
          this.router.navigate(["/manage-content"]);
        } else {
          this.toaster.error(res.message);
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
