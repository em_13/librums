import { TestBed } from '@angular/core/testing';

import { EditManageContentService } from './edit-manage-content.service';

describe('EditManageContentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditManageContentService = TestBed.get(EditManageContentService);
    expect(service).toBeTruthy();
  });
});
