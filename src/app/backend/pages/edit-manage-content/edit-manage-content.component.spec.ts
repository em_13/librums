import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManageContentComponent } from './edit-manage-content.component';

describe('EditManageContentComponent', () => {
  let component: EditManageContentComponent;
  let fixture: ComponentFixture<EditManageContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditManageContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManageContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
