import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackendFeedbackComponent } from './backend-feedback.component';

describe('BackendFeedbackComponent', () => {
  let component: BackendFeedbackComponent;
  let fixture: ComponentFixture<BackendFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackendFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackendFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
