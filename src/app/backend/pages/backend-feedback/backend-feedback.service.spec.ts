import { TestBed } from '@angular/core/testing';

import { BackendFeedbackService } from './backend-feedback.service';

describe('BackendFeedbackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendFeedbackService = TestBed.get(BackendFeedbackService);
    expect(service).toBeTruthy();
  });
});
