import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class BackendFeedbackService {
  feedbackdata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getFeedbacks() {
    this.feedbackdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.feedbackdata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `getFeedbacks`, null, {
      headers
    });
  }

  deleteFeedback(feedback_id) {
    this.feedbackdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.feedbackdata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `deleteFeedback`, feedback_id, {
      headers
    });
  }
}
