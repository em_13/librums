import { Component, OnInit } from "@angular/core";
import { BaseUrl } from "./../../../base-url";
import { MessageBoardService } from "../message-board/message-board.service";

@Component({
  selector: "app-book-report",
  templateUrl: "./book-report.component.html",
  styleUrls: ["./book-report.component.css"]
})
export class BookReportComponent implements OnInit {
  public getreports: Object;
  public temp: Object = false;
  data: any;
  delId: any;
  baseimage: any;
  showLoader = false
  constructor(private message: MessageBoardService) {
    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.loadReports();
  }

  loadReports() {
    this.message.getReports().subscribe(
      (res: any) => {
        this.getreports = res.data;
        this.temp = true;
        this.showLoader = false;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  onClick11(event) {
    this.delId = event;
  }
  onClick(event) {
    this.message.deleteReport(event).subscribe(
      (res: any) => {
        this.loadReports();
      },
      error => {
        // tslint:disable-next-line: quotemark
        console.log("ERROR");
      }
    );
  }
}
