import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class EditManageContentTypeService {
  edittype: any;
  updatetype: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getContentTypeDetail(id) {
    this.edittype = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.edittype.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getContentTypeDetail`,
      { contenttype_id: id },
      {
        headers
      }
    );
  }

  updateContentType(data) {
    this.updatetype = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.updatetype.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `updateContentType`, data, {
      headers
    });
  }
}
