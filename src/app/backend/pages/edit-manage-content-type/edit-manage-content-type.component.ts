import { Component, OnInit } from "@angular/core";
import { EditManageContentTypeService } from "./edit-manage-content-type.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { WhiteSpace } from 'src/app/_helpers/must-match'

@Component({
  selector: "app-edit-manage-content-type",
  templateUrl: "./edit-manage-content-type.component.html",
  styleUrls: ["./edit-manage-content-type.component.css"]
})
export class EditManageContentTypeComponent implements OnInit {
  getType: any;
  temp: boolean;
  updateForm: any;
  typeid: any;
  loading = false;
  submitted = false;
  showMsg = false;
  showImg = true;
  errorMsg: any;
  alertService: any;
  constructor(
    private edittype: EditManageContentTypeService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.typeid = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadGenre();
    this.updateForm = this.formBuilder.group({
      contenttype_id: [""],
      name: ["", Validators.required]
    });
  }

  loadGenre() {
    // this.manageservice.getBookDetail("5def94f7ed54f659fae740fe").subscribe(
    this.edittype.getContentTypeDetail(this.typeid).subscribe(
      (res: any) => {
        // console.log(res.data);
        this.getType = res.data;
        this.temp = true;
      },
      () => {
        console.log("ERROR");
      }
    );
  }

  get f() {
    return this.updateForm.controls;
  }
  public cleanForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((key) => formGroup.get(key).setValue(formGroup.get(key).value.trim()));
  }
  onSubmit() {
    // console.log(this.updateForm.value);
    this.cleanForm(this.updateForm);
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.loading = true;
    this.submitted = true;
    this.edittype.updateContentType(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success("Content type updated successfully.")
          this.router.navigate(["/manage-content-type"]);
        } else {
          this.toaster.error(res.message)
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
