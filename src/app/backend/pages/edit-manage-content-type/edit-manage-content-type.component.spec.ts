import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManageContentTypeComponent } from './edit-manage-content-type.component';

describe('EditManageContentTypeComponent', () => {
  let component: EditManageContentTypeComponent;
  let fixture: ComponentFixture<EditManageContentTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditManageContentTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManageContentTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
