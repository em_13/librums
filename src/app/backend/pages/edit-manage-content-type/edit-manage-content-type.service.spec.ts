import { TestBed } from '@angular/core/testing';

import { EditManageContentTypeService } from './edit-manage-content-type.service';

describe('EditManageContentTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditManageContentTypeService = TestBed.get(EditManageContentTypeService);
    expect(service).toBeTruthy();
  });
});
