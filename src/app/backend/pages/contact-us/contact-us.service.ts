import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  managedata: any;
  base: any;
  bookdata: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }
  manageContent() {
    this.managedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.managedata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `getContactUs`, null, {
      headers
    });
  }
}
