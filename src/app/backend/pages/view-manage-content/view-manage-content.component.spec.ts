import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewManageContentComponent } from './view-manage-content.component';

describe('ViewManageContentComponent', () => {
  let component: ViewManageContentComponent;
  let fixture: ComponentFixture<ViewManageContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewManageContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewManageContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
