import { TestBed } from '@angular/core/testing';

import { ViewManageContentService } from './view-manage-content.service';

describe('ViewManageContentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewManageContentService = TestBed.get(ViewManageContentService);
    expect(service).toBeTruthy();
  });
});
