import { TestBed } from '@angular/core/testing';

import { AdminEditSubscriptionService } from './admin-edit-subscription.service';

describe('AdminEditSubscriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminEditSubscriptionService = TestBed.get(AdminEditSubscriptionService);
    expect(service).toBeTruthy();
  });
});
