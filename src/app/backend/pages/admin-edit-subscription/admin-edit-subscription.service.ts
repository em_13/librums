import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class AdminEditSubscriptionService {
  editsub: any;
  updatesub: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getSubscriptionDetail(id) {
    this.editsub = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.editsub.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getSubscriptionDetail`,
      { subscription_id: id },
      {
        headers
      }
    );
  }

  updateSubscription(data) {
    this.updatesub = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.updatesub.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `updateSubscription`, data, {
      headers
    });
  }
}
