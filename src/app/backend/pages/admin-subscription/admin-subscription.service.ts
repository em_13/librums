import { Injectable } from "@angular/core";
import { BaseUrl } from "./../../../base-url";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AdminSubscriptionService {
  subdata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getSubscriptions() {
    this.subdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.subdata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `getSubscriptions`, null, {
      headers
    });
  }

  updateStatus(data) {
    this.subdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.subdata.data.token
    });
    return this.http.post(this.base + `updatesubcriptionstatus`, data, {
      headers
    });
  }
}
