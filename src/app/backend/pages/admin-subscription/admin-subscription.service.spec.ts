import { TestBed } from '@angular/core/testing';

import { AdminSubscriptionService } from './admin-subscription.service';

describe('AdminSubscriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminSubscriptionService = TestBed.get(AdminSubscriptionService);
    expect(service).toBeTruthy();
  });
});
