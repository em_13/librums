import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ManageCopyrightService {
  copyrightdata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getCopyrights() {
    this.copyrightdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.copyrightdata.data.token
    });
    return this.http.post(this.base + `getCopyrights`, null, {
      headers
    });
  }
  updateStatus(data) {
    this.copyrightdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.copyrightdata.data.token
    });
    return this.http.post(this.base + `updateCopyrightStatus`, data, {
      headers
    });
  }
}
