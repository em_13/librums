import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCopyrightComponent } from './manage-copyright.component';

describe('ManageCopyrightComponent', () => {
  let component: ManageCopyrightComponent;
  let fixture: ComponentFixture<ManageCopyrightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCopyrightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCopyrightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
