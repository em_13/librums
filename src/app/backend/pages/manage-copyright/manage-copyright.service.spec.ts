import { TestBed } from '@angular/core/testing';

import { ManageCopyrightService } from './manage-copyright.service';

describe('ManageCopyrightService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageCopyrightService = TestBed.get(ManageCopyrightService);
    expect(service).toBeTruthy();
  });
});
