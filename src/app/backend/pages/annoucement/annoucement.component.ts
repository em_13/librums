import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AnnoucementService } from './annoucement.service';
declare var $:any;
@Component({
  selector: 'app-annoucement',
  templateUrl: './annoucement.component.html',
  styleUrls: ['./annoucement.component.css']
})
export class AnnoucementComponent implements OnInit {
  updateForm:FormGroup;
  loading = false;
  submitted = false;
  showMsg : any;
  showImg = true;
  errorMsg = '';
  constructor(private formBuilder : FormBuilder, private annoucement : AnnoucementService, private toaster: ToastrService) { 

  }

  ngOnInit() {
    this.updateForm = this.formBuilder.group({
      message : ["",Validators.required]
    })
  }

  get f() { return this.updateForm.controls;}
  onSubmit(){
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.loading = true;
    this.annoucement.adminAnnoucment(this.updateForm.value).subscribe(
      (res: any) => {
        $("#message").text('');
        $("#message").val('');
        if (res.status) {
          this.toaster.success(res.message)
        } else {
          this.toaster.error(res.message)
        }
      },
      error => {
        this.loading = false;
      }
    );
  }

}
