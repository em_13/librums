import { Component, OnInit } from "@angular/core";
import { ViewBookReportService } from "./view-book-report.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-view-book-report",
  templateUrl: "./view-book-report.component.html",
  styleUrls: ["./view-book-report.component.css"]
})
export class ViewBookReportComponent implements OnInit {
  managePages: any;
  temp: boolean;
  getReport: any;
  // tslint:disable-next-line: variable-name
  report_id: string;
  showLoader = false
  constructor(
    private report: ViewBookReportService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.showLoader = true;
    this.report_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadPages();
  }
  loadPages() {
    this.report.getReportDetail(this.report_id).subscribe(
      (res: any) => {
        this.getReport = res.data;
        this.showLoader = false
      },
      () => {
        console.log("ERROR");
      }
    );
  }
}
