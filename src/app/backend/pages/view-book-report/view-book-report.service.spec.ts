import { TestBed } from '@angular/core/testing';

import { ViewBookReportService } from './view-book-report.service';

describe('ViewBookReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewBookReportService = TestBed.get(ViewBookReportService);
    expect(service).toBeTruthy();
  });
});
