import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBookReportComponent } from './view-book-report.component';

describe('ViewBookReportComponent', () => {
  let component: ViewBookReportComponent;
  let fixture: ComponentFixture<ViewBookReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBookReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBookReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
