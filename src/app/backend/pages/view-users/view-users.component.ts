import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { ViewUsersService } from "./view-users.service";

@Component({
  selector: "app-view-users",
  templateUrl: "./view-users.component.html",
  styleUrls: ["./view-users.component.css"]
})
export class ViewUsersComponent implements OnInit {
  userid;
  viewdata: any;
  baseimage: any;
  showLoader = false;
  public temp: Object = false;
  constructor(
    private view: ViewUsersService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.userid = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadManage();
  }

  loadManage() {
    // this.view.manageContent(this.userid).subscribe(
    this.view.manageContent(this.userid).subscribe(
      (res: any) => {
        if (res.status) {
          this.viewdata = res.data;
          console.log(res.data);
          this.temp = true;
          this.showLoader = false;
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
}
