import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ViewUsersService {
  managedata: any;
  baseurl: any;
  constructor(private http: HttpClient) {
    this.baseurl = BaseUrl.admin;
  }

  manageContent(id) {
    this.managedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.managedata.data.token
    });
    return this.http.post(
      this.baseurl + `manageContent`,
      { user_id: id },
      {
        headers
      }
    );
  }
}
