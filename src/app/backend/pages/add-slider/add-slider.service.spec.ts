import { TestBed } from '@angular/core/testing';

import { AddSliderService } from './add-slider.service';

describe('AddSliderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddSliderService = TestBed.get(AddSliderService);
    expect(service).toBeTruthy();
  });
});
