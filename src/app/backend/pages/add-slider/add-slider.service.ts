import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AddSliderService {

  usersdata: any;
  usersimage: any;
  updateuser: any;
  baseimage: any;
  base: any;
  constructor(private http: HttpClient) {
    this.baseimage = BaseUrl.imageApi;
    this.base = BaseUrl.admin;
  }
  user_image(image) {
    this.usersimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      // "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": this.usersimage.data.token
    });
    return this.http.post(this.baseimage + `slider`, image, {
      headers
    });
  }
  addSlider(data) {
    this.usersimage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersimage.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `addSlider`, data, {
      headers
    });
  }
}
