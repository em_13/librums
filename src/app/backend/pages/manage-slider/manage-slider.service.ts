import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ManageSliderService {

  typedata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getSliders() {
    // alert('call');
    this.typedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.typedata.data.token
    });
    return this.http.post(this.base + `getSliders`, null, {
      headers
    });
  }
  deleteSlider(feedback_id) {
    this.typedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.typedata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `deleteSlider`, feedback_id, {
      headers
    });
  }
}
