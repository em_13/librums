import { TestBed } from '@angular/core/testing';

import { ManageSliderService } from './manage-slider.service';

describe('ManageSliderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageSliderService = TestBed.get(ManageSliderService);
    expect(service).toBeTruthy();
  });
});
