import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ViewUserDetaliService {
  usersdata: any;
  baseurl: any;
  constructor(private http: HttpClient) {
    this.baseurl = BaseUrl.admin;
  }

  getUserData(id) {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(
      this.baseurl + `getUserData`,
      { user_id: id },
      {
        headers
      }
    );
  }
}
