import { TestBed } from '@angular/core/testing';

import { ViewUserDetaliService } from './view-user-detali.service';

describe('ViewUserDetaliService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewUserDetaliService = TestBed.get(ViewUserDetaliService);
    expect(service).toBeTruthy();
  });
});
