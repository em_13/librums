import { BaseUrl } from "./../../../base-url";
import { Component, OnInit } from "@angular/core";
import { EditUsersService } from "../edit-users/edit-users.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-view-user-detali",
  templateUrl: "./view-user-detali.component.html",
  styleUrls: ["./view-user-detali.component.css"]
})
export class ViewUserDetaliComponent implements OnInit {
  userid: any;
  usersUpdate: any;
  baseimage: any;
  public temp: Object = false;
  showLoader = false
  constructor(
    private editUserService: EditUsersService,
    private activatedRoute: ActivatedRoute
  ) {
    this.baseimage = BaseUrl.image;
  }

  ngOnInit() {
    this.showLoader = true;
    this.userid = this.activatedRoute.snapshot.paramMap.get("id");
    this.getUserData();
  }
  getUserData() {
    this.editUserService.getUserData(this.userid).subscribe(
      (res: any) => {
        if (res.status) {
          this.usersUpdate = res.data;
          this.temp = true;
          this.showLoader = false
        }
      },
      error => {
        console.log("ERROR");
      }
    );
  }
}
