import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUserDetaliComponent } from './view-user-detali.component';

describe('ViewUserDetaliComponent', () => {
  let component: ViewUserDetaliComponent;
  let fixture: ComponentFixture<ViewUserDetaliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUserDetaliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserDetaliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
