import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageContentTypeComponent } from './manage-content-type.component';

describe('ManageContentTypeComponent', () => {
  let component: ManageContentTypeComponent;
  let fixture: ComponentFixture<ManageContentTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageContentTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageContentTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
