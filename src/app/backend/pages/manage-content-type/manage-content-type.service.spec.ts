import { TestBed } from '@angular/core/testing';

import { ManageContentTypeService } from './manage-content-type.service';

describe('ManageContentTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageContentTypeService = TestBed.get(ManageContentTypeService);
    expect(service).toBeTruthy();
  });
});
