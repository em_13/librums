import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ManageContentTypeService {
  typedata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getContentTypes() {
    this.typedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.typedata.data.token
    });
    return this.http.post(this.base + `getContentTypes`, null, {
      headers
    });
  }
  updateStatus(data) {
    this.typedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.typedata.data.token
    });
    return this.http.post(this.base + `updateContentTypeStatus`, data, {
      headers
    });
  }
}
