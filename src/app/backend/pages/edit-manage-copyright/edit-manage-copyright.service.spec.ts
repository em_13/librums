import { TestBed } from '@angular/core/testing';

import { EditManageCopyrightService } from './edit-manage-copyright.service';

describe('EditManageCopyrightService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditManageCopyrightService = TestBed.get(EditManageCopyrightService);
    expect(service).toBeTruthy();
  });
});
