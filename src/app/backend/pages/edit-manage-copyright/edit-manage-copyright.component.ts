import { Component, OnInit } from "@angular/core";
import { EditManageCopyrightService } from "./edit-manage-copyright.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { WhiteSpace } from 'src/app/_helpers/must-match'

@Component({
  selector: "app-edit-manage-copyright",
  templateUrl: "./edit-manage-copyright.component.html",
  styleUrls: ["./edit-manage-copyright.component.css"]
})
export class EditManageCopyrightComponent implements OnInit {
  getcopy: any;
  temp: boolean;
  updateForm: any;
  copyrightid: any;
  loading = false;
  submitted = false;
  showMsg = false;
  showImg = true;
  errorMsg: any;
  alertService: any;
  constructor(
    private editcopy: EditManageCopyrightService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.copyrightid = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadCopy();
    this.updateForm = this.formBuilder.group({
      copyright_id: [""],
      name: ["", Validators.required]
    });
  }

  loadCopy() {
    // this.manageservice.getBookDetail("5def94f7ed54f659fae740fe").subscribe(
    this.editcopy.getCopyrightDetail(this.copyrightid).subscribe(
      (res: any) => {
        // console.log(res.data);
        this.getcopy = res.data;
        this.temp = true;
      },
      () => {
        console.log("ERROR");
      }
    );
  }

  get f() {
    return this.updateForm.controls;
  }
  public cleanForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((key) => formGroup.get(key).setValue(formGroup.get(key).value.trim()));
  }
  onSubmit() {
    // console.log(this.updateForm.value);
    this.cleanForm(this.updateForm);
    // console.log(this.updateForm.value);
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.loading = true;
    this.submitted = true;
    this.editcopy.updateCopyright(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          // this.showMsg = res.message;
          this.toaster.success('Copyright updated successfully.')
          this.router.navigate(["/manage-copyright"]);
        } else {
          this.toaster.error(res.message)
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
}
