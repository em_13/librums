import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManageCopyrightComponent } from './edit-manage-copyright.component';

describe('EditManageCopyrightComponent', () => {
  let component: EditManageCopyrightComponent;
  let fixture: ComponentFixture<EditManageCopyrightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditManageCopyrightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManageCopyrightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
