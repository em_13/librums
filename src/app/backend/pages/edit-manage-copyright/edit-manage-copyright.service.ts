import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class EditManageCopyrightService {
  editcopyright: any;
  updatecopyright: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getCopyrightDetail(id) {
    this.editcopyright = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.editcopyright.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getCopyrightDetail`,
      { copyright_id: id },
      {
        headers
      }
    );
  }

  updateCopyright(data) {
    this.updatecopyright = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.updatecopyright.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `updateCopyright`, data, {
      headers
    });
  }
}
