import { TestBed } from '@angular/core/testing';

import { AddManageContentTypeService } from './add-manage-content-type.service';

describe('AddManageContentTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddManageContentTypeService = TestBed.get(AddManageContentTypeService);
    expect(service).toBeTruthy();
  });
});
