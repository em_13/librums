import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class AddManageContentTypeService {
  addtype: any;
  base: string;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  addContentType(data) {
    this.addtype = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.addtype.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `addContentType`, data, {
      headers
    });
  }
}
