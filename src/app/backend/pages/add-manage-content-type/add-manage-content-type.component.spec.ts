import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddManageContentTypeComponent } from './add-manage-content-type.component';

describe('AddManageContentTypeComponent', () => {
  let component: AddManageContentTypeComponent;
  let fixture: ComponentFixture<AddManageContentTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddManageContentTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddManageContentTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
