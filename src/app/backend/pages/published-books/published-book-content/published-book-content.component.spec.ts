import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedBookContentComponent } from './published-book-content.component';

describe('PublishedBookContentComponent', () => {
  let component: PublishedBookContentComponent;
  let fixture: ComponentFixture<PublishedBookContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishedBookContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishedBookContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
