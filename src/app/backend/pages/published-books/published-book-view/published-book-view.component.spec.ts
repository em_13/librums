import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedBookViewComponent } from './published-book-view.component';

describe('PublishedBookViewComponent', () => {
  let component: PublishedBookViewComponent;
  let fixture: ComponentFixture<PublishedBookViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishedBookViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishedBookViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
