import { TestBed } from '@angular/core/testing';

import { PublishedBookService } from './published-book.service';

describe('PublishedBookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublishedBookService = TestBed.get(PublishedBookService);
    expect(service).toBeTruthy();
  });
});
