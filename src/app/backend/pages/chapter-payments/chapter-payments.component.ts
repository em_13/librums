import { Component, OnInit } from '@angular/core';
import { PaymentDashboardService } from '../payments-dashboard/payment-dashboard.service';


@Component({
  selector: 'app-chapter-payments',
  templateUrl: './chapter-payments.component.html',
  styleUrls: ['./chapter-payments.component.css']
})
export class ChapterPaymentsComponent implements OnInit {
  public getpayments: Object;
  public temp: Object = false;
  showLoader = false;
  constructor(private payment : PaymentDashboardService) { }

  ngOnInit() {
    this.showLoader = true;
    this.loadReports();
  }

  loadReports() {
    this.payment.getChapterPayments().subscribe(
      (res: any) => {
        this.getpayments = res.data;
        this.temp = true;
        this.showLoader = false
      },
      error => {
        console.log("ERROR");
      }
    );
  }

}
