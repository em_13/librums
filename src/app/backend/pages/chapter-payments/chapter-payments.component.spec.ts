import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterPaymentsComponent } from './chapter-payments.component';

describe('ChapterPaymentsComponent', () => {
  let component: ChapterPaymentsComponent;
  let fixture: ComponentFixture<ChapterPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChapterPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
