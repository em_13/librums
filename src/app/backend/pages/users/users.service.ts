import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class UsersService {
  usersdata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getUsers() {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(this.base + `getUsers`, null, {
      headers
    });
  }

  getAuthors() {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(this.base + `getAuthors`, null, {
      headers
    });
  }

  UpdateUserStatus(data) {
    this.usersdata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.usersdata.data.token
    });
    return this.http.post(this.base + `UpdateUserStatus`, data, {
      headers
    });
  }
}
