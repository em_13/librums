import { TestBed } from '@angular/core/testing';

import { MessageBoardService } from './message-board.service';

describe('MessageBoardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessageBoardService = TestBed.get(MessageBoardService);
    expect(service).toBeTruthy();
  });
});
