import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class MessageBoardService {
  messagedata: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getReports() {
    this.messagedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.messagedata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `getReports`, null, {
      headers
    });
  }

  deleteReport(id) {
    this.messagedata = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.messagedata.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `deleteReport`, id, {
      headers
    });
  }
}
