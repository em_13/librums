import { TestBed } from '@angular/core/testing';

import { AddManageGenreService } from './add-manage-genre.service';

describe('AddManageGenreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddManageGenreService = TestBed.get(AddManageGenreService);
    expect(service).toBeTruthy();
  });
});
