import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddManageGenreComponent } from './add-manage-genre.component';

describe('AddManageGenreComponent', () => {
  let component: AddManageGenreComponent;
  let fixture: ComponentFixture<AddManageGenreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddManageGenreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddManageGenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
