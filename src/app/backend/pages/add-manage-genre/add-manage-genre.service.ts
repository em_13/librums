import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class AddManageGenreService {
  addgenre: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  addGenre(data) {
    this.addgenre = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.addgenre.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `addGenre`, data, {
      headers
    });
  }
}
