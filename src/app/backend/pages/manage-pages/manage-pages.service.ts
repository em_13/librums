import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class ManagePagesService {
  managepage: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getPages() {
    this.managepage = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.managepage.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `getPages`, null, {
      headers
    });
  }
}
