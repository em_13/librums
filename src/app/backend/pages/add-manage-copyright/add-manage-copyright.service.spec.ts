import { TestBed } from '@angular/core/testing';

import { AddManageCopyrightService } from './add-manage-copyright.service';

describe('AddManageCopyrightService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddManageCopyrightService = TestBed.get(AddManageCopyrightService);
    expect(service).toBeTruthy();
  });
});
