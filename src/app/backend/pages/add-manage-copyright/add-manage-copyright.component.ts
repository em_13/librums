import { Component, OnInit } from '@angular/core';
import { AddManageCopyrightService } from "./add-manage-copyright.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { WhiteSpace } from 'src/app/_helpers/must-match'
@Component({
  selector: 'app-add-manage-copyright',
  templateUrl: './add-manage-copyright.component.html',
  styleUrls: ['./add-manage-copyright.component.css']
})
export class AddManageCopyrightComponent implements OnInit {
  updateForm: any;
  loading = false;
  submitted = false;
  showMsg = false;
  errorMsg: any;
  alertService: any;
  constructor(
    private addcopy: AddManageCopyrightService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private toaster: ToastrService
  ) { }

  ngOnInit() {
     this.updateForm = this.formBuilder.group({
      name: ["", Validators.required]
    });
  }

  get f() {
    return this.updateForm.controls;
  }
  public cleanForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((key) => formGroup.get(key).setValue(formGroup.get(key).value.trim()));
  }
  onSubmit() {
    // console.log(this.updateForm.value);
    this.cleanForm(this.updateForm);
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.loading = true;
    this.submitted = true;
    this.addcopy.addCopyright(this.updateForm.value).subscribe(
      (res: any) => {
        if (res.status) {
          this.toaster.success("Copyright added successfully.")
          this.router.navigate(["/manage-copyright"]);
        } else {
          this.toaster.error(res.message)
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

}
