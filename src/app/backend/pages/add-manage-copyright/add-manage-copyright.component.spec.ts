import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddManageCopyrightComponent } from './add-manage-copyright.component';

describe('AddManageCopyrightComponent', () => {
  let component: AddManageCopyrightComponent;
  let fixture: ComponentFixture<AddManageCopyrightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddManageCopyrightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddManageCopyrightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
