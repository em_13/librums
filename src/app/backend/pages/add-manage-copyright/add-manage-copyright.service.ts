import { BaseUrl } from "./../../../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root"
})
export class AddManageCopyrightService {
  addcopy: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  addCopyright(data) {
    this.addcopy = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.addcopy.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(this.base + `addCopyright`, data, {
      headers
    });
  }
}
