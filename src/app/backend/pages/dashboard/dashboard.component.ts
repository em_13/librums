import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DashboardService } from "./dashboard.service";
declare var $: any;
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  getreports: any;
  showLoader = false;
  temp = false;
  showWelcom = true;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dashboard: DashboardService
  ) {}

  ngOnInit() {
    this.showLoader = true;
    this.getDashboardData();
    
  }

  getDashboardData() {
    this.dashboard.getReports().subscribe(
      (res: any) => {
        this.getreports = res.data;
        this.showLoader = false;
        this.temp = true;
      },
      error => {
        console.log("ERROR");
      }
    );
  }
  viewFeedback(id){
    this.router.navigate(["/view-feedback"],{ queryParams: { id: id } });
  }
}
