import { Component, OnInit } from "@angular/core";
import { ViewMessageBoardService } from "./view-message-board.service";
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-view-message-board",
  templateUrl: "./view-message-board.component.html",
  styleUrls: ["./view-message-board.component.css"]
})
export class ViewMessageBoardComponent implements OnInit {
  managePages: any;
  temp: boolean;
  getReport: any;
  // tslint:disable-next-line: variable-name
  report_id: string;
  showLoader = false
  constructor(
    private report: ViewMessageBoardService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.showLoader = true;
    this.report_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.loadPages();
  }
  loadPages() {
    this.report.getReportDetail(this.report_id).subscribe(
      (res: any) => {
        this.getReport = res.data;
        this.showLoader = false;
      },
      () => {
        console.log("ERROR");
      }
    );
  }
}
