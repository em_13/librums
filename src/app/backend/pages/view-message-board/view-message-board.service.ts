import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseUrl } from "./../../../base-url";
@Injectable({
  providedIn: "root"
})
export class ViewMessageBoardService {
  viewreport: any;
  base: any;
  constructor(private http: HttpClient) {
    this.base = BaseUrl.admin;
  }

  getReportDetail(id) {
    this.viewreport = JSON.parse(localStorage.getItem("currentUser"));
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-access-token": this.viewreport.data.token
    });
    // tslint:disable-next-line: align
    return this.http.post(
      this.base + `getReportDetail`,
      { report_id: id },
      {
        headers
      }
    );
  }
}
