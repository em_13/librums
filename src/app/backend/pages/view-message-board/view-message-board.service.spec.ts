import { TestBed } from '@angular/core/testing';

import { ViewMessageBoardService } from './view-message-board.service';

describe('ViewMessageBoardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewMessageBoardService = TestBed.get(ViewMessageBoardService);
    expect(service).toBeTruthy();
  });
});
