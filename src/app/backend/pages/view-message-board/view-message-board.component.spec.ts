import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMessageBoardComponent } from './view-message-board.component';

describe('ViewMessageBoardComponent', () => {
  let component: ViewMessageBoardComponent;
  let fixture: ComponentFixture<ViewMessageBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMessageBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMessageBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
