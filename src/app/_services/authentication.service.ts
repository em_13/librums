import { BaseUrl } from "./../base-url";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "../_models/user";
import { ActivatedRoute, Router } from "@angular/router";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  base: any;
  userdata: any;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();

    this.base = BaseUrl.frontend;
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  checkUserStatus(){
    this.userdata = JSON.parse(localStorage.getItem("currentUser"));
    if(this.userdata){
      let headers = new HttpHeaders({
        "Content-Type": "application/json",
        "x-access-token": this.userdata.data.token
      });
      return this.http.post(this.base + `/getUserProfile`, null ,{ headers: headers }
      );
    }
  }

  // tslint:disable-next-line: variable-name
  login(email: string, password: string, device_token: string) {
    return this.http
      .post<any>(this.base + `/login`, {
        email,
        password,
        device_token
      })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user.status) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem("currentUser", JSON.stringify(user));
            this.currentUserSubject.next(user);
            return user;
          } else {
            return user;
          }
        })
      );
  }

  forget(email: string) {
    return this.http
      .post<any>(this.base + `/forgotpassword`, { email })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user.status) {
            return user;
          } else {
            return user;
          }
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("currentUser");
    localStorage.setItem("role", "guset");
    // window.location.href = "login";
    return;
  }
}
