import { TestBed } from '@angular/core/testing';

import { SocialloginServiceService } from './sociallogin-service.service';

describe('SocialloginServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocialloginServiceService = TestBed.get(SocialloginServiceService);
    expect(service).toBeTruthy();
  });
});
