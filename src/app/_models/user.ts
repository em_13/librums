export class User {
  id: number;
  email: string;
  password: string;
  fullname: string;
  dob: string;
  token: string;
}
