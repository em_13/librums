// $(document).ready(function () {
//   new WOW().init();
// });
var $canvas = $("#canvas");
var canvasOffset = $canvas.offset();
var offsetX;
// alert(offsetX);
var offsetY;


options = {

  // Required. Called when a user selects an item in the Chooser.
  success: function (files) {
    console.log(files)
    alert("Here's the file link: " + files[0].link)

    var filename = files[0].thumbnailLink.replace("bounding_box=75", "bounding_box=800");
    // console.log(filename)
    // cropzeeTriggerModal1('cover_image', filename);

    getBase64Image(filename, function (base64image) {
      console.log(base64image);
      console.log('Here');
    });
  },

  // Optional. Called when the user clos  es the dialog without selecting a file
  // and does not include any parameters.
  cancel: function () {

  },

  // Optional. "preview" (default) is a preview link to the document for sharing,
  // "direct" is an expiring link to download the contents of the file. For more
  // information about link types, see Link types below.
  linkType: "preview", // or "direct"

  // Optional. A value of false (default) limits selection to a single file, while
  // true enables multiple file selection.
  multiselect: false, // or true

  // Optional. This is a list of file extensions. If specified, the user will
  // only be able to select files with these extensions. You may also specify
  // file types, such as "video" or "images" in the list. For more information,
  // see File types below. By default, all extensions are allowed.
  extensions: ['.jpeg', '.png'],

  // Optional. A value of false (default) limits selection to files,
  // while true allows the user to select both folders and files.
  // You cannot specify `linkType: "direct"` when using `folderselect: true`.
  folderselect: false, // or true

  // Optional. A limit on the size of each file that may be selected, in bytes.
  // If specified, the user will only be able to select files with size
  // less than or equal to this limit.
  // For the purposes of this option, folders have size zero.
  //sizeLimit: 1024, // or any positive number
};

var mainText;
var size;
$(document).on('click', '#crop1', function () {
  var orgImg = $('#cropbox1').get(0);
  // draw image to canvas and get image data
  var canvas = document.createElement("canvas");
  canvas.width = orgImg.width;
  canvas.height = orgImg.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(orgImg, 0, 0);
  var imageData = ctx.getImageData(size.x, size.y, 270, 360);

  // create destiantion canvas
  var canvas1 = document.createElement("canvas");
  canvas1.width = 270;
  canvas1.height = 360;
  var ctx1 = canvas1.getContext("2d");
  ctx1.rect(0, 0, 270, 360);
  ctx1.fillStyle = 'white';
  ctx1.fill();
  ctx1.putImageData(imageData, 0, 0);

  // put data to the img element
  var dstImg = $('#blah').get(0);
  dstImg.src = canvas1.toDataURL("image/png");
  $("#cropImage1").hide();
  $("#first_sec").show();//kuldeep
  $("#crop_sec").hide();


});
/**Dropbox */
function cropzeeTriggerModal1(id, src) {
  // take in animation option and add 'animated' before it
  var animation = options.modalAnimation;
  if (animation) {
    if (animation.indexOf('animated') == -1) {
      animation = 'animated ' + animation;
    }
  }
  // modal element with dynamic image data, dynamic animation class as supported by animate.css and dynamic input id
  // lightmodal see https://hunzaboy.github.io/Light-Modal/#
  var lightmodalHTML =
    '<div class="light-modal" id="cropzee-modal" data-backdrop="static"  role="dialog" aria-labelledby="light-modal-label" aria-hidden="false" data-lightmodal="close">'
    + '<div class="light-modal-content ' + animation + '">'
    + '<!-- light modal header -->'
    + '<!-- <div class="light-modal-header">'
    + '<h3 class="light-modal-heading">Cropzee</h3>'
    + '<a href="#" class="light-modal-close-icon" aria-label="close">&times;</a>'
    + '</div> -->'
    + '<!-- light modal body -->'
    + '<div class="light-modal-body" style="max-height: 500px;">'
    + '<img height="500" id="cropzee-modal-image" src="' + src + '">'
    + '</div>'
    + '<!-- light modal footer -->'
    + '<div class="light-modal-footer" style="justify-content: space-between;">'
    + '<div id="cancelall" class="light-modal-close-btn" style="cursor: pointer;" aria-label="close">Cancel</div>'
    // + '<div onclick="cropzeeRotateImage(`' + id + '`);" class="light-modal-close-btn" style="cursor: pointer;">Rotate 90deg</div>'
    + '<div onclick="cropzeeCreateImage(`' + id + '`);" class="light-modal-close-btn" style="cursor: pointer;">Done</div>'
    + '</div>'
    + '</div>'
    + '<canvas style="position: absolute; top: -99999px; left: -99999px;" id="cropzee-hidden-canvas"></canvas>'
    + '<a style="display:none;" id="cropzee-link"></a>'
    + '</div>';
  // modal element is appended to body
  $("body").append(lightmodalHTML);
  // alert('hello');

  // after which the inserted image is drawn onto the hidden canvas within the modal
  setTimeout(function () {
    // alert('sdds');  
    var canvas = document.getElementById('cropzee-hidden-canvas');
    var ctx = canvas.getContext('2d');
    ctx.canvas.width = 300;
    ctx.canvas.height = 300;
    var img = new Image();
    img.src = src;
    ctx.drawImage(img, 0, 0, 300, 300);
    setTimeout(function () {
      // the css-only modal is called via href see https://hunzaboy.github.io/Light-Modal/#
      window.location = base_url + "#cropzee-modal";
      // function to trigger croppr.js on picture in modal
      cropzeeTriggerCroppr();
      //   $('.undefined').remove();
    }, 50);
  }, 50);
}
// function getBase64Image(imgUrl, callback) {

//   var img = new Image();

//   // onload fires when the image is fully loadded, and has width and height

//   img.onload = function(){

//     var canvas = document.createElement("canvas");
//     canvas.width = img.width;
//     canvas.height = img.height;
//     var ctx = canvas.getContext("2d");
//     ctx.drawImage(img, 0, 0);
//     var dataURL = canvas.toDataURL("image/png"),
//     dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

//     callback(dataURL); // the base64 string

//   };

//   // set attributes and src crossorigin="anonymous"
//   // img.setAttribute('crossOrigin', 'Anonymous'); 
//   img.crossOrigin = "anonymous";
//   // img.addEventListener("load", imageReceived, false);
//   img.src = imgUrl;

// }
/**Dropbox */
/**Crop 2 */
$(document).on('click', '#crop2', function () {
  var orgImg = $('#cropbox2').get(0);
  // draw image to canvas and get image data
  var canvas = document.createElement("canvas");
  canvas.width = orgImg.width;
  canvas.height = orgImg.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(orgImg, 0, 0);
  var imageData = ctx.getImageData(size.x, size.y, 270, 360);

  // create destiantion canvas
  var canvas1 = document.createElement("canvas");
  canvas1.width = 270;
  canvas1.height = 360;
  var ctx1 = canvas1.getContext("2d");
  ctx1.rect(0, 0, 270, 360);
  ctx1.fillStyle = 'white';
  ctx1.fill();
  ctx1.putImageData(imageData, 0, 0);

  // put data to the img element
  var dstImg = $('#second').get(0);
  dstImg.src = canvas1.toDataURL("image/png");
  $("#first_sec").show();
  $("#crop_sec").hide();
  $("#cropImage2").hide();


});
/**Crop 2 */

$(document).on('change', '#slide', function () {
  var v = $(this).val();
  console.log('here', v);
  $('#cc').css('font-size', v + 'px')
  //$('span').css('font-size', v + 'px')
  //$('span').html(v);
});

$(document).on('change', '#transparency', function () {
  var v = $(this).val();
  console.log(v);
  if ($('#second').attr('src') !== '') {
    $('#second').css('opacity', v)
  } else {
    $('#blah').css('opacity', v)
  }


});

$(document).on('click', '.sugestions', function () {
  $('.sujection_box').hide();
});
$(document).on('change', '#brightness', function () {
  var v = $(this).val();
  console.log(v);
  if ($('#second').attr('src') !== '') {
    console.log('second');
    $('#second').css('filter', 'brightness(' + v + ')')
  } else {
    console.log('blah');
    $('#blah').css('filter', 'brightness(' + v + ')');
  }

});

$(document).on('click', '.custom-owl-prev', function () {
  console.log('hereree');
  $(this).trigger('prev.owl.carousel');
});
$(document).on('click', '.custom-owl-prev-explore', function () {


  // currentOwlIndex = $(this).attr('id'); 
  // console.log(currentOwlIndex);
});
var currentOwlIndex = 0;
$(document).on('initialized.owl.carousel translate.owl.carousel', function (e) {
  $(this).trigger('to.owl.carousel', 3);
  idx = e.item.index;

  console.log(e);

  $('.owl-item.big').removeClass('big');
  $('.owl-item.medium').removeClass('medium');
  // setTimeout(function(){ 


  $('#caro_' + currentOwlIndex).find('.owl-item').removeClass('previous1');
  $('#caro_' + currentOwlIndex).find('.owl-item').removeClass('next1');
  $('#caro_' + currentOwlIndex).find('.owl-item').eq(idx).addClass('big');
  $('#caro_' + currentOwlIndex).find('.owl-item').eq(idx - 1).addClass('previous1');
  $('#caro_' + currentOwlIndex).find('.owl-item').eq(idx + 1).addClass('next1');
  // }, 300);
});

$(document).on('click', '.carousel-indicators li', function () {
  $('.carousel-indicators li').removeClass('active');
  console.log($(this));
  $(this).addClass('active');
})
$(document).on('click', '.custom-owl-next', function () {
  // if($(this).parent().closest('.item').parent().next('.active').next('.owl-item').next('.owl-item').length == 0 ){
  //   $(this).parent().hide()
  // }

  $(this).trigger('next.owl.carousel');
});

$(document).on('click', '.custom-owl-next-explore', function () {


  // currentOwlIndex = $(this).attr('id'); 
  // console.log(currentOwlIndex);
});

/***************** Add Story Jquery ************************/
$(document).on('click', '#edit-cover', function () {
  var d_canvas = document.getElementById('canvas');
  var context = d_canvas.getContext('2d');
  context.clearRect(0, 0, d_canvas.width, d_canvas.height);
  $('#canvas').hide();
  $('#blah').show();
  $('#remove_first').show();
  $('#save_image').hide();
  $("#next1").show();
  if ($('#second').attr('src') !== '') {
    $('#second').show();
    $('#second_browse').hide();
    $('#second_browse1').hide();
    $('#remove_second').show();
    $(".another-img").hide();
    $('#crop1').hide();
    $('#crop2').show();
    $("#transparency").val('0.5');
  } else {
    // $('#main_forms').hide();//kuldeep
    $('#first_browse').hide();
    $('#second_browse').show();
    $('#first_browse1').hide();
    $('#second_browse1').show();
    $('#dummy').hide();
    $(".add-img").hide();
    $(".another-img").show();
  }
});
$(document).on('click', '#cancelall', function () {
  // if (confirm('Are you sure?')) {
  $('#next3').hide();
  $('#cropzee-modal').remove();
  window.location = window.location.href + '#';
  $('#save_image').hide();
  $('#final').attr('src', 'assets/img/rectangle1.svg');
  localStorage.removeItem("image");
  $('#text_functionality').hide();
  $('#draw').hide();
  $('#canvas').hide();
  $('#filters').hide();
  $('#final_save').show();
  $('#main_forms').show();
  $('#after_create').hide();
  $('#before_create').show();
  // $('#first_sec').hide();//kuldeep
  $('#remove_first').hide();
  $('#remove_second').hide();
  $('#first_browse').show();
  $('#first_browse1').show();
  // if($( "#first_browse" ).has( "img" )){
  // alert($( "#first_browse" ).find('label').length);
  if ($("#first_browse").find('label').length == 0) {
    $('#first_browse').append('<label for="cover_image" data-cropzee="cover_image">' +
      '<img src="assets/img/add1.svg" alt="">' +
      '</label>');
  }
  // if($( "#second_browse" ).has( "img" )){
  // alert($( "#second_browse" ).find('label').length);
  if ($("#second_browse").find('label').length == 0) {
    $('#second_browse').append('<label for="cover_image2" data-cropzee="cover_image2">' +
      '<img src="assets/img/add1.svg" alt="">' +
      '</label>');
  }
  $('#second_browse').hide();
  $('#second_browse1').hide();
  $('#dummy').show();
  $('#blah').hide();
  $('#second').hide();
  $('#blah').attr('src', '');
  $('#second').attr('src', '');
  $("#next1").hide();
  $(".add-img").show();
  $("#cancel-next1").show();
  $(".cover_img").show();
  $(".another-img").hide();
  $(".undefined").remove();
  var d_canvas = document.getElementById('canvas');
  var context = d_canvas.getContext('2d');
  context.clearRect(0, 0, d_canvas.width, d_canvas.height);
  // $(this).hide();
  // }
});

$('body').on('click', '#remove_first', function () {

  if ($('#second').attr('src') !== '') {
    alert('Please delete second image first.');
    // alert('If you want to remove first image, then firstly  remove second image and then first image.');
    return false;
  }
  $('#first_browse').append('<label for="cover_image" data-cropzee="cover_image">' +
    '<img src="assets/img/add1.svg" alt="">' +
    '</label>');
  $("#blah").attr('src', '').hide();
  $("#dummy").show();
  $("#first_browse").show();
  $("#first_browse1").show();
  $("#second_browse").hide();
  $("#second_browse1").hide();
  $("#next1").hide();
  $(".add-img").show();
  $(".another-img").hide();
  $(".undefined").hide();
  $(this).hide()
})

$('body').on('click', '#remove_second', function () {
  $("#second").attr('src', '').hide();
  $("#dummy").hide();
  $(".undefined").hide();
  $("#second_browse").show();
  $('#second_browse').append('<label for="cover_image2" data-cropzee="cover_image2">' +
    '<img src="assets/img/add1.svg" alt="">' +
    '</label>');
  $(".add-img").hide();
  $(".another-img").show();
  $("#remove_first").show();
  $(this).hide()
})


$(document).on('click', '#next1', function () {
  $('#first_browse').hide();
  $('#first_browse1').hide();
  $('#second_browse').hide();
  $('#second_browse1').hide();
  $(".another-img").hide();
  $('#next1').hide();
  $('#cancel-next1').hide();
  $('#remove_first').hide();
  $('#remove_second').hide();
  $('#filters').show();
  $('#next2').show();
  $('#cancel1').hide();
});

$(document).on('click', '.color', function () {
  $('#color_text_change').html($(this).val() + '<span  style="background:' + $(this).val() + '" class="colored_box"></span>');
  $('#color').val($(this).val());
  var textAlign = "center";
  var fontSize = $('#font').val();
  var fontFamily = $('#font_family').val();
  var fontSizeFamily = fontSize + "px " + fontFamily;
  createText(fontSizeFamily, textAlign)
})
function xyz(e) {
  alert('here');
}
$(document).on('change paste keyup keydown', '#text_color', function () {
  // $('#color_text_change').html($(this).val() + '<span  style="background:' + $(this).val() + '" class="colored_box"></span>');
  // $('#color').val($(this).val());
  var textAlign = "center";
  var fontSize = $('#font').val();
  var fontFamily = $('#font_family').val();
  var fontSizeFamily = fontSize + "px " + fontFamily;
  createText(fontSizeFamily, textAlign)
})

$(document).on('change', '#font_family', function () {
  var textAlign = "center";
  var fontSize = $('#font').val();
  var fontFamily = $(this).val();
  var fontSizeFamily = fontSize + "px " + fontFamily;
  createText(fontSizeFamily, textAlign)
});

$(document).on('click', '#next3', function () {
  $('#next3').hide();
  $('#text_functionality').hide();
  $('#save_image').show();
  var c = document.getElementById("canvas");
  var d = c.toDataURL("image/png");
  $('#final').attr('src', d);
  localStorage.setItem("image", d);
});

$(document).on('click', '#next2', function () {
  $('#next2').hide();
  $('#next3').show();
  $('#filters').hide();
  $('#text_functionality').show();
  $('#draw').show();
  var $ballon = $('#second'),
    $canvas = $('#canvas');
  var ballon_x = $ballon.offset().left - $canvas.offset().left,
    ballon_y = $ballon.offset().top - $canvas.offset().top;
  var d_canvas = document.getElementById('canvas');
  var context = d_canvas.getContext('2d');
  var background = document.getElementById('blah');
  var ballon = document.getElementById('second')
  context.drawImage(background, 0, 0, 270, 360);
  context.globalAlpha = $('#transparency').val();
  context.drawImage(ballon, 0, 0, 270, 360);


  // $ballon.hide();
  $(this).attr('disabled', 'disabled');
  $('#second').hide();
  $('#blah').hide();
  $('#canvas').show();
  // var c=document.getElementById("canvas");
  // var d=c.toDataURL("image/png");
  // var w=window.open('about:blank','image from canvas');
  // w.document.write("<img src='"+d+"' alt='from canvas'/>");
});
$(document).on('click', '.create_cover_btn', function () {
  $('#final_save').hide();
  $('#before_create').hide();
  $('#after_create').show();
  /**latest avtar */
  $('#next3').hide();
  $('#cropzee-modal').remove();
  window.location = window.location.href + '#';
  $('#save_image').hide();
  $('#final').attr('src', 'assets/img/rectangle1.svg');
  localStorage.removeItem("image");
  $('#text_functionality').hide();
  $('#draw').hide();
  $('#canvas').hide();
  $('#filters').hide();


  // $('#first_sec').hide();//kuldeep
  $('#remove_first').hide();
  $('#remove_second').hide();
  $('#first_browse').show();
  $('#first_browse1').show();
  // if($( "#first_browse" ).has( "img" )){
  // alert($( "#first_browse" ).find('label').length);
  if ($("#first_browse").find('label').length == 0) {
    $('#first_browse').append('<label for="cover_image" data-cropzee="cover_image">' +
      '<img src="assets/img/add1.svg" alt="">' +
      '</label>');
  }
  // if($( "#second_browse" ).has( "img" )){
  // alert($( "#second_browse" ).find('label').length);
  if ($("#second_browse").find('label').length == 0) {
    $('#second_browse').append('<label for="cover_image2" data-cropzee="cover_image2">' +
      '<img src="assets/img/add1.svg" alt="">' +
      '</label>');
  }
  $('#second_browse').hide();
  $('#second_browse1').hide();
  $('#dummy').show();
  $('#blah').hide();
  $('#second').hide();

  $('#second').attr('src', '');
  $("#next1").hide();
  $(".add-img").show();
  $("#cancel-next1").show();
  $(".cover_img").show();
  $(".another-img").hide();
  $(".undefined").remove();
  var d_canvas = document.getElementById('canvas');
  var context = d_canvas.getContext('2d');
  context.clearRect(0, 0, d_canvas.width, d_canvas.height);
  /**latest avtar */


  $('#main_forms').hide();//Avtar
  $('#first_sec').show();//kuldeep
  $('#cancel1').show();
});
/***************** Add Story Jquery ************************/

// $(document).on('change','#cover_image', function () {

//   //alert('sadasd');
//   readURL(this);
// });
// $(document).on('change','#cover_image2', function () {
//   //alert('sadasd');
//   readURL2(this);
// });

$(document).on('click', '#cancel2', function () {
  $('#main_forms').show();
  // $('#first_sec').hide();//kuldeep
  $('#first_browse').show();
  $('#first_browse1').show();
  $('#second_browse').hide();
  $('#second_browse1').hide();
  $('#next1').show();
  $('#filters').hide();
  $('#next2').hide();
  // $(this).hide();
});

$(document).on('keyup', '#img_title', function () {


  var textAlign = "center";
  var fontSize = $('#font').val();
  var fontFamily = $('#font_family').val();
  var fontSizeFamily = fontSize + "px " + fontFamily;
  createText(fontSizeFamily, textAlign)
  //  textUnderline(context,text,x,y,textColor,fontSize,textAlign);
});

$(document).on('change', '#color', function () {
  var textAlign = "center";
  var fontSize = $('#font').val();
  var fontFamily = $('#font_family').val();
  var fontSizeFamily = fontSize + "px " + fontFamily;
  createText(fontSizeFamily, textAlign)
});


$(document).on('click', '.custom-style', function () {
  // alert('here');    
  $('.custom-style').parent().removeClass('active');
  $(this).parent().addClass('active');
  var textAlign = "center";
  var fontSize = $('#font').val();
  var fontFamily = $('#font_family').val();
  var fontSizeFamily = $(this).data('func') + " " + fontSize + "px " + fontFamily;
  createText(fontSizeFamily, textAlign)
});
$(document).on('click', '.custom-align', function () {
  // alert('here');    
  $('.custom-align').parent().removeClass('active');
  $(this).parent().addClass('active');
  var textAlign = $(this).data('func');
  var fontSize = $('#font').val();
  var fontFamily = $('#font_family').val();
  var fontSizeFamily = $(this).data('func') + " " + fontSize + "px " + fontFamily;
  createText(fontSizeFamily, textAlign)
});


/**Underlline */
var textUnderline = function (context, text, x, y, color, textSize, align) {

  //Get the width of the text
  var textWidth = context.measureText(text).width;

  //var to store the starting position of text (X-axis)
  var startX;

  //var to store the starting position of text (Y-axis)
  // I have tried to set the position of the underline according 
  // to size of text. You can change as per your need
  var startY = y + (parseInt(textSize) / 15);

  //var to store the end position of text (X-axis)
  var endX;

  //var to store the end position of text (Y-axis)
  //It should be the same as start position vertically. 
  var endY = startY;

  //To set the size line which is to be drawn as underline.
  //Its set as per the size of the text. Feel free to change as per need.
  var underlineHeight = parseInt(textSize) / 15;

  //Because of the above calculation we might get the value less 
  //than 1 and then the underline will not be rendered. this is to make sure 
  //there is some value for line width.
  if (underlineHeight < 1) {
    underlineHeight = 1;
  }

  context.beginPath();
  if (align == "center") {
    startX = x - (textWidth / 2);
    endX = x + (textWidth / 2);
  } else if (align == "right") {
    startX = x - textWidth;
    endX = x;
  } else {
    startX = x;
    endX = x + textWidth;
  }

  context.strokeStyle = color;
  context.lineWidth = underlineHeight;
  context.moveTo(startX, startY);
  context.lineTo(endX, endY);
  context.stroke();
}
/**Underlline */

/**Create Text Custom Function */
var createText = function (fontSizeFamily, textAlign) {
  // alert(fontSizeFamily);
  textAlign = $('.active .custom-align').data('func');
  // alert(textAlign);
  var textColor = $('#text_color').val();
  var text = $('#img_title').val();

  var $canvas = $("#canvas");
  var canvasOffset = $canvas.offset();
  offsetX = canvasOffset.left;
  // alert(offsetX);
  offsetY = canvasOffset.top;

  var d_canvas = document.getElementById('canvas');
  var context = d_canvas.getContext('2d');
  var background = document.getElementById('blah');
  var ballon = document.getElementById('second')
  context.clearRect(0, 0, d_canvas.width, d_canvas.height);
  // context.globalAlpha = $('#transparency').val();
  context.drawImage(background, 0, 0, 270, 360);
  // context.globalAlpha = $('#brightness').val();
  context.drawImage(ballon, 0, 0, 270, 360);
  // context.globalAlpha = $('#transparency').val();
  // context.globalAlpha = $('#brightness').val();
  context.globalCompositeOperation = 'source-over';
  if (textAlign == 'right') {
    var x = d_canvas.width;
  } else if (textAlign == 'left') {
    var x = 10;
  } else {
    var x = d_canvas.width / 2;
  }
  var y = d_canvas.height / 2;
  mainText = {
    text: $("#img_title").val(),
    x: x,
    y: y
  };
  mainText.width = context.measureText(mainText.text).width;
  mainText.height = $('#font').val();
  context.font = fontSizeFamily;
  context.textAlign = textAlign;
  context.fillStyle = textColor;
  // context.fillText(text,  x, y);
  wrapText(context, text, x, y, 270, 25)
  //  textUnderline(context, text, x, y, textColor, $('#font').val(), textAlign);
}
/**Create Text Custom Function */


/**Wrap text */
function wrapText(context, text, x, y, maxWidth, lineHeight) {
  // alert(x +'ddd'+y);
  var words = text.split(' ');
  var line = '';


  for (var n = 0; n < words.length; n++) {
    var testLine = line + words[n] + ' ';
    var metrics = context.measureText(testLine);
    var testWidth = metrics.width;
    if (testWidth > maxWidth && n > 0) {
      context.fillText(line, x, y);
      if ($('.active .custom-style').data('func') == 'underline') {

        textUnderline(context, line, x, y, $('#color').val(), $('#font').val(), $('.active .custom-align').data('func'));
      }

      line = words[n] + ' ';
      y += lineHeight;
    }
    else {
      line = testLine;
    }
  }
  context.fillText(line, x, y);
  if ($('.active .custom-style').data('func') == 'underline') {

    textUnderline(context, line, x, y, $('#color').val(), $('#font').val(), $('.active .custom-align').data('func'));
  }
}


/**Wrap text */
$(document).on('change', '#font', function () {

  var textAlign = "center";

  var fontSize = $('#font').val();
  var fontFamily = $('#font_family').val();
  var fontSizeFamily = fontSize + "px " + fontFamily;

  createText(fontSizeFamily, textAlign)
});




//  $('#second').draggable();
$(document).on('click', '.nav-link', function () {
  var ids = $(this).attr('id');
  currentOwlIndex = $(this).attr('id');
  $('#caro_' + ids).find('.owl-item').eq(1).addClass('next1');
})
$(document).on('click', '#cancel5', function () {
  $('#main_forms').show();
  $('#first_sec').hide();//kuldeep

});
$(document).on('click', '#save', function () {
  $('#main_forms').show();
  // $('#first_sec').hide();//kuldeep
  $('#before_create').show();
  $('#final_save').show();
  $('#after_create').hide();
  $('#save_image').hide();
});

$(document).on('click', '#cancel4', function () {
  $('#main_forms').show();
  $('#first_sec').hide();//kuldeep

  $('#next3').show();
  $('#text_functionality').show();
  $('#save_image').hide();

  $('#next2').show();
  $('#next3').hide();
  $('#filters').show();
  $('#text_functionality').hide();
  $('#draw').hide();

  $('#first_browse').show();
  $('#first_browse1').show();
  $('#second_browse').hide();
  $('#second_browse1').hide();
  $('#next1').show();
  $('#filters').hide();
  $('#next2').hide();

  // $(this).hide();
});


$(document).on('click', '#cancel3', function () {
  $('#main_forms').show();
  $('#first_sec').hide();//kuldeep

  $('#next2').show();
  $('#next3').hide();
  $('#filters').show();
  $('#text_functionality').hide();
  $('#draw').hide();
  $('#first_browse').show();
  $('#first_browse1').show();
  $('#second_browse').hide();
  $('#second_browse1').hide();
  $('#next1').show();
  $('#filters').hide();
  $('#next2').hide();

  // $(this).hide();
});


/**Cropzee Code */
function loadFirstImage(url) {

  // $('#main_forms').hide();//kuldeep
  $('#remove_first').show();
  $('#first_browse').hide();
  $('#first_browse1').hide();
  $('#second_browse').show();
  $('#second_browse1').show();
  $('#dummy').hide();
  $('#blah').show();
  $('#blah').attr('src', url);
  $("#next1").show();
  $(".add-img").hide();
  $(".another-img").show();
}

function loadSecondImage(url) {

  $('#second').show();
  $('#second_browse').hide();
  $('#second_browse1').hide();
  $('#remove_second').show();
  $(".another-img").hide();
  $('#crop1').hide();
  $('#crop2').show();
  $('#second').attr('src', url);
  $("#transparency").val('0.5');




}
/**Cropzee Code */
/**Add story  */
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#main_forms').hide();//kuldeep
      $('#crop_sec').show();
      $('#first_sec').hide();//kuldeep
      $('#remove_first').show();
      $('#first_browse').hide();
      $('#first_browse1').hide();
      $('#second_browse').show();
      $('#second_browse1').show();
      $('#dummy').hide();
      $('#blah').show();
      $('#blah').attr('src', e.target.result);
      $("#next1").show();
      $(".add-img").hide();
      $(".another-img").show();
      $("#cropImage1").show();
      /**Crop */
      // var size;
      $('#cropbox1').attr('src', e.target.result);

      $('#cropbox1').Jcrop({
        // boxWidth: 270, boxHeight: 360,
        // trueSize: [270,360],
        minSize: [270, 360],
        maxSize: [270, 360],
        setSelect: [100, 100, 370, 460],


        aspectRatio: 0,
        onSelect: function (c) {
          size = { x: c.x, y: c.y, w: c.w, h: c.h };
          $("#crop1").css("visibility", "visible");
        }
      });


      /**Crop */
    }

    reader.readAsDataURL(input.files[0]);
  }
}
var context, ballon;
function readURL2(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#second').show();
      $('#second_browse').hide();
      $('#second_browse1').hide();
      $('#remove_second').show();
      $('#first_sec').hide();//kuldeep
      $('#crop_sec').show();

      $('#crop1').hide();
      $('#crop2').show();
      $('#second').attr('src', e.target.result);
      $("#transparency").val('0.5');
      $("#cropImage2").show();


      /**Crop */
      // var size;
      $('#cropbox2').attr('src', e.target.result);

      $('#cropbox2').Jcrop({
        // boxWidth: 270, boxHeight: 360,
        // trueSize: [270,360],
        minSize: [270, 360],
        maxSize: [270, 360],
        setSelect: [100, 100, 370, 460],


        aspectRatio: 0,
        onSelect: function (c) {
          size = { x: c.x, y: c.y, w: c.w, h: c.h };
          $("#crop2").css("visibility", "visible");
        }
      });


      /**Crop */
    }

    reader.readAsDataURL(input.files[0]);

  }
}



$('.owl-carousel').on('translate.owl.carousel', function (e) {
  // $(document).on('translate.owl.carousel','.loop', function (e) {
  console.log('dfsf');
  idx = e.item.index;
  // $('.owl-item.big').removeClass('big');
  // $('.owl-item.medium').removeClass('medium');
  $('.owl-item').eq(idx).addClass('big');
  $('.owl-item').eq(idx - 1).addClass('medium');
  $('.owl-item').eq(idx + 1).addClass('medium');
});






/**New */
$(document).ready(function () {
  // Activate Carousel
  $("#myCarousel").carousel();
  $("#myCarousel1").carousel('pause');


  // Enable Carousel Indicators
  $(".item1").click(function () {
    $("#myCarousel").carousel(0);
  });
  $(".item2").click(function () {
    $("#myCarousel").carousel(1);
  });
  $(".item3").click(function () {
    $("#myCarousel").carousel(2);
  });

  // Enable Carousel Controls
  $(".carousel-control-prev").click(function () {
    $("#myCarousel").carousel("prev");
  });
  $(".carousel-control-next").click(function () {
    $("#myCarousel").carousel("next");
  });

  $(document).on('slid.bs.carousel', "#myCarousel", function (e) {
    // $("#myCarousel").on('slid.bs.carousel', function(e){
    // alert('A new slide is about to be shown!');
    // alert( $("#myCarousel div.active").index());
    var currentIndex = $("#myCarousel div.active").index();
    $("#myCarousel").carousel(currentIndex);
    $("#myCarousel1").carousel(currentIndex);
    $('.carousel-indicators li').removeClass('active');
    $('.carousel-indicators li').eq(currentIndex).addClass('active');



  });
});
/**New */

// MediumEditor for edit chapter
$(document).ready(function () {
  var editor = new MediumEditor('#containernew');
  console.log(editor)
  $(function () {
    $('#containernew').mediumInsert({
      editor: editor,
      addons: {
        images: {
          fileUploadOptions: {
            type: 'post',
            url: 'https://librums.com:2001/api/uploadImageEditor',
            // url: 'http://18.209.143.118:8002/api/uploadImageEditor', 
          },
          uploadCompleted: function ($el, data) {
          },
        }
      }
    });
  });

});

$(document).ready(function () {


  // var button = Dropbox.createChooseButton(options);
  // if(jQuery("#Dropbox").length){
  //   document.getElementById("Dropbox").append(button);
  // }

  $('.also_like_slider').each(function (index, value) {
    $("#alsolike" + index).flipster({
      style: 'carousel',
      spacing: -0.3,
      buttons: true,
      start: 1,
      loop: false,
      autoplay: false,
      click: true,
      scrollwheel: false
      //   onItemSwitch: function(cur, prev) {
      //     $('.flipster__button--next, .flipster__button--prev').removeClass('disabled')

      //     if cur.siblings('.flipster__item--future').length == 0 {
      //         $('.flipster__button--next').addClass('disabled')
      //     }

      //     if cur.siblings('.flipster__item--past').length == 0 {
      //         $('.flipster__button--prev').addClass('disabled')
      //     }
      // }
    });
  });






  $('.toggle111').click(function () {
    $('.mobile_menu').slideToggle();
  });
});

$(window).scroll(function () {
  // var scroll = $(window).scrollTop();
  // var $h1 = $("#canvas");
  // mainText.y = $h1.offset().top - $(window).scrollTop();

  if (scroll >= 100) {
    $(".logo").addClass("logo_scroll, logo_scroll a img ").css({ "transition": "0.3s" });
    $(".flex_header").addClass("scroll_pd");
    $(".main_header").css({ "box-shadow": "0px 6px 11px rgba(0, 0, 0, 0.06)", "background": "#fff" });
  } else {
    $(".logo").removeClass("logo_scroll, logo_scroll a img").css({ "transition": "0.3s" });
    $(".flex_header").removeClass("scroll_pd");
    $(".main_header").css({ "box-shadow": "unset", "background": "#fff" });
  }
});
$(window).on('load', function () {
  new WOW().init();
  // $('#wlcm_back').modal('show');
  $("#cover_image").cropzee({
    aspectRatio: 1.33,

  });

  $("#cover_image2").cropzee({
    aspectRatio: 1.33,

  });
  $('#chkToggle2').bootstrapToggle()

  $('.exp_crsl').each(function (index, value) {
    $("#coverflow" + index).flipster({
      style: 'carousel',
      spacing: -0.3,
      buttons: true,
      start: 'center',
      loop: true,
      autoplay: false,
      click: true,
      scrollwheel: false,
    });
  });
});
$(document).on('click', '.custom-control-input', function () {
  $('#text_change').text($(this).attr('data-text'));
})

$(document).on('click', '.toggle5', function () {
  $("header.ch_header .header_icon_right2").slideToggle("");
});


$(document).on('click', '#list_view', function () {
  // $('.col-md-12.col-lg-12.col-xl-12.mb-5').css('flex', '0 0 100%', 'max-width', '100%');
  $('.layout-1').removeClass('col-lg-6 col-xl-6');
  $('.layout-1').addClass('col-lg-12 col-xl-12');
  $('button').removeClass("active");
  $(this).addClass("active");
});
$(document).on('click', '#grid_view', function () {
  // $('.col-lg-12.col-xl-12.mb-5').css('flex', '0 0 50%', 'max-width', '50%');
  $('.layout-1').addClass('col-lg-6 col-xl-6');
  $('.layout-1').removeClass('col-lg-12 col-xl-12');
  $('button').removeClass("active");
  $(this).addClass("active");
});


$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  if (scroll >= 50) {
    $("header.top_header_mr .logo3 a img").css({ "width": "140px", "transition": "0.3s" });
    $("header.top_header_mr .flex_header").removeClass("scroll_pd");
  } else {
    $("header.top_header_mr .logo3 a img").css({ "width": "unset", "transition": "0.3s" });
  }
});


var hh = $("#carousel").flipster({
  style: 'carousel',
  spacing: -0.5,
  nav: true,
  buttons: true,
});
console.log(alert)

$(document).on('click', '.toggle2.comment_setting', function () {
  $('#list_view_setting').slideToggle();
});

$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  if (scroll >= 50) {
    $("header.ch_header .main_header .flex_header").removeClass("scroll_pd");
    $("header.ch_header .main_header").css({ "padding": "0px" });
  } else {
    $("header.ch_header .main_header .flex_header").addClass("scroll_pd");
    $("header.ch_header .main_header").css({ "padding": "10px" });
    $("header.ch_header .main_header .flex_header").removeClass("scroll_pd");
  }
});
/**New */
// $(document).ready(function(){
setTimeout(function () {
  //your code here

  // window.onload = (event) => {
  var scrollDuration = 300;
  // paddles
  var leftPaddle = document.getElementsByClassName('left-paddle');
  var rightPaddle = document.getElementsByClassName('right-paddle');
  // get items dimensions
  var itemsLength = $('.nav-item').length;
  var itemSize = $('.nav-item').outerWidth(true);
  // get some relevant size for the paddle triggering point
  var paddleMargin = 20;

  // get wrapper width
  var getMenuWrapperSize = function () {
    return $('.over_flow_tab2').outerWidth();
  }
  var menuWrapperSize = getMenuWrapperSize();
  // alert(menuWrapperSize);
  // the wrapper is responsive
  $(window).on('resize', function () {
    menuWrapperSize = getMenuWrapperSize();
  });
  // size of the visible part of the menu is equal as the wrapper size 
  var menuVisibleSize = menuWrapperSize;

  // get total width of all menu items
  var getMenuSize = function () {
    return itemsLength * itemSize;
  };
  var menuSize = getMenuSize();
  // get how much of menu is invisible
  var menuInvisibleSize = menuSize - menuWrapperSize;

  // get how much have we scrolled to the left
  var getMenuPosition = function () {
    return $('.custom_tabs_pills').scrollLeft();
  };

  // finally, what happens when we are actually scrolling the menu
  $('.custom_tabs_pills').on('scroll', function () {
    // alert('here');
    // get how much of menu is invisible
    menuInvisibleSize = menuSize - menuWrapperSize;
    // get how much have we scrolled so far
    var menuPosition = getMenuPosition();

    var menuEndOffset = menuInvisibleSize - paddleMargin;

    // show & hide the paddles 
    // depending on scroll position
    if (menuPosition <= paddleMargin) {
      $(leftPaddle).addClass('hidden');
      $(rightPaddle).removeClass('hidden');
    } else if (menuPosition < menuEndOffset) {
      // show both paddles in the middle
      $(leftPaddle).removeClass('hidden');
      $(rightPaddle).removeClass('hidden');
    } else if (menuPosition >= menuEndOffset) {
      $(leftPaddle).removeClass('hidden');
      $(rightPaddle).addClass('hidden');
    }

    // // print important values
    // $('#print-wrapper-size span').text(menuWrapperSize);
    // $('#print-menu-size span').text(menuSize);
    // $('#print-menu-invisible-size span').text(menuInvisibleSize);
    // $('#print-menu-position span').text(menuPosition);

  });
  // scroll to left
  // $(rightPaddle).on('click', function() {
  $(document).on('click', '.right-paddle', function () {
    // alert('x');
    // alert(menuInvisibleSize);
    $('.custom_tabs_pills').animate({ scrollLeft: '+=260' }, scrollDuration);
    // $('.custom_tabs_pills').animate( { scrollLeft: menuInvisibleSize}, scrollDuration);
  });

  // scroll to right
  // $(leftPaddle).on('click', function() {
  $(document).on('click', '.left-paddle', function () {
    // alert('xx');
    $('.custom_tabs_pills').animate({ scrollLeft: '-=260' }, scrollDuration);
  });

}, 10000);
/**New */


/**Dragabl */
$(window).on('load', function () {


  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");

  // var $canvas = $("#canvas");
  // var canvasOffset = $canvas.offset();
  // var offsetX = canvasOffset.left;
  // // alert(offsetX);
  // var offsetY = canvasOffset.top;
  var scrollX = $canvas.scrollLeft();
  var scrollY = $canvas.scrollTop();



  var startX;
  var startY;

  // an array to hold text objects
  var texts = [];

  texts.x = canvas.width;
  texts.y = canvas.height;

  // this var will hold the index of the hit-selected text
  var selectedText = -1;


  function draw(dx, dy) {
    var textAlign = "center";
    var fontSize = $('#font').val();
    var fontFamily = $('#font_family').val();
    var fontSizeFamily = fontSize + "px " + fontFamily;
    textAlign = $('.active .custom-align').data('func');
    // alert(textAlign);
    var textColor = $('#color').val();
    var text = $('#img_title').val();
    var d_canvas = document.getElementById('canvas');
    var context = d_canvas.getContext('2d');
    var background = document.getElementById('blah');
    var ballon = document.getElementById('second')
    context.clearRect(0, 0, d_canvas.width, d_canvas.height);
    // context.globalAlpha = $('#transparency').val();
    context.drawImage(background, 0, 0, 270, 360);
    // context.globalAlpha = $('#brightness').val();
    context.drawImage(ballon, 0, 0, 270, 360);
    // context.globalAlpha = $('#transparency').val();
    // context.globalAlpha = $('#brightness').val();
    context.globalCompositeOperation = 'source-over';

    context.font = fontSizeFamily;
    context.textAlign = textAlign;
    context.fillStyle = textColor;
    // context.fillText(text,  x, y);
    wrapText(context, text, dx, dy, 270, 25)
  }

  // test if x,y is inside the bounding box of texts[textIndex]

  function textHittest(x, y, textIndex) {
    var text = mainText;
    // alert('$(window).scrollTop()'+$(window).scrollTop());
    // alert('x'+x);
    // alert('text.x'+text.x);
    // alert('text.width'+text.width);
    // alert('y'+y);
    // alert('text.y'+text.y);
    // alert('text.height'+text.height);
    var textdoty = parseInt(text.y - $(window).scrollTop());
    // alert(x +">="+ text.x +"&&"+ x +"<="+ text.x +"+"+ text.width +"&&"+ y +">="+ textdoty +"-"+ text.height +"&&"+ y +"<="+ textdoty);
    return (x >= text.x && x <= text.x + text.width && y >= textdoty - text.height && y <= textdoty);
  }
  // function textHittest(x, y, textIndex) {
  //   var text = $('#img_title').val();
  //   // alert(text);

  //   var colorTable = document.getElementById("canvas");
  //   var tOLeft = colorTable.offsetLeft;
  //   var tOLeft1 = colorTable.offsetTop;
  //   alert(document.body.offsetLeft);
  //   alert(document.body.offsetTop);
  //   alert(mainText.x);
  //   alert(mainText.y);
  //   alert(mainText.width);
  //   alert(mainText.height);
  //   alert(x);
  //   alert(y);
  //   return (x >= (mainText.x) && x <= mainText.x + mainText.width  && y >= mainText.y - mainText.height && y <= mainText.y);
  // }

  // handle mousedown events
  // iterate through texts[] and see if the user
  // mousedown'ed on one of them
  // If yes, set the selectedText to the index of that text
  function handleMouseDown(e) {
    e.preventDefault();
    // alert('new');
    // alert(e.clientX+'e.clientX');
    // alert(offsetX+ 'offsetX');
    startX = parseInt(e.clientX - offsetX);
    startY = parseInt(e.clientY - offsetY);
    // alert(e.clientY,'e.clientXy');
    // alert(offsetY,'e.offsetY');
    // Put your mousedown stuff here
    // for (var i = 0; i < texts.length; i++) {
    if (textHittest(startX, startY, 0)) {
      // alert('true');
      selectedText = 0;
    } else {
      // selectedText = 0;
      // alert('false');
    }
    // }
  }

  // done dragging
  function handleMouseUp(e) {
    e.preventDefault();
    selectedText = -1;
  }

  // also done dragging
  function handleMouseOut(e) {
    e.preventDefault();
    selectedText = -1;
  }

  // handle mousemove events
  // calc how far the mouse has been dragged since
  // the last mousemove event and move the selected text
  // by that distance


  function handleMouseMove(e) {
    // alert(selectedText);
    if (selectedText < 0) {
      return;
    }
    e.preventDefault();

    mouseX = parseInt(e.clientX - offsetX);
    mouseY = parseInt(e.clientY - offsetY);

    // Put your mousemove stuff here
    var dx = mouseX - startX;
    var dy = mouseY - startY;
    startX = mouseX;
    startY = mouseY;


    mainText.x += dx;
    mainText.y += dy;
    // mainText.y += ( $(window).scrollTop());
    // alert('$(window).scrollTop()'+$(window).scrollTop());  
    draw(mainText.x, (mainText.y));
  }

  // listen for mouse events
  $("#canvas").mousedown(function (e) {
    // $(document).on('mousedown', '#canvas', function (event) {
    // alert('qq');
    handleMouseDown(e);
  });
  $("#canvas").mousemove(function (e) {
    // $(document).on('mousemove', '#canvas', function (event) {
    console.log('zxcxzc');
    // alert('qqzz');
    handleMouseMove(e);
  });
  $("#canvas").mouseup(function (e) {
    // $(document).on('mouseup', '#canvas', function (event) {
    // alert('qqx');
    handleMouseUp(e);
  });
  $("#canvas").mouseout(function (e) {
    // $(document).on('mouseout', '#canvas', function (event) {
    //alert('qqz');
    handleMouseOut(e);
  });
});
/**Dragabl */
// $(document).click(function (e) {
//   if (!$(e.target).is('#setting_filter')) {
//     $('.collapse').collapse('hide');
//   }
// });

$(document).on('click', function (e){
  /* bootstrap collapse js adds "in" class to your collapsible element*/
  var menu_opened = $('#setting_filter').hasClass('show');

  if(!$(e.target).closest('#setting_filter').length &&
      !$(e.target).is('#setting_filter') &&
      menu_opened === true){
          $('#setting_filter').collapse('toggle');
  }
  var menu_opened = $('#show_chapter').hasClass('show');

  if(!$(e.target).closest('#show_chapter').length &&
      !$(e.target).is('#show_chapter') &&
      menu_opened === true){
          $('#show_chapter').collapse('toggle');
  }

});